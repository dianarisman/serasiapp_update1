/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import LoginPage from "./src/pages/LoginPage/LoginPage";
import RegisterPage from "./src/pages/RegisterPage/RegisterPage";
import TestingPage from "./src/pages/TestingPage/TestingPage";
import TestingPage2 from "./src/pages/TestingPage/TestingPage2";
import HomePage from "./src/pages/MainActivity/MainActivity";
import AuthLoadingScreen from "./src/pages/AuthLoadingScreen/AuthLoadingScreen";
import {Toast, Root, ActionSheet} from "native-base";
import RegisterPage_Selfie from "./src/pages/RegisterPage_Selfie/RegisterPage_Selfie";
import RegisterPage_Eula from "./src/pages/RegisterPage_Eula/RegisterPage_Eula";
import TestingTab from "./src/pages/TestingPage/TestingTab";
import ResetPasswordPage from "./src/pages/ResetPasswordPage/ResetPasswordPage";
import TestingGridView from "./src/pages/TestingPage/TestingGridView";
import TestingGridView2 from "./src/pages/TestingPage/TestingGridView2";
import TestingSlides from "./src/pages/TestingPage/TestingSlides";
import TestingToolbarParalax from "./src/pages/TestingPage/TestingToolbarParalax";
import TestingToolbarParalax2 from "./src/pages/TestingPage/TestingToolbarParalax2";
import ProfileDetailPage from "./src/pages/ProfileDetailPage/ProfileDetailPage";
import TestingGridView3 from "./src/pages/TestingPage/TestingGridView3";
import NewsPage from "./src/pages/NewsPage/NewsPage";
import NewsDetailPage from "./src/pages/NewsPage/NewsDetailPage";
import TestingPageDatePickerStrip from "./src/pages/TestingPage/TestingPageDatePickerStrip";
import TestingHighlightSelectItemFlatList from "./src/pages/TestingPage/TestingHighlightSelectItemFlatList";
import ConsultationPage from "./src/pages/ConsultationPage/ConsultationPage";
import UploadProofOfPayment from "./src/pages/UploadProofOfPayment/UploadProofOfPayment";
import TestingPopover from "./src/pages/TestingPage/TestingPopover";
import TherapyPage from "./src/pages/TherapyPage/TherapyPage";
import SchedulePage from "./src/pages/SchedulePage/SchedulePage";
import TestingAppIntroSlider from "./src/pages/TestingPage/TestingAppIntroSlider";
import AppIntro from "./src/pages/AppIntro/AppIntro";
import EditProfilePage from "./src/pages/EditProfilePage/EditProfilePage";
import ChangePasswordPage from "./src/pages/ChangePasswordPage/ChangePasswordPage";


import * as firebase from 'firebase';
import Chat from "./src/pages/TestingPage/TestingChat3/Chat";
import TestingPricing from "./src/pages/TestingPage/TestingPricing";
import PricingPage from "./src/pages/PricingPage/PricingPage";
import IntroductionPage from "./src/pages/IntroductionPage/IntroductionPage";
import ChatList from "./src/pages/TestingPage/TestingChat3/ChatList";
import ChatConversation from "./src/pages/TestingPage/TestingChat3/ChatConversation";
import ChatFindFriends from "./src/pages/TestingPage/TestingChat3/ChatFindFriends";
import PricingHistoryPage from "./src/pages/PricingPage/PricingHistoryPage";
import ChatingRoomPage from "./src/pages/ChatingRoomPage/ChatingRoomPage";
import TestingPricing2 from "./src/pages/TestingPage/TestingPricing2";
import NotifService from './NotifService';
import {Alert} from 'react-native';
import GlobalDataRequest from "./src/services/GlobalDataRequest";
import TestingImageZoom from "./src/pages/TestingPage/TestingImageZoom";
import PushNotification from "react-native-push-notification";
import variables from "./src/variables/MyGlobalVariable";
import ChangePhotoProfile from "./src/pages/ChangePhotoProfile";

let configDB = {
    apiKey: variables.apiKey,
    authDomain: variables.authDomain,
    databaseURL: variables.databaseURL,
    projectId: variables.projectId,
    storageBucket: variables.storageBucket,
    messagingSenderId: variables.messagingSenderId
};

!firebase.apps.length ? firebase.initializeApp(configDB) : null;

const RootStack = createStackNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      Login: LoginPage,
      Testing: TestingPage,
      Testing_mstore: TestingPage2,
      Home: HomePage,
      RegisterStep1: RegisterPage_Eula,
      RegisterStep2: RegisterPage,
      RegisterStep3: RegisterPage_Selfie,
      NewsPage: NewsPage,
        ChangePhotoProfile: ChangePhotoProfile,
        ChatRoomPage: ChatingRoomPage,
        NewsDetailPage: NewsDetailPage,
        ConsultationPage: ConsultationPage,
        TherapyPage: TherapyPage,
        SchedulePage: SchedulePage,
        UploadProofOfPayment: UploadProofOfPayment,
        AppIntro: AppIntro,
        EditProfilePage: EditProfilePage,
        ChangePasswordPage: ChangePasswordPage,
        PricingPage: PricingPage,
        PricingHistoryPage: PricingHistoryPage,
        IntroductionPage: IntroductionPage,
      TestingHomeTab: TestingTab,
      ResetPassword: ResetPasswordPage,
        TestingGridView: TestingGridView,
        TestingGridView2: TestingGridView2,
        TestingGridView3: TestingGridView3,
        TestingSlides: TestingSlides,
        TestingToolbarParalax: TestingToolbarParalax,
        TestingToolbarParalax2: TestingToolbarParalax2,
        TestingPageDatePickerStrip: TestingPageDatePickerStrip,
        TestingHighlightSelectItemFlatList: TestingHighlightSelectItemFlatList,
        TestingAppIntroSlider: TestingAppIntroSlider,
        TestingPopover: TestingPopover,
        TestingPricing: TestingPricing,
        TestingPricing2: TestingPricing2,
        Chat_TestingChatPage2: Chat,
        TestingChat3_Chat: Chat,
        TestingChat3_ChatList: ChatList,
        TestingChat3_ChatConversation: ChatConversation,
        TestingChat3_ChatFindFriends: ChatFindFriends,
        TestingImageZoom: TestingImageZoom,
        ProfileDetail: ProfileDetailPage,

    },
    {
      initialRouteName: 'AuthLoading',
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        }
    }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            senderId: variables.senderID
        };

        this.notif = new NotifService(this.onRegister.bind(this), this.onNotif.bind(this));
    }

    onRegister(token) {
        //Alert.alert("Registered !", JSON.stringify(token, null, 3));
        //console.log(token);
        this.setState({ registerToken: token.token, gcmRegistered: true });
        GlobalDataRequest.fcmUserTokenSave(token.token).then((result)=>{
            console.log('fcmUserTokenSave ', result);
        }).catch(error=>{
            console.log('error fcmUserTokenSave ', error);
        });
    }

    onNotif(notif) {
        console.log(notif);
        //this.localNotif();
        //Alert.alert(notif.title, notif.message);
    }

    localNotif() {
        PushNotification.localNotification({
            /* Android Only Properties */
            id: '210', // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
            ticker: "My Notification Ticker", // (optional)
            autoCancel: true, // (optional) default: true
            largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
            smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
            bigText: "My big text that will be shown when notification is expanded", // (optional) default: "message" prop
            subText: "This is a subText", // (optional) default: none
            color: "red", // (optional) default: system default
            vibrate: true, // (optional) default: true
            vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
            tag: 'some_tag', // (optional) add tag to message
            group: "group", // (optional) add group to message
            ongoing: false, // (optional) set whether this is an "ongoing" notification

            /* iOS only properties */
            alertAction: 'view', // (optional) default: view
            category: null, // (optional) default: null
            userInfo: null, // (optional) default: null (object containing additional notification data)

            /* iOS and Android properties */
            title: "Local Notification", // (optional)
            message: "My Notification Message", // (required)
            playSound: true, // (optional) default: true
            soundName: 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
            number: '10', // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
            actions: '["Yes", "No"]',  // (Android only) See the doc for notification actions to know more
        });
    }

    handlePerm(perms) {
        Alert.alert("Permissions", JSON.stringify(perms));
    }

    componentWillMount() {
        this.notif.configure(this.onRegister.bind(this), this.onNotif.bind(this), this.state.senderId);
    }

    componentWillUnmount() {
        Toast.toastInstance = null;
        ActionSheet.actionsheetInstance = null;
    }
  render() {
    return <Root><AppContainer /></Root>;
  }
}


