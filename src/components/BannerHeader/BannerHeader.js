import React from "react";
import {ScrollView} from "react-native";
import SlideItem from "../SlideItem";

export default class BannerLarge extends React.Component {
    constructor(props){
        super(props);
    }

    onViewItemScreen = (item) => {
        alert(JSON.stringify(item));
    };
    render() {
        return (
            <ScrollView>
                <SlideItem
                    items={this.props.dataBanner}
                    onViewItemScreen={this.onViewItemScreen}/>
            </ScrollView>
        );
    }
}
