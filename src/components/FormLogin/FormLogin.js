import React from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import GlobalDataRequest from "../../services/GlobalDataRequest";
import variables from "../../variables/MyGlobalVariable";
import styles from "../../styles/MainStyle";

export default class FormLogin extends React.Component {


    constructor(props){
        super(props);
        this.state =
            {
                isLoading: false,
                username: 'root@gmail.com',
                password: 'rasmuslerdorf',
                text: ""
            };
    }

    loginTest() {
        this.setState({
            isLoading: true
        });
        GlobalDataRequest.login_eCluster(this.state.username,this.state.password).then((result) => {
            this.setState({
                isLoading: false,
                dataSource: JSON.stringify(result),
            }, function(){

            });

            if(result.access_token) {
                GlobalDataRequest.AuthSave(JSON.stringify(result), "true", result.access_token).then((result) => {
                    this.props.navigation.replace('Home');
                }, err => {
                    alert("Somthing Error " + JSON.stringify(err));
                });
            }else {
                alert("after if");
            }
        }, err => {
            alert("error   " + JSON.stringify(err));
            this.setState({
                isLoading: false
            }, function(){

            });
        }).catch((err)=> alert('error ' + JSON.stringify(err)));
    }

    _signInAsync() {
        //await AsyncStorage.setItem('userToken', 'abc');
        //alert("a");
        this.props.navigation.navigate('Home')
    };

    render(){

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return(
            <View style={styles.formLogin.container}>
                <Text>{this.state.dataSource}</Text>
                <Text>{this.state.text}</Text>
                <TextInput
                    mode="outlined"
                    label='Email'
                    value={this.state.text}
                    onChangeText={text => this.setState({ text })}
                />
                <TextInput style={styles.formLogin.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder={variables.emailPlaceholder}
                           placeholderTextColor = "#ffffff"
                           selectionColor="#fff"
                           keyboardType="email-address"
                           onSubmitEditing={()=> this.passwordnya.focus()} // jika klik enter akan fokus ke input ref={(input) => this.password = input}
                           onChangeText={(username) => this.setState({username})}
                           value={this.state.username}
                />
                <TextInput style={styles.formLogin.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder={variables.passwordPlaceholder}
                           secureTextEntry={true}
                           placeholderTextColor = "#ffffff"
                           ref={(input) => this.passwordnya = input}
                           onChangeText={(password) => this.setState({password})}
                           value={this.state.password}
                />
                <TouchableOpacity style={styles.formLogin.button} onPress={() => this.loginTest() }>
                    <Text style={styles.formLogin.buttonText}>{variables.login}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
