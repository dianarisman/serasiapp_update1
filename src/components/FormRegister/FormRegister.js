import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native';
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";

export default class FormRegister extends Component {
    render(){
        return(
            <View style={styles.formRegister.container}>
                <TextInput style={styles.formRegister.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder={variables.emailPlaceholder}
                           placeholderTextColor = "#ffffff"
                           selectionColor="#fff"
                           keyboardType="email-address"
                           onSubmitEditing={()=> this.password.focus()}
                />
                <TextInput style={styles.formRegister.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder={variables.passwordPlaceholder}
                           secureTextEntry={true}
                           placeholderTextColor = "#ffffff"
                           ref={(input) => this.password = input}
                />
                <TextInput style={styles.formRegister.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder={variables.confirmPasswordPlaceholder}
                           secureTextEntry={true}
                           placeholderTextColor = "#ffffff"
                           ref={(input) => this.password = input}
                />
                <TouchableOpacity style={styles.formRegister.button}>
                    <Text style={styles.formRegister.buttonText}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
