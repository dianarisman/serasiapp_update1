import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
export default class Logo extends Component<{}> {
    render(){
        return(
            <View style={styles.container}>
                <Image
                        source={require('../../images/serasilogo1.png')} style={{height: 63, width: 250}} />
                <Text style={styles.logoText}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        justifyContent:'flex-end',
        alignItems: 'center'
    },

    logoText : {
        marginVertical: 15,
        fontSize:18,
        color:'#000000'
    }
});
