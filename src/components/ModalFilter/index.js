import React, { Component } from 'react';
import {FlatList, Modal, TouchableOpacity, View} from "react-native";
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import { Icon, Left, ListItem, Right, Text, Picker } from 'native-base';
export default class ModalFilter extends Component {
    constructor(props){
        super(props);
    }
    _onPressHide = () => {
        this.props.onHideModal(!this.props.isModalVisible);
        this.props.onReadyFilter(this.props.selectedMarital);
    };

    _onSelectItemMarital = (value) => {
        this.props.onGetSelectedItemMarital(value);
    };
    _onSelectItem = (index) => {
        this.props.onSelectItemCriteria(index, this.props.listCriteria);
    };

    _onSelectItemCity = (index) => {
        this.props.onSelectItemCity(index, this.props.listCity);
    };

    render() {
        let marital_list = this.props.listMarital.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });
        return (
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.props.isModalVisible}
                        onRequestClose={() => {
                            //Alert.alert('Modal has been closed.');
                            this._onPressHide();
                        }}>
                        <View style={{flex: 1,  marginHorizontal: 30,marginTop: 30}}>
                            <TouchableOpacity
                                onPress={()=> this.props.onResetFilter()}
                                style={{
                                    position: 'absolute',
                                    top: -10,
                                    right: 0,
                                    zIndex: 100,
                                paddingHorizontal: 15,
                                paddingVertical: 10,
                                alignItems: 'center',
                                borderRadius: 7,
                                borderWidth: 1.4,
                                borderColor: styles.color.serasiColorButton
                            }}>
                                <Text
                                    style={{
                                        color: styles.color.serasiColorButton,
                                        fontSize: 13
                                    }}
                                >{variables.resetFilter}</Text>
                            </TouchableOpacity>
                            <Text style={styles.registerPage.listItemHeaderText}>{this.props.selectTitleMarital}</Text>
                            <Picker
                                mode="dialog"
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.statusMaritalPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                selectedValue={this.props.selectedMarital}
                                onValueChange={(itemValue, itemIndex) =>
                                    this._onSelectItemMarital(itemValue)
                                }>
                                {marital_list}
                            </Picker>
                            <Text style={styles.registerPage.listItemHeaderText}>{this.props.selectTitleCity}</Text>
                            <FlatList
                                data={this.props.listCity}
                                renderItem={({item, index}) =>
                                    <ListItem
                                        onPress={()=> this._onSelectItemCity(index, item.id)}>
                                        <Left>
                                            <Text style={styles.color.listItemSelect((item.is_selected) ? styles.color.serasiColorButton : styles.color.placeholderInput)}>{item.city}</Text>
                                        </Left>
                                        <Right>
                                            {(item.is_selected) ? <Icon type="Entypo" name="check" style={{ color: styles.color.serasiColorButton}} /> : null}
                                        </Right>
                                    </ListItem>}
                                keyExtractor={item => item.id.toString()}
                            />
                            <Text/>
                            <Text style={styles.registerPage.listItemHeaderText}>{this.props.selectTitleCriteria}</Text>
                            <FlatList
                                data={this.props.listCriteria}
                                renderItem={({item, index}) =>
                                    <ListItem
                                        key={index.toString()}
                                        onPress={()=> this._onSelectItem(index, item.key)}>
                                        <Left>
                                            <Text style={styles.color.listItemSelect((item.is_selected) ? styles.color.serasiColorButton : styles.color.placeholderInput)}>{item.title}</Text>
                                        </Left>
                                        <Right>
                                            {(item.is_selected) ? <Icon type="Entypo" name="check" style={{ color: styles.color.serasiColorButton}} /> : null}
                                        </Right>
                                    </ListItem>}
                            />
                            <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this._onPressHide() }>
                                <Text style={styles.formLogin.buttonText}>{variables.ok}</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
        );
    }
}


