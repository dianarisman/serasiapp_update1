import React, { Component } from 'react';
import {FlatList, Modal, TouchableOpacity, View} from "react-native";
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import { Container, Header, Content, Form, Item, Input, Label, Icon, Left, ListItem, Right, Text } from 'native-base';
export default class ModalSelectCriteria extends Component {
    constructor(props){
        super(props);
    }
    _onPressHide = () => {
        this.props.onHideModal(!this.props.isModalVisible);
        this._getSelectedItem();
    };
    _getSelectedItem = () => {
        this.props.onGetSelectedItem(this.props.listCriteria);
    };
    _onSelectItem = (index) => {
        this.props.onSelectItem(index, this.props.listCriteria);
    };
    render() {
        return (
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.props.isModalVisible}
                        onRequestClose={() => {
                            //Alert.alert('Modal has been closed.');
                            this._onPressHide();
                        }}>
                        <View style={{flex: 1,  marginHorizontal: 30,marginTop: 30}}>
                            <Text style={styles.registerPage.listItemHeaderText}>{this.props.selectTitle}</Text>
                            <FlatList
                                data={this.props.listCriteria}
                                renderItem={({item, index}) =>
                                    <ListItem onPress={()=> this._onSelectItem(index, item.key)}>
                                        <Left>
                                            <Text style={styles.color.listItemSelect((item.is_selected) ? styles.color.serasiColorButton : styles.color.placeholderInput)}>{item.title}</Text>
                                        </Left>
                                        <Right>
                                            {(item.is_selected) ? <Icon type="Entypo" name="check" style={{ color: styles.color.serasiColorButton}} /> : null}
                                        </Right>
                                    </ListItem>}
                            />
                            <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this._onPressHide() }>
                                <Text style={styles.formLogin.buttonText}>{variables.ok}</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
        );
    }
}


