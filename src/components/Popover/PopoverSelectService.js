import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";

export default class PopoverSelectService extends React.Component {
    render() {
        let renderServiceList = this.props.serviceList.map( (data, index) => {
            return (
                <TouchableOpacity
                    style={{marginVertical: 6}}
                    activeOpacity={0.7}
                    key={index}
                    onPress={()=> this.props.onSelectService(data)}
                >
                    <Text>- {data.layanan} | Rp. {GlobalDataRequest.numberFormat(data.price)}</Text>
                </TouchableOpacity>
            );
        });

        return (
            <ScrollView contentContainerStyle={{padding: 20, minWidth: 400}}>
                <Text style={{fontSize: 16, fontWeight: 'bold', marginBottom: 10}}>Pilih Layanan</Text>
                <View style={styles.row}>
                    <View style={{flex: 1}}>
                        {renderServiceList}
                    </View>
                </View>
            </ScrollView>
        )
    }
}
