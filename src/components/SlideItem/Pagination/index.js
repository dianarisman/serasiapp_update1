/** @format */

import React from "react";
import { View, Dimensions } from "react-native";
import Item from "./Item";
import variables from "../../../variables/MyGlobalVariable";
const { width } = Dimensions.get("window");


class Pagination extends React.Component {
  render() {
    const { items, selectedIndex, onNext } = this.props;
    let pageItem = items.map( (data, index) => {
      return <Item
          key={index}
          totalItems={items.length}
          active={index === selectedIndex}
          isReset={selectedIndex === 0}
          onNext={onNext}
      />
    });

    return (
      <View style={styles.container}>
        {pageItem}
      </View>
    );
  }
}

const styles = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    position: "absolute",
    bottom: 10,
    left: width * 0.2,
    right: width * 0.2,
  },
};

export default Pagination;
