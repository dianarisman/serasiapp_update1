import React, { Component } from 'react';
import {
    StatusBar
} from 'react-native';
import MyGlobalVariable from "../../variables/MyGlobalVariable";
import MainStyle from "../../styles/MainStyle";

export default class MyStatusBar extends Component {
    render(){
        return(
            <StatusBar
                backgroundColor={MainStyle.color.statBarColor}
                barStyle="light-content"
            />
        )
    }
}

