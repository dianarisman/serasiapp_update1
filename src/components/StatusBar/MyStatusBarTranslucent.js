import React, { Component } from 'react';
import {
    StatusBar
} from 'react-native';

export default class MyStatusBarTranslucent extends Component {
    render(){
        return(
            <StatusBar
                translucent
                barStyle="light-content"
                backgroundColor="rgba(0, 0, 0, 0.251)"
            />
        )
    }
}

