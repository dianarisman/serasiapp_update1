import React, {Component} from 'react';
import {Text, View} from  'react-native';
import styles from "../../styles/MainStyle";

export default class StatusMaritalDetailProfile extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {statusMarital} = this.props;
        let color = '';
        if(statusMarital==='janda') {
            color = styles.color.jandaColor;
        }else if(statusMarital==='duda') {
            color = styles.color.dudaColor;
        }else if(statusMarital==='Single' || statusMarital==='single' || statusMarital==='lajang') {
            color = styles.color.singleColor;
        }else if(statusMarital==='menikah') {
            color = styles.color.menikahColor;
        }else {
            color = styles.color.placeholderInput;
        }
        return (
            <View style={{flexDirection: 'row'}}>
                <Text style={styles.otherStyle.statusMaritalDetailProfiletext(color)}>
                    {this.props.statusMarital}
                </Text>
                <Text style={{color: '#ffffff', fontSize: 14, fontWeight: 'bold', paddingHorizontal: 10}}>{this.props.city}</Text>
            </View>

        );
    }
}
