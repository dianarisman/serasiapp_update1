import React, {Component} from 'react';
import {Text} from  'react-native';
import styles from "../../styles/MainStyle";

class StatusMaritalText extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {statusMarital} = this.props;
        let color = '';
        if(statusMarital==='janda') {
            color = styles.color.jandaColor;
        }else if(statusMarital==='duda') {
            color = styles.color.dudaColor;
        }else if(statusMarital==='Single' || statusMarital==='single' || statusMarital==='lajang') {
            color = styles.color.singleColor;
        }else if(statusMarital==='menikah') {
            color = styles.color.menikahColor;
        }else {
            color = styles.color.placeholderInput;
        }
        return (
            <Text style={styles.otherStyle.statusMaritaltext(color)}>
                {this.props.statusMarital}
            </Text>
        );
    }
}

export default StatusMaritalText;
