import React, { Component } from 'react';
import { Container, Footer, Tab, Tabs, TabHeading, Icon, Text, FooterTab, Badge, Button, StyleProvider} from 'native-base';
import getTheme from '../../../native-base-theme/components';
import material from '../../../native-base-theme/variables/material';
import variables from "../../variables/MyGlobalVariable";
export default class TabBar extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Footer>
                    <FooterTab>
                        <Button
                                vertical
                                active={this.props.navigation.state.index === 0}
                                onPress={() => this.props.navigation.navigate("Tab1")}>


                            <Icon type="SimpleLineIcons" name="home" />
                            <Text>{variables.home}</Text>
                        </Button>
                        <Button vertical
                                active={this.props.navigation.state.index === 1}
                                onPress={() => this.props.navigation.navigate("Tab2")}>
                            <Icon type="Feather" name="search" />
                            <Text>{variables.explore}</Text>
                        </Button>
                        <Button //badge
                                vertical
                                active={this.props.navigation.state.index === 2}
                                onPress={() => this.props.navigation.navigate("Tab3")}>
                            {/*<Badge ><Text>51</Text></Badge>*/}
                            <Icon type="AntDesign" name="message1" />
                            <Text>{variables.message}</Text>
                        </Button>
                        <Button
                            //badge
                            vertical
                                active={this.props.navigation.state.index === 3}
                                onPress={() => this.props.navigation.navigate("Tab4")}>
                            {/*<Badge ><Text>51</Text></Badge>*/}
                            <Icon type="SimpleLineIcons" name="bell" />
                            <Text>{variables.notification_sort}</Text>
                        </Button>
                        <Button vertical
                                active={this.props.navigation.state.index === 4}
                                onPress={() => this.props.navigation.navigate("Tab5")}>
                            <Icon type="FontAwesome" name="user-o" />
                            <Text>{variables.me}</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </StyleProvider>

        );
    }
}
