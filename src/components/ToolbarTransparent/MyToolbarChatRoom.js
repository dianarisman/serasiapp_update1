import React, { Component } from 'react';
import {Body, Button, Header, Icon, Left, Right, Thumbnail} from "native-base";
import MyStatusBar from "../StatusBar/MyStatusBar";
import {
    TouchableOpacity} from 'react-native';
import styles from "../../styles/MainStyle";
import {Text} from 'react-native';
export default class MyToolbarChatRoom extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <Header style={{backgroundColor: '#ffffff'}}>
                <MyStatusBar/>
                <Left style={{flex:1, flexDirection: 'row'}}>
                    <Button transparent onPress={()=> this.props.navigation.goBack()}>
                        <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
                            <Icon type='Ionicons' name='ios-arrow-back' style={{color: styles.color.serasiColorButton}} />
                        </TouchableOpacity>
                    </Button>
                    <Button transparent >
                        <Thumbnail small source={{uri: this.props.avatarImageUri}} />
                        <Text style={{color: '#323232', marginHorizontal: 9}}>{this.props.avatarName}</Text>
                    </Button>
                </Left>
                <Body style={{flex:1}} />
                <Right style={{flex:1}}/>
            </Header>
        )
    }
}
