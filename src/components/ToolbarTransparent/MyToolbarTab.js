import React, { Component } from 'react';
import {Body, Button, Header, Icon, Left, Right, Title, Badge} from "native-base";
import MyStatusBar from "../StatusBar/MyStatusBar";
import {
    Image, Text, TouchableOpacity, View, ActivityIndicator
} from 'react-native';
import styles from "../../styles/MainStyle";
import MyStatusBarTranslucent from "../StatusBar/MyStatusBarTranslucent";
import variables from "../../variables/MyGlobalVariable";
import TryAgainButtonFetch from "../TryAgainButtonFetch/TryAgainButtonFetch";

export default class MyToolbarTab extends Component {
    constructor(props){
        super(props);
    }

    _tryAgain() {
        //alert('try 2');
        this.props.tryAgainFetchFunc();
    }
    render(){
        let loading_button = null;
        let done_edit_profile = null;
        let edit_button_profile = null;
        let pricing_button = null;
        let pricing_history_button = null;
        let search_icon = null;
        let back_icon = null;
        let proof_button = null;
        let loading_proof_button = null;
        let loading_done_edit_profile_button = null;
        let statusBar = this.props.statusbarType === 'translucent' ? <MyStatusBarTranslucent/> : <MyStatusBar/>;

        if(this.props.showPricingButton){
            pricing_button= (
                <Button transparent >
                    <TouchableOpacity onPress={()=> this.props.onPressPricingButton() }>
                        <Icon name='store' type='FontAwesome5' style={{color: styles.color.serasiColorButton}} />
                    </TouchableOpacity>
                </Button>
            );
        }

        if(this.props.showHistoryPricingButton){
            let badge = (
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        right: 0,
                        paddingHorizontal: 3,
                        backgroundColor: styles.color.serasiColorButton,
                        borderRadius: 9
                    }}>
                    <Text
                        style={{
                            color: '#ffff',
                            fontSize: 9

                        }}>
                        {this.props.badgeCount}
                    </Text>
                </View>
            );
            pricing_history_button= (
                <Button transparent>
                    <TouchableOpacity onPress={()=> this.props.onPressHistoryButton() }>
                        <Icon name='shoppingcart' type='AntDesign' style={{color: styles.color.serasiColorButton}} />
                        {this.props.badgeCount <=0 ? null : badge}
                    </TouchableOpacity>
                </Button>
            );
        }

        if(this.props.showEditButton){
            edit_button_profile= (
                <Button transparent >
                    <TouchableOpacity onPress={()=> this.props.onPressEditButton() }>
                        <Text style={{color: styles.color.serasiColorButton, fontWeight: 'bold'}}>{variables.edit}</Text>
                    </TouchableOpacity>
                </Button>
            );
        }

        if(this.props.searchIconVisible){
            search_icon= (
                <Button transparent >
                    <TouchableOpacity onPress={()=> this.props.onPressSearch(true) }>
                        <Icon name='search' style={{color: styles.color.serasiColorButton}} />
                    </TouchableOpacity>
                </Button>
            );
        }

        if(this.props.showDoneEditButton){
            done_edit_profile= (
                <Button transparent >
                    <TouchableOpacity onPress={()=> this.props.onPressDoneEditButton() }>
                        <Text style={{color: styles.color.serasiColorButton, fontWeight: 'bold'}}>{variables.done}</Text>
                    </TouchableOpacity>
                </Button>
            );
        }

        if(this.props.isLoadingButtonVisible){
            loading_button = (
                <Button transparent >
                    {/* <ActivityIndicator size='small' color={styles.color.serasiColorButton}/> */}
                    <Text>Loading</Text>
                </Button>
            );
        }

        if(this.props.sendProofButtonVisible){
            proof_button= (
                <Button transparent >
                    <TouchableOpacity onPress={()=> this.props.onPressProofButton() }>
                        <Text style={{color: styles.color.serasiColorButton, fontWeight: 'bold'}}>{variables.send}</Text>
                    </TouchableOpacity>
                </Button>
            );

            if(this.props.isLoadingProofButtonVisible){
                loading_proof_button = (
                    <Button transparent >
                        <ActivityIndicator size='small' color={styles.color.serasiColorButton}/>
                    </Button>
                );
            }
        }

        if(this.props.backIconVisible){
            back_icon= (
                <Button transparent onPress={()=> this.props.navigation.goBack()}>
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
                        <Icon type='Ionicons' name='ios-arrow-back' style={{color: styles.color.serasiColorButton}} />
                    </TouchableOpacity>
                </Button>
            );
        }
        return(
            <View>
                <Header style={{backgroundColor: '#ffffff'}}>
                    {statusBar}
                    <Left style={{flex:1, flexDirection: 'row'}}>
                        {back_icon}
                        <Button transparent >
                            <Image
                                source={require('../../images/serasilogo1.png')} style={{height: 26, width: 105, marginLeft: 10}}/>
                        </Button>
                    </Left>
                    <Body style={{flex:1}} />
                    <Right style={{flex:1}}>
                        {search_icon}
                        {edit_button_profile}
                        {pricing_button}
                        {pricing_history_button}
                        {this.props.isLoadingProofButtonVisible ? loading_proof_button : proof_button }
                        {done_edit_profile }
                        {loading_button }
                    </Right>
                </Header>
                {this.props.isFetchFailed ? (<TryAgainButtonFetch onPress={this._tryAgain.bind(this)}/>) : null}
            </View>
        )
    }
}
