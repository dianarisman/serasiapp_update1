import React, { Component } from 'react';
import {Body, Button, Header, Icon, Left, Right, Title} from "native-base";
import MyStatusBar from "../StatusBar/MyStatusBar";
import variables from "../../styles/MainStyle";
import MyStatusBarTranslucent from "../StatusBar/MyStatusBarTranslucent";

export default class MyToolbar extends Component {
    constructor(props){
        super(props);
    }
    render(){
        let statusBar = this.props.statusbarType === 'translucent' ? <MyStatusBarTranslucent/> : <MyStatusBar/>;
        return(
            <Header style={{backgroundColor: '#ffffff'}}>

                {statusBar}
                <Left>
                    <Button transparent onPress={()=> this.props.navigation.goBack()}>
                        <Icon name='arrow-back' style={{color: variables.color.serasiColorButton}} />
                    </Button>
                </Left>
                <Body>
                <Title style={{color: variables.color.serasiColorButton}} >{this.props.title}</Title>
                </Body>
                <Right />
            </Header>
        )
    }
}
