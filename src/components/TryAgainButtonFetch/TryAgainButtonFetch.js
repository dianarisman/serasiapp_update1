import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Text} from "native-base";
import variables from "../../variables/MyGlobalVariable";

export default class TryAgainButtonFetch extends Component {
    render() {
        return (
            <View style={{
                position: 'absolute',
                top:80,
                left: 0,
                right: 0,
                zIndex: 2,
                alignItems: 'center'
            }}
            >
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={{
                        backgroundColor: '#fff',
                        borderRadius: 20,
                        paddingHorizontal: 10,
                        borderWidth: 1,
                        borderColor: '#bfbfbf'
                    }}
                    onPress={()=> this.props.onPress()}>
                    <Text style={{color: '#000', padding: 8}}>{variables.tryAgain}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
