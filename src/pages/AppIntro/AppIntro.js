import React from 'react';
import {StyleSheet, View, Text, StatusBar} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import variables from "../../variables/MyGlobalVariable";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import MainStyle from "../../styles/MainStyle";

export default class AppIntro extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showRealApp: false,
        };
        //StatusBar.setHidden(false);
    }
    _onDone = () => {
        // After user finished the intro slides. Show real app through
        // navigation or simply by controlling state
        this.setState({ showRealApp: true });
        GlobalDataRequest.appIntroSave('false').then(()=>console.log('app intro done'));
        this.props.navigation.replace('Login');
    };
    _onSkip = () => {
        // After user skip the intro slides. Show real app through
        // navigation or simply by controlling state
        this.setState({ showRealApp: true });
        GlobalDataRequest.appIntroSave('false').then(()=>console.log('skip'));
        this.props.navigation.replace('Login');
    };
    render() {
        return (
            <AppIntroSlider
                dotStyle={styles.dotInactiveColor}
                activeDotStyle={styles.dotActiveColor}
                skipLabel={variables.skip}
                nextLabel={variables.next}
                doneLabel={variables.signUpNow}
                slides={slides}
                //comming from the JsonArray below
                onDone={this._onDone}
                //Handler for the done On last slide
                buttonStyle={styles.buttonStyle}
                showSkipButton={false}
                onSkip={this._onSkip}
                bottomButton
            />
        );
    }
}
const styles = StyleSheet.create({
    dotInactiveColor: {
        backgroundColor: '#d8d8d8'
    },
    buttonStyle: {
        backgroundColor: MainStyle.color.serasiColorButton,
        borderRadius: 5
    },
    buttonTextStyle: {
        backgroundColor: '#f44500'
    },

    dotActiveColor: {
        backgroundColor: MainStyle.color.serasiColorButton
    },
    image: {
        width: 300,
        height: 200,
        resizeMode: 'contain'

    },
    text: {
        color: '#717171',
        fontSize: 17,
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#7e7e80',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginTop: 16,
    },
});

const slides = [
    {
        key: 's1',
        text: 'Pasangan hidup Anda mungkin sedang menanti Anda disini!',
        title: 'Temukan Jodoh Anda!',
        titleStyle: styles.title,
        textStyle: styles.text,
        image: require('../../images/slide1.png'),
        imageStyle: styles.image,
        backgroundColor: '#fff',
    },
    {
        key: 's2',
        title: 'Mengatasi masalah Dengan SMART',
        titleStyle: styles.title,
        textStyle: styles.text,
        text: 'Konsultasikan masalah Anda dengan tim yang sudah berpengalaman dibidangnya',
        image: require('../../images/slide2.png'),
        imageStyle: styles.image,
        backgroundColor: '#fff',
    },
    {
        key: 's3',
        title: 'Keamanan Data Diri',
        titleStyle: styles.title,
        textStyle: styles.text,
        text: 'Kami pastikan data diri Anda terjaga dengan aman',
        image: require('../../images/slide3.png'),
        imageStyle: styles.image,
        backgroundColor: '#fff',
    }
];
