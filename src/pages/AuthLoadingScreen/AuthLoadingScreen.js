import React from 'react';
import {
    ActivityIndicator,
    View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import variables from "../../variables/MyGlobalVariable";
import MyStatusBar from "../../components/StatusBar/MyStatusBar";


export default class AuthLoadingScreen extends React.Component {
    static navigationOptions = {
        header: null ,
    };
    constructor(props) {
        super(props);
        this.state = {
            showAppIntro: true
        };

    }

    componentWillMount(): void {
        this._bootstrapAsync();
    }

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem(variables.authToken);
        let value;
        try {
            value = await AsyncStorage.getItem(variables.showAppIntroKey);
            if (value !== null) {
                this.setState(
                    {
                        showAppIntro: value === 'true'
                    }
                )
            }
        } catch (error) {
        }

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.replace(userToken ? 'Home' : (this.state.showAppIntro ? 'AppIntro' : 'Login'));
    };

    // Render any loading content that you like her
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#ffffff' }}>
                <MyStatusBar/>
                <ActivityIndicator />
            </View>
        );
    }
}
