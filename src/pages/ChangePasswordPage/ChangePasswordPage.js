import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Item, Input, List
} from 'native-base';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import {ActivityIndicator, Alert, TouchableOpacity} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
export default class ChangePasswordPage extends Component {
    constructor(props){
        super(props);
        this.state =
            {
                hidePassword: true,
                secureTextEntry: true,
                is_password: true,
                isLoading: false,
                password: "",
                current_password: "",
                passwordConfirm: "",
                user_token: this.props.navigation.getParam('user_token'),
                emptyPassword: false,
                emptyPassword2: false,
                emptyPassword3: false,
                passwordIncorrect: false,

            };
    }

    managePasswordVisibility = () =>
    {
        this.setState({
            hidePassword: !this.state.hidePassword,
            secureTextEntry: !this.state.secureTextEntry,
            is_password: !this.state.is_password,
        });
    };

    _onChangePassword() {

        if(this.state.current_password === "") {
            this.setState({
                emptyPassword: true
            });
        }else {
            this.setState({
                emptyPassword: false
            });
        }

        if(this.state.password === "") {
            this.setState({
                emptyPassword2: true
            });
        }else {
            this.setState({
                emptyPassword2: false
            });
        }

        if(this.state.passwordConfirm === "") {
            this.setState({
                emptyPassword3: true
            });
        }else {
            this.setState({
                emptyPassword3: false
            });
        }


        if(this.state.password !== this.state.passwordConfirm) {
            this.setState({
                passwordIncorrect: true
            });
            return
        }else{
            this.setState({
                passwordIncorrect: false
            });
        }

        if(this.state.current_password === "" || this.state.passwordConfirm === "" || this.state.password === ""){
            return
        }

        this.setState({isLoading: true});
        GlobalDataRequest.changePassword(this.state.user_token, this.state.current_password, this.state.password, this.state.passwordConfirm).then((result)=>{
            this.setState({isLoading: false});
            if(result.success){
                GlobalDataRequest.toastHandler(variables.successChangePassword,'success','top');
                //this.props.navigation.goBack();
                const { navigation } = this.props;
                navigation.goBack();
                navigation.state.params.onEdit();
            }else{
                GlobalDataRequest.toastHandler(variables.failed,'danger','top');
                Alert.alert(variables.warning, result.message);
            }
            //alert(JSON.stringify(result));
        }).catch((err)=> {
            GlobalDataRequest.consoleLog('Result_changePassword',err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({
                isLoading: false
            });
        });
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content contentContainerStyle={{ justifyContent: 'center', flex: 1, marginHorizontal: 30 }}>
                    <List>
                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={{...styles.registerPage.listItemHeaderText, textAlign: 'center', flex: 1}}>
                                {variables.changePassword}
                            </Text>
                        </Item>
                        <Item>
                            <Input
                                disabled={this.state.isLoading}
                                secureTextEntry={ this.state.secureTextEntry}
                                password={ this.state.is_password }
                                placeholder={variables.currentPasswordPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({current_password: text})}
                                value={this.state.current_password}
                            />
                            <TouchableOpacity activeOpacity = { 0.8 } onPress = { this.managePasswordVisibility }>
                                <Icon active name={( this.state.hidePassword ) ? 'eye' : 'eye-off'} />
                            </TouchableOpacity>
                        </Item>
                        {(this.state.emptyPassword) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordEmptyErrorMessage}</Text> : <Text/>}
                        <Item>
                            <Input
                                disabled={this.state.isLoading}
                                secureTextEntry={ this.state.secureTextEntry}
                                password={ this.state.is_password }
                                placeholder={variables.passwordPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({password: text})}
                                value={this.state.password}
                            />
                            <TouchableOpacity activeOpacity = { 0.8 } onPress = { this.managePasswordVisibility }>
                                <Icon active name={( this.state.hidePassword ) ? 'eye' : 'eye-off'} />
                            </TouchableOpacity>
                        </Item>
                        {(this.state.emptyPassword2) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordEmptyErrorMessage}</Text> : <Text/>}
                        <Item>
                            <Input
                                disabled={this.state.isLoading}
                                secureTextEntry={ this.state.secureTextEntry}
                                password={ this.state.is_password }
                                placeholder={variables.passwordConfirmPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({passwordConfirm: text})}
                                value={this.state.passwordConfirm}
                            />
                            <TouchableOpacity activeOpacity = { 0.8 } onPress = { this.managePasswordVisibility }>
                                <Icon active name={( this.state.hidePassword ) ? 'eye' : 'eye-off'} />
                            </TouchableOpacity>
                        </Item>
                        {(this.state.emptyPassword3) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordEmptyErrorMessage}</Text> : <Text/>}
                        {(this.state.passwordIncorrect) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordIncorrectErrorMessage}</Text> : null}
                        <Item style={{borderBottomWidth: 0}}>
                            <TouchableOpacity disabled={this.state.isLoading} activeOpacity={ 0.8 } style={styles.formLogin.buttonItem(styles.color.serasiColorButton)} onPress={() => this._onChangePassword() }>
                                {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.done}</Text> }
                            </TouchableOpacity>
                        </Item>
                    </List>
                </Content>
            </Container>
        );
    }
}
