import React, { Component } from 'react';
import { Container, Content, Icon} from 'native-base';
import styles from "../../styles/MainStyle";
import { View, TouchableOpacity, Image, Text, Dimensions} from "react-native";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import TryAgainButtonFetch from "../../components/TryAgainButtonFetch/TryAgainButtonFetch";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import variables from "../../variables/MyGlobalVariable";
const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');
export default class ChangePhotoProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isFetchFailed: false,
            user_token: this.props.navigation.getParam('user_token', 'undefine'),
            user_id: this.props.navigation.getParam('user_id', 'undefine'),
            imageHeight: 0,
            imageWidth: 0,
            imageData: null,
            imageBase64: '',

        };
    }

    selectPhotoTapped() {
        GlobalDataRequest.takePhotoHandler().then((result) => {
            // You can also display the image using data:
            try {
                const source = {uri: 'data:image/jpeg;base64,' + result.data};
                // const source = { uri: result.uri };
                const ratio = SCREEN_WIDTH / result.width;
                const imgBase64 = 'data:image/jpeg;base64,' + result.data;
                this.setState({
                    isLoading: false,
                    imageHeight: result.height * ratio,
                    imageWidth: SCREEN_WIDTH,
                    imageData: source,
                    imageBase64: imgBase64
                });

            } catch (e) {
                GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            }

        });
    }

    _onPressDoneEditButton() {
        if(this.state.imageBase64.length <= 0){
            GlobalDataRequest.toastHandler(variables.pleaseTakePhotoMessage,'danger','top');
            return;
        }
        this.setState({isLoading: true});
        GlobalDataRequest.changeAvatar(this.state.user_token,this.state.user_id,this.state.imageBase64).then((result)=>{
            console.log('_changeAvatar ', result);
            GlobalDataRequest.toastHandler(variables.welcome,'success','top');
            this.setState({isLoading: true});
            if(result.success){
                this.props.navigation.replace('Home')
            }else{
                GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            }
        }).catch(error=>{
            console.log('error _changeAvatar ', error.message);
            this.setState({isLoading: false});
            //Alert.alert(variables.warning, error.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
        });
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    navigation={this.props.navigation}
                    showDoneEditButton={!this.state.isLoading}
                    isLoadingButtonVisible={this.state.isLoading}
                    onPressDoneEditButton={this._onPressDoneEditButton.bind(this)}
                />
                <Content
                    style={styles.otherStyle.containerMainWhite}
                >
                    <View>
                        <View style={{backgroundColor: '#ffffff', padding: 10}}>
                            <Text
                                style={{fontStyle: 'italic',
                                textAlign: 'center'}}>
                                {variables.uploadeYourPhotoDescription}
                            </Text>
                        </View>
                        <View style={{
                            minHeight: SCREEN_HEIGHT-140,
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center"
                        }}>
                            <TouchableOpacity activeOpacity={0.7} onPress={this.selectPhotoTapped.bind(this)}>
                                {
                                    this.state.imageData ? (<Image source={this.state.imageData}
                                                                   style={{height: this.state.imageHeight,
                                                                       width: this.state.imageWidth}}/>) : (<Icon type='Entypo' name='camera' style={{color: '#ccc', fontSize: (SCREEN_HEIGHT-100)/5}}/>)
                                }
                            </TouchableOpacity>
                        </View>
                    </View>


                </Content>
            </Container>
        );
    }
}
