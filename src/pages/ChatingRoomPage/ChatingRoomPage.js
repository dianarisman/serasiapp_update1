import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import firebase from "../../services/FirebaseConfig";
import {GiftedChat, Time} from "react-native-gifted-chat";
import MyToolbarChatRoom from "../../components/ToolbarTransparent/MyToolbarChatRoom";
import {ActivityIndicator} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import variables from "../../variables/MyGlobalVariable";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';

export default class ChatConversation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //email: this.props.navigation.getParam('email'),
            lastMessageText: '',
            user_id: this.props.navigation.getParam('user_id'),
            user_avatar: '',
            user_name: '',
            to_id: this.props.navigation.getParam('to_id'),
            to_avatar: this.props.navigation.getParam('to_avatar'),
            to_name: this.props.navigation.getParam('to_name'),
            to_fcm_token: this.props.navigation.getParam('to_fcm_token', 'fcm_null'),
            messages: [],
            fcm_token: ''
        };
    }

    componentWillMount() {
        AsyncStorage.getItem(variables.fcm_token_KEY, (err, item) => {
            console.log('fcm_token_KEY ', item);
            this.setState({
                fcm_token: item
            })
        });
        AsyncStorage.getItem(variables.user_avatar_KEY, (error, result) => {
            if (result) {
                this.setState({
                    user_avatar: result
                });
            }
        });

        AsyncStorage.getItem(variables.user_name_KEY, (error, result) => {
            if (result) {
                this.setState({
                    user_name: result
                });
            }
        });
    }

    fromUID() {
        return this.state.user_id
    }

    toUID() {
        return this.state.to_id
    }

    get ref_FROM() {
        return firebase.database().ref('Chating_Room/Users/'+this.fromUID()+'/to/'+this.toUID()+'/messages/');
    }

    get ref_TO() {
        return firebase.database().ref('Chating_Room/Users/'+this.toUID()+'/to/'+this.fromUID()+'/messages/');
    }

    get ref_chat_list_from() {
        return firebase.database().ref('Chating_Room/Users/'+this.fromUID()+'/chat_list/');
    }

    get ref_chat_list_to() {
        return firebase.database().ref('Chating_Room/Users/'+this.toUID()+'/chat_list/');
    }

    parse = snapshot => {
        const { timestamp: numberStamp, createdAt, text, user, sent, received } = snapshot.val();
        const { key: _id } = snapshot;
        const timestamp = new Date(numberStamp);
        const message = {
            _id,
            timestamp,
            createdAt,
            text,
            sent,
            received,
            user,
        };
        return message;
    };

    on = callback =>
        this.ref_FROM
            .limitToLast(10000)
            .on('child_added', snapshot => callback(this.parse(snapshot)));

    onChanged = callback =>
        this.ref_FROM
            .limitToLast(10000)
            .on('child_changed', snapshot => callback(this.parse(snapshot)));

    timestamp() {
        return new Date().getTime();
    }
    // send the message to the Backend
    send = messages => {
        //alert(JSON.stringify(messages));
        for (let i = 0; i < messages.length; i++) {
            const { text, user, _id: message_id } = messages[i];
            const createdAt = this.timestamp();
            const sent = true;
            const received = false;
            const timestamp = this.timestamp();
            const to_user_id = this.toUID().toString();
            const from_user_id = this.fromUID().toString();
            const message = {
                to_user_id,
                from_user_id,
                text,
                user,
                message_id,
                createdAt,
                sent,
                received,
                timestamp
            };
            this.append(message);
        }
    };

    append(message){
        GlobalDataRequest.sendNotif(this.state.user_name, message.text, this.state.to_fcm_token, this.fromUID()).then((result)=>{
            console.log('success send notif ', result);
        }).catch(error=>{
            console.log('error send notif ', error);
        });
        const from = {
            user_id: this.fromUID().toString(),
            avatar: this.state.user_avatar,
            name: this.state.user_name,
            text: message.text,
            fcm_token: this.state.fcm_token,
            me: false,
            timestamp: this.timestamp(),
            received: false,
            sent: true
            //user_email: this.state.email
        };

        const to = {
            user_id: this.toUID().toString(),
            avatar: this.state.to_avatar,
            name: this.state.to_name,
            text: message.text,
            fcm_token: this.state.to_fcm_token,
            me: true,
            timestamp: this.timestamp(),
            received: false,
            sent: true
            //user_email: this.state.dataItem.name
        };

        this.ref_chat_list_from.orderByChild("user_id").equalTo(to.user_id).once("value",snapshot => {
            if (!snapshot.exists()){
                this.ref_chat_list_from.push(to);
            }else{
                this.ref_chat_list_from.orderByChild("user_id").equalTo(to.user_id).once("child_added",snapshot => {
                    snapshot.ref.update(to)
                });
            }
        });

        this.ref_chat_list_to.orderByChild("user_id").equalTo(from.user_id).once("value",snapshot => {
            if (!snapshot.exists()){
                this.ref_chat_list_to.push(from);
            }else{
                this.ref_chat_list_to.orderByChild("user_id").equalTo(from.user_id).once("child_added",snapshot => {
                    snapshot.ref.update(from)
                });
            }
        });

        /*this.ref_chat_list_from.orderByChild("user_id").equalTo(to.user_id).once("child_added",snapshot => {
            snapshot.ref.update(to)
        });

        this.ref_chat_list_to.orderByChild("user_id").equalTo(from.user_id).once("child_added",snapshot => {
            snapshot.ref.update(from)
        });*/

        this.ref_FROM.push(message);
        this.ref_TO.push(message);
    }

    // close the connection to the Backend
    off() {
        this.ref_FROM.off();
        this.ref_TO.off();
        //this.ref_chat_list_from.off();
        //this.ref_chat_list_to.off();
    }

    get user() {
        return {
            //name: this.state.email,
            _id: this.state.user_id,
        };
    }


    render() {
        //alert(this.state.to_avatar+'\n'+this.state.to_name);
        return (
            <Container>
                <MyToolbarChatRoom
                    avatarImageUri={this.state.to_avatar}
                    avatarName={this.state.to_name}
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <GiftedChat
                    //renderLoadEarlier={this.renderLoadEarlier}
                    //isLoadingEarlier={this.state.isLoadingEarlier}
                    //loadEarlier={!this.state.isLoadingEarlier}
                    messages={this.state.messages}
                    onSend={this.send}
                    user={this.user}
                    showUserAvatar={false}
                    renderAvatar={null}
                    //renderMessage={this.renderMessage}
                />
            </Container>
        );
    }

    componentDidMount() {
        this.on(message =>
            this.setState(previousState => ({
                messages: GiftedChat.append(previousState.messages, message),
            }))
        );

        this.onChanged(message => {
            this.setState(previousState => ({
                messages: this._replaceAt(
                    previousState.messages,
                    previousState.messages.findIndex(x => x._id === message._id),
                    message
                )
            }));
        });

        this.ref_TO.orderByChild("to_user_id").equalTo(this.fromUID().toString()).on("child_added",snapshot => {
            snapshot.ref.child('received').set(true);
        });

        this.ref_chat_list_to.orderByChild("user_id").equalTo(this.fromUID().toString()).once("child_added",snapshot => {
            snapshot.ref.child('received').set(true);
        });
    }

    _replaceAt(array, index, value) {
        const ret = array.slice(0);
        ret[index] = value;
        return ret;
    }

    componentWillUnmount() {
        this.off();
    }
}
