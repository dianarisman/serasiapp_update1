import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Alert,
    Modal,
    FlatList
} from 'react-native';
import {
    Container,
    Content,
    Button,
    Left,
    Icon,
    Text,
    Input, Segment, Item, List, ListItem, Picker, Label
} from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import GlobalDataRequest from "../../services/GlobalDataRequest";
import variables from "../../variables/MyGlobalVariable";
import styles from "../../styles/MainStyle";
import moment from 'moment'
import ModalSelectCriteria from "../../components/ModalSelectCriteria/ModalSelectCriteria";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";

/*class ItemList extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <View style={{height: 50, justifyContent: "center"}}>
                <Text>{this.props.title}</Text>
            </View>
        )
    }

}*/
export default class EditProfilePage extends Component {
// noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: null ,
    };

    constructor(props){
        super(props);

        this.state = {
            testCriteria: [],
            user_token: this.props.navigation.getParam('user_token'),
            emailInput: null,
            phoneInput: null,
            imageHeightKTP: null,
            imageWidthKTP: null,
            imageDataKTP: null,
            imageHeightSELFIE: null,
            imageWidthKTPSELFIE: null,
            imageDataKTPSELFIE: null,
            modalVisiblePasangan:false,
            isDateTimePickerVisible: false,
            birthDate: '',  //input
            placeOfBirth: null,   //input
            hobbyInput: null,
            aboutInput: null, //inputt
            statusMarital: undefined,   //input
            statusmaritalNoSelect: false,
            modalVisibleDiri:false,
            modalVisibleCity: false,
            list: [ "test1", "test2", "test3" ],
            listCriteriaDiri: variables.criteria_list,
            listCriteriaPasangan: variables.criteria_list,
            listCity: variables.cityList,
            citySearchText: undefined,
            selectCriteriaPasangan: [], //input
            selectCriteriaDiri: [], //input
            selectCity: null,   //input
            tester: 0,
            invalidNik: false,
            identityCard: null, //input
            addressInput: null, //input
            fullName: null, //input
            genderSelect: null, //input
            jobSelect: undefined,   //input
            jobNoSelect: false,
            nationalityNoSelect: false,
            educationSelect: null,
            educationNoSelect: false,
            religionSelect: undefined,  //input
            religionNoSelect: false,
            yourOrderInFamily: undefined,   //input anak ke
            numberOfSiblings: undefined, //input jumlah saudara
            my_profile: this.props.navigation.getParam('dataDetailProfile'),
            selectCriteriaDiri_holder: [],
            selectCriteriaPasangan_holder: [],
            test_looping_kriteria: null

        };
        this.arrayHolderCity = variables.cityList;

        //this.getImageDimensions = this.getImageDimensions.bind(this);
    }

    _nullStringHandler(handlerData, handlerString){
        let data = handlerData.trim();
        return (data === 'null' || data === '' || data.length < 1 || !data) ? handlerString : data;
    }

    _onPressDoneEditButton() {
        this.setState({isLoading: true});
        const {
            no_anggota,
            user_id,
            emailInput,
            phoneInput,
            statusMarital,
            identityCard,
            images,
            fullName,
            genderSelect,
            religionSelect,
            placeOfBirth,
            birthDate,
            hobbyInput,
            educationSelect,
            aboutInput,
            jobSelect,
            addressInput,
            selectCity,
            selectProvince,
            //selectCriteriaDiri,
            selectCriteriaPasangan,
            yourOrderInFamily,
            numberOfSiblings,
            bodyHeightInput,
            bodyWeightInput,
            nationalitySelect,
            numberOfDependents,
            numberOfChildren
        } = this.state.my_profile;

        const {
            selectCriteriaDiri_holder,
            selectCriteriaPasangan_holder
        } = this.state;
        const input2 = {
            no_anggota: no_anggota,
            nik: identityCard,
            name: fullName,
            gender: genderSelect,
            agama: religionSelect,
            birth_place: placeOfBirth,
            birth_date: birthDate,
            kewarganegaraan: nationalitySelect,
            anak_ke: yourOrderInFamily,
            jml_saudara: numberOfSiblings,
            tb: bodyHeightInput,
            bb: bodyWeightInput,
            pendidikan: educationSelect,
            pekerjaan: jobSelect,
            hobby: hobbyInput,
            status_marital: statusMarital,
            jml_anak: numberOfChildren,
            jml_tanggungan: numberOfDependents,
            address: addressInput,
            city: selectCity,
            criteria_diri: selectCriteriaDiri_holder.length === 0 ? [] : selectCriteriaDiri_holder,
            criteria_pasangan: selectCriteriaPasangan_holder.length === 0 ? [] : selectCriteriaPasangan_holder,
            provinsi: this._nullStringHandler(selectProvince, null),
            phone: phoneInput,
            description: aboutInput,
        };

        //alert(JSON.stringify(input2));
        //this.setState({isLoading: true});
        if(!this.checkProperties(input2) || this.state.invalidNik){
            this.setState({isLoading: false});
            GlobalDataRequest.toastHandler(variables.dataNotValid,'danger','top');
            return
        }

        GlobalDataRequest.updateProfile(this.state.user_token, this.state.my_profile.user_id, input2).then((result)=>{
            this.setState({isLoading: false});
            if(result.success){
                GlobalDataRequest.toastHandler(variables.successEditProfileMessage,'success','top');
                //this.props.navigation.goBack();
                const { navigation } = this.props;
                navigation.goBack();
                navigation.state.params.onEdit();
            }else{
                Alert.alert(variables.warning, ' Ada kesalahan, cek koneksi anda atau Mohon kontak admin '+JSON.stringify(result));
            }
        }, () => {
            this.setState({isLoading: false});
            alert('cant request to server');
        });

    }

    componentDidMount(): void {
        this.initCriteriaDiri();
        this.initCriteriaPasangan();
    }

    initCriteriaDiri() {
        let listCriteriaDiri = this.state.listCriteriaDiri;
        let dataSelectedCriteriaDiri = this.state.my_profile.selectCriteriaDiri;
        let criteria = [ ...listCriteriaDiri ];
        let selectCriteriaDiri_array = [];
        dataSelectedCriteriaDiri.map( (data, indexSelected) => {
            selectCriteriaDiri_array.push(data.criteria_name);
            const findIndex = listCriteriaDiri.findIndex(
                item => item.title.toLowerCase() === data.criteria_name.toLowerCase()
            );
            criteria[findIndex] = {...criteria[findIndex], is_selected: true};
        });

        this.setState({
            listCriteriaDiri: criteria,
            selectCriteriaDiri_holder: selectCriteriaDiri_array
        });
        //alert(JSON.stringify(selectCriteriaDiri_array));
        //alert(JSON.stringify(this.state.testCriteria));
    }

    initCriteriaPasangan() {
        let listCriteriaPasangan = [ ...this.state.listCriteriaPasangan ];
        let dataSelectedCriteriaPasangan = this.state.my_profile.selectCriteriaPasangan;
        let criteria = [ ...listCriteriaPasangan ];
        let selectCriteriaPasangan_array = [];
        dataSelectedCriteriaPasangan.map( (data, indexSelected) => {
            selectCriteriaPasangan_array.push(data.criteria_name);
            const findIndex = listCriteriaPasangan.findIndex(
                item => item.title.toLowerCase() === data.criteria_name.toLowerCase()
            );
            criteria[findIndex] = {...criteria[findIndex], is_selected: true};
        });

        this.setState({
            listCriteriaPasangan: criteria,
            selectCriteriaPasangan_holder: selectCriteriaPasangan_array
        });
    }

    _showModalKriteriaDiri(visible) {
        this.setState({
            modalVisibleDiri: visible
        });
    }
    _getSelectedCriteriaDiri(dataList) {
        /*
        const data = [];
        const criteria = dataList;
        for(let i=0;i<criteria.length;i++){
            if (criteria[i].is_selected){
                data.push(criteria[i]);
            }
        }
        this.setState({
            selectCriteriaDiri: data
        });*/

        let selectCriteriaDiri_array = [];
        this.state.listCriteriaDiri.filter(item => item.is_selected).forEach((data, i) => {
            selectCriteriaDiri_array.push(data.title);
        });

        this.setState({
            selectCriteriaDiri_holder: selectCriteriaDiri_array,

        });
    }



    _selectCriteriaDiri(indexSelect, dataList) {
        let criteria = [ ...dataList ];
        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }
        this.setState({
            listCriteriaDiri: criteria
        });
        console.log(criteria)
    }

    _showModalKriteriaPasangan(visible) {
        this.setState({
            modalVisiblePasangan: visible
        });
    }
    _getSelectedCriteriaPasangan(dataList) {
        /*const data = [];
        const criteria = dataList;
        for(let i=0;i<criteria.length;i++){
            if (criteria[i].is_selected){
                data.push(criteria[i]);
            }
        }
        this.setState({
            selectCriteriaPasangan: data
        });*/

        let selectCriteriaPasangan_array = [];
        this.state.listCriteriaPasangan.filter(item => item.is_selected).forEach((data, i) => {
            selectCriteriaPasangan_array.push(data.title);
        });

        this.setState({
            selectCriteriaPasangan_holder: selectCriteriaPasangan_array
        });
    }

    _selectCriteriaPasangan(indexSelect, dataList) {
        let criteria = [ ...dataList ];
        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }
        this.setState({
            listCriteriaPasangan: criteria
        });
    }

    _showModalCity(visible) {
        this.setState({
            modalVisibleCity: visible
        });
    }

    _searchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayHolderCity.filter(function(item) {
            const itemData = `${item.city.toUpperCase()}`;
            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            listCity: newData,
            citySearchText: text,
        });
    }

    _selectCity(indexSelect, selectItem) {
        let listCity = [ ...this.state.listCity ];
        this._showModalCity(!this.state.modalVisibleCity);
        this.setState({
            my_profile: {
                ...this.state.my_profile, selectCity: listCity[indexSelect].city
            },
            listCity: listCity,
        });
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        this.setState({
            my_profile: {
                ...this.state.my_profile, birthDate: moment(date).format('YYYY-MM-DD')
            }

        });
        this._hideDateTimePicker();
    };

    selectGender(value) {
        this.setState({ my_profile:{...this.state.my_profile, genderSelect: value}});
    }

    selectJob(value: string) {
        let noSelect = value === variables.job_list[0].name;
        this.setState({
            jobNoSelect: noSelect,
            my_profile: {
                ...this.state.my_profile, jobSelect: value
            }

        });
    }

    selectNationality(value: string) {
        let noSelect = value === variables.nationality_list[0].name;
        this.setState({
            nationalityNoSelect: noSelect,
            my_profile: {
                ...this.state.my_profile, nationalitySelect: value
            }

        });

    }

    selectMarital(value: string) {
        let statusmaritalNoSelect = value === variables.statusMaritalList[0].name;
        this.setState({
            statusmaritalNoSelect: statusmaritalNoSelect,
            my_profile: {
                ...this.state.my_profile, statusMarital: value
            }
        });
    }

    selectEducation(value: string) {
        let educationNoSelect = value === variables.education_list[0].name;
        this.setState({
            educationNoSelect: educationNoSelect,
            my_profile: {
                ...this.state.my_profile, educationSelect: value
            }

        });
    }

    selectRelegion(value: string) {
        let noSelect = value === variables.religion_list[0].name;
        this.setState({
            religionNoSelect: noSelect,
            my_profile: {
                ...this.state.my_profile, religionSelect: value
            }
        });
    }

    checkNik(text){
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            } else {
                // your call back function
            }
        }

        var fixText = newText;
        if(newText !== '0') {
            fixText = newText.replace(/^(0+)/g, '');
        }
        if((fixText+'').length < 16 || (fixText+'').length >= 17 ) {
            this.setState({
                invalidNik: true
            })
        }else{
            this.setState({
                invalidNik: false
            })
        }
    }

    _numberOnly(text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            } else {
                // your call back function
                Alert.alert(null, variables.numberOnlyErrorMessage);
            }
        }

        var fixText = newText;
        if(newText !== '0') {
            fixText = newText.replace(/^(0+)/g, '');
        }
        return fixText;
    }

    checkProperties(obj) {

        //let empty_no_anggota = obj.no_anggota === null || obj.no_anggota === '' || obj.no_anggota === 'null';
        let empty_nik = obj.nik === null || obj.nik === '' || obj.nik === 'null';
        let empty_name = obj.name === null || obj.name === '' || obj.name === 'null';
        let empty_gender = obj.gender === null || obj.gender === '' || obj.gender === 'null';
        let empty_agama = obj.agama === null || obj.agama === '' || obj.agama === 'null';
        let empty_birth_place = obj.birth_place === null || obj.birth_place === '' || obj.birth_place === 'null';
        let empty_birth_date = obj.birth_date === null || obj.birth_date === '' || obj.birth_date === 'null';
        let empty_kewarganegaraan = obj.kewarganegaraan === null || obj.kewarganegaraan === '' || obj.kewarganegaraan === 'null';
        let empty_anak_ke = obj.anak_ke === null || obj.anak_ke === '' || obj.anak_ke === 'null';
        let empty_jml_saudara = obj.jml_saudara === null || obj.jml_saudara === '' || obj.jml_saudara === 'null';
        let empty_tb = obj.tb === null || obj.tb === '' || obj.tb === 'null';
        let empty_bb = obj.bb === null || obj.bb === '' || obj.bb === 'null';
        let empty_pendidikan = obj.pendidikan === null || obj.pendidikan === '' || obj.pendidikan === 'null';
        let empty_pekerjaan = obj.pekerjaan === null || obj.pekerjaan === '' || obj.pekerjaan === 'null';
        let empty_hobby = obj.hobby === null || obj.hobby === '' || obj.hobby === 'null';
        let empty_status_marital = obj.status_marital === null || obj.status_marital === '' || obj.status_marital === 'null';
        let empty_jml_anak = obj.jml_anak === null || obj.jml_anak === '' || obj.jml_anak === 'null';
        let empty_jml_tanggungan = obj.jml_tanggungan === null || obj.jml_tanggungan === '' || obj.jml_tanggungan === 'null';
        let empty_address = obj.address === null || obj.address === '' || obj.address === 'null';
        let empty_city = obj.city === null || obj.city === '' || obj.city === 'null';
        //let empty_provinsi = obj.provinsi === null || obj.provinsi === '' || obj.provinsi === 'null';
        let empty_phone = obj.phone === null || obj.phone === '' || obj.phone === 'null';
        let empty_description = obj.description === null || obj.description === '' || obj.description === 'null';

        const input = {
            empty_nik: empty_nik,
            empty_name: empty_name,
            empty_gender: empty_gender,
            empty_agama: empty_agama,
            empty_birth_place: empty_birth_place,
            empty_birth_date: empty_birth_date,
            empty_kewarganegaraan: empty_kewarganegaraan,
            empty_anak_ke: empty_anak_ke,
            empty_jml_saudara: empty_jml_saudara,
            empty_tb: empty_tb,
            empty_bb: empty_bb,
            empty_pendidikan: empty_pendidikan,
            empty_pekerjaan: empty_pekerjaan,
            empty_hobby: empty_hobby,
            empty_status_marital: empty_status_marital,
            empty_jml_anak: empty_jml_anak,
            empty_jml_tanggungan: empty_jml_tanggungan,
            empty_address: empty_address,
            empty_city: empty_city,
            empty_phone: empty_phone,
            empty_description: empty_description
        };
        this.setState({
            empty_nik: empty_nik,
            empty_name: empty_name,
            empty_gender: empty_gender,
            empty_agama: empty_agama,
            empty_birth_place: empty_birth_place,
            empty_birth_date: empty_birth_date,
            empty_kewarganegaraan: empty_kewarganegaraan,
            empty_anak_ke: empty_anak_ke,
            empty_jml_saudara: empty_jml_saudara,
            empty_tb: empty_tb,
            empty_bb: empty_bb,
            empty_pendidikan: empty_pendidikan,
            empty_pekerjaan: empty_pekerjaan,
            empty_hobby: empty_hobby,
            empty_status_marital: empty_status_marital,
            empty_jml_anak: empty_jml_anak,
            empty_jml_tanggungan: empty_jml_tanggungan,
            empty_address: empty_address,
            empty_city: empty_city,
            empty_phone: empty_phone,
            empty_description: empty_description
        });

        for(let key in input){
            if(input[key]) {
                console.log(key + ' masih kosong');
                return false;
            }
        }
        return true;
        /*this._next();
        return false;*/
    }

    _next() {
        Alert.alert('Warning',
            'DATA AKUN'+'\n'+
            '-Email : '+ this.state.my_profile.emailInput +'\n'+
            '-Phone : '+ this.state.my_profile.phoneInput +'\n'+
            'DATA DIRI'+'\n'+
            '-Nomor ktp : '+ this.state.my_profile.identityCard +'\n'+
            '-Nama Lengkap : '+ this.state.my_profile.fullName +'\n'+
            '-Jenis Kelamin : '+ this.state.my_profile.genderSelect +'\n'+
            '-Agama : '+ this.state.my_profile.religionSelect +'\n'+
            '-Tempat Lahir : '+ this.state.my_profile.placeOfBirth +'\n'+
            '-Tanggal Lahir : '+ this.state.my_profile.birthDate +'\n'+
            '-Tinggi Badan : '+ this.state.my_profile.bodyHeightInput +'\n'+
            '-Berat Badan : '+ this.state.my_profile.bodyWeightInput +'\n'+
            '-Pendidikan : '+ this.state.my_profile.educationSelect +'\n'+
            '-Hobi : '+ this.state.my_profile.hobbyInput +'\n'+
            '-About : '+ this.state.my_profile.aboutInput +'\n'+
            '-Status Perkawinan : '+ this.state.my_profile.statusMarital +'\n'+
            '-Pekerjaan : '+ this.state.my_profile.jobSelect +'\n'+
            '-Alamat : '+ this.state.my_profile.addressInput +'\n'+
            '-City : '+ this.state.my_profile.selectCity +'\n'+
            '-Kriteria diri : '+ JSON.stringify(this.state.my_profile.selectCriteriaDiri) +'\n'+
            '-Kriteria pasangan : '+ JSON.stringify(this.state.my_profile.selectCriteriaPasangan) +'\n'+
            '-Anak ke: '+ this.state.my_profile.yourOrderInFamily +'\n'+
            '-Jumlah Saudara : '+ this.state.my_profile.numberOfSiblings +'\n'+
            '-Jumlah Anak : '+ this.state.my_profile.numberOfChildren +'\n'+
            '-Jumlah Tanggungan : '+ this.state.my_profile.numberOfDependents +'\n'
        );
    }
    render() {
        let nationality_list = variables.nationality_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let job_list = variables.job_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let religion_list = variables.religion_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let marital_list = variables.statusMaritalList.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let education_list = variables.education_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        const {
            selectCriteriaDiri_holder,
            selectCriteriaPasangan_holder
        } = this.state;

        const {
            no_anggota,
            user_id,
            emailInput,
            phoneInput,
            statusMarital,
            identityCard,
            images,
            fullName,
            genderSelect,
            religionSelect,
            placeOfBirth,
            birthDate,
            hobbyInput,
            educationSelect,
            aboutInput,
            jobSelect,
            addressInput,
            selectCity,
            selectProvince,
            selectCriteriaDiri,
            selectCriteriaPasangan,
            yourOrderInFamily,
            numberOfSiblings,
            bodyHeightInput,
            bodyWeightInput,
            nationalitySelect,
            numberOfDependents,
            numberOfChildren
        } = this.state.my_profile;

        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                    showDoneEditButton={!this.state.isLoading}
                    isLoadingButtonVisible={this.state.isLoading}
                    onPressDoneEditButton={this._onPressDoneEditButton.bind(this)}
                />
                <Content contentContainerStyle={{ marginHorizontal: 30 }}>
                    {/*<Text>identityCard {identityCard}</Text>
                    <Text>fullName {fullName}</Text>
                    <Text>genderSelect {genderSelect}</Text>
                    <Text>religionSelect {religionSelect}</Text>
                    <Text>placeOfBirth {placeOfBirth}</Text>
                    <Text>birthDate {moment(birthDate).format("YYYY-MM-DD")}</Text>
                    <Text>educationSelect {educationSelect}</Text>
                    <Text>hobbyInput {hobbyInput}</Text>
                    <Text>aboutInput {aboutInput}</Text>
                    <Text>statusMarital {statusMarital}</Text>
                    <Text>jobSelect {jobSelect}</Text>
                    <Text>addressInput {addressInput}</Text>
                    <Text>selectCity {selectCity}</Text>
                    <Text>selectCriteriaDiri {JSON.stringify(selectCriteriaDiri)}</Text>
                    <Text>selectCriteriaPasangan {JSON.stringify(selectCriteriaPasangan)}</Text>
                    <Text>yourOrderInFamily {JSON.stringify(yourOrderInFamily)}</Text>
                    <Text>numberOfSiblings {JSON.stringify(numberOfSiblings)}</Text>*/}
                    <ModalSelectCriteria
                        selectTitle={variables.kriteriaDiri}
                        isModalVisible={this.state.modalVisibleDiri}
                        onSelectItem={this._selectCriteriaDiri.bind(this)}
                        onHideModal={this._showModalKriteriaDiri.bind(this)}
                        onGetSelectedItem={this._getSelectedCriteriaDiri.bind(this)}
                        listCriteria={this.state.listCriteriaDiri}
                    />

                    <ModalSelectCriteria
                        selectTitle={variables.kriteriaPasangan}
                        isModalVisible={this.state.modalVisiblePasangan}
                        onSelectItem={this._selectCriteriaPasangan.bind(this)}
                        onHideModal={this._showModalKriteriaPasangan.bind(this)}
                        onGetSelectedItem={this._getSelectedCriteriaPasangan.bind(this)}
                        listCriteria={this.state.listCriteriaPasangan}
                    />

                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisibleCity}
                        onRequestClose={() => {
                            //Alert.alert('Modal has been closed.');
                            this._showModalCity(!this.state.modalVisibleCity);
                        }}>
                        <View style={{flex: 1,  marginHorizontal: 30,marginTop: 30}}>

                            <Item>
                                <Icon type='FontAwesome' name='search' style={{color: styles.color.serasiColorButton}}/>
                                <Input
                                    placeholder={variables.findCityPlaceholder}
                                    placeholderTextColor={styles.color.placeholderInput}
                                    onChangeText={text => this._searchFilterFunction(text)}
                                    autoCorrect={false}
                                    value={this.state.citySearchText}
                                />
                            </Item>
                            <FlatList
                                data={this.state.listCity}
                                keyExtractor={item => item.id.toString()}
                                renderItem={({item, index}) =>
                                    <ListItem onPress={()=> this._selectCity(index, item.id)}>
                                        <Left>
                                            <Text>{item.city}</Text>
                                        </Left>
                                    </ListItem>}
                            />
                        </View>
                    </Modal>

                    <List>
                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={styles.registerPage.listItemHeaderText}>
                                Account Detail
                            </Text>
                        </Item>
                        <Item>
                            <Label>{variables.emailPlaceholder}</Label>
                            <Input
                                disable={true}
                                editable={false}
                                keyboardType='email-address'
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, emailInput: text
                                    }
                                })}
                                value={emailInput}
                            />
                        </Item>
                        <Item>
                            <Label>{variables.phonePlaceholder}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, phoneInput: text
                                    }
                                })}
                                value={phoneInput}
                            />
                        </Item>
                        {(this.state.empty_phone) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={styles.registerPage.listItemHeaderText}>
                                {variables.dataDiri}
                            </Text>
                        </Item>
                        <Item>
                            <Label>{variables.identityCardPlaceholder}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.identityCardPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, identityCard: this._numberOnly(text)
                                    }
                                },()=>{
                                    this.checkNik(text);
                                })}
                                value={identityCard.toString()}
                            />
                        </Item>
                        {(this.state.invalidNik) ? <Text style={{ color: styles.color.errorInput}}>{variables.invalidNikMessage}</Text> : null}
                        {(this.state.empty_nik) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.fullnamePlaceholder}</Label>
                            <Input
                                editable={false}
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({ my_profile:{...this.state.my_profile, fullName: text}})}
                                value={fullName}
                            />
                        </Item>
                        {(this.state.empty_name) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <View style={styles.loginPage.content_bottom}>
                                <View style={styles.loginPage.leftContainer}>
                                    <Label>{variables.gender}</Label>
                                </View>
                                <View style={styles.loginPage.rightContainer}>
                                    <Segment
                                        style={{backgroundColor: null}}
                                    >
                                        <Button
                                            disabled={true}
                                            style={styles.loginPage.segmentButtonFirst((genderSelect === 'pria'))}
                                            onPress={() => this.selectGender('pria')}
                                        ><Text style={styles.loginPage.segmentTextActive(genderSelect === 'pria')}>{variables.male}</Text></Button>
                                        <Button
                                            disabled={true}
                                            style={styles.loginPage.segmentButtonLast((genderSelect === 'wanita'))}
                                            onPress={() => this.selectGender('wanita')}
                                        ><Text style={styles.loginPage.segmentTextActive(genderSelect === 'wanita')}>{variables.female}</Text>
                                        </Button>
                                    </Segment>
                                </View>
                            </View>
                        </Item>
                        {(this.state.empty_gender) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Label>{variables.religionPlaceholder}</Label>
                            <Picker
                                disabled={this.state.isLoading}
                                mode="dialog"
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.religionPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.religionNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={religionSelect}
                                onValueChange={this.selectRelegion.bind(this)}
                            >
                                {religion_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_agama) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.placeOfBirth}</Label>
                            <Input
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, placeOfBirth: text
                                    }

                                })}
                                value={placeOfBirth}
                            />
                        </Item>
                        {(this.state.empty_birth_place) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item onPress={this._showDateTimePicker}>
                            <Label>{variables.birthDatePlaceholder}</Label>
                            <Input
                                editable={false}
                                placeholder={variables.birthDatePlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                value={moment(birthDate).format("YYYY-MM-DD")}
                            />
                            <Icon type='Entypo' name='calendar' style={{color: styles.color.placeholderInput}}/>
                            <DateTimePicker
                                datePickerModeAndroid='spinner'
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this._handleDatePicked}
                                onCancel={this._hideDateTimePicker}
                            />
                        </Item>
                        {(this.state.empty_birth_date) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.bodyHeight}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, bodyHeightInput: this._numberOnly(text)
                                    }
                                })}
                                value={bodyHeightInput}
                            />
                        </Item>
                        {(this.state.empty_tb) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.bodyWeight}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, bodyWeightInput: this._numberOnly(text)
                                    }
                                })}
                                value={bodyWeightInput}
                            />
                        </Item>
                        {(this.state.empty_bb) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Label>{variables.educationPlaceholder}</Label>
                            <Picker
                                disabled={this.state.isLoading}
                                mode="dialog"
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                //placeholder={variables.educationPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.educationNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={educationSelect}
                                onValueChange={this.selectEducation.bind(this)}
                            >
                                {education_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_pendidikan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.hobbyPlaceholder}</Label>
                            <Input
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, hobbyInput: text
                                    }

                                })}
                                value={hobbyInput}
                            />
                        </Item>
                        {(this.state.empty_hoby) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.about}</Label>
                            <Input
                                multiline={true}
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, aboutInput: text
                                    }
                                })}
                                value={aboutInput}
                            />
                        </Item>
                        {(this.state.empty_description) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            {/**<Input disabled={true} value={variables.statusMarital}/>**/}
                            <Label>{variables.statusMaritalPlaceholder}</Label>
                            <Picker
                                mode="dialog"
                                disabled={this.state.isLoading}
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                //placeholder={variables.statusMaritalPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.statusmaritalNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={statusMarital}
                                onValueChange={this.selectMarital.bind(this)}
                            >
                                {marital_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_status_marital) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}


                        <Item>
                            <Label>{variables.selectJobPlaceholder}</Label>
                            <Picker
                                mode="dialog"
                                disabled={this.state.isLoading}
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                //placeholder={variables.selectJobPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.jobNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={jobSelect}
                                onValueChange={this.selectJob.bind(this)}
                            >
                                {job_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_pekerjaan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Label>{variables.adressPlaceholder}</Label>
                            <Input
                                multiline={true}
                                disabled={this.state.isLoading}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, addressInput: text
                                    }

                                })}
                                value={addressInput}
                            />
                        </Item>
                        {(this.state.empty_address) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <View style={{flex: 1,
                                height: 50,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between'}}>
                                <Label>{variables.city}</Label>
                                <TouchableOpacity  style={styles.loginPage.rightContainer} onPress={() => {
                                    this._showModalCity(true);
                                }}>
                                    <Text style={{color: styles.color.placeholderInput}}>{this.state.my_profile.selectCity === null ? variables.select : selectCity} </Text><Icon type="AntDesign" name="search1" style={{fontSize:10, color: styles.color.placeholderInput}}/>
                                </TouchableOpacity>
                            </View>
                        </Item>
                        {(this.state.empty_city) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Label>{variables.nationality}</Label>
                            <Picker
                                disabled={this.state.isLoading}
                                mode="dialog"
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.religionPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.nationalityNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={nationalitySelect}
                                onValueChange={this.selectNationality.bind(this)}
                            >
                                {nationality_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_kewarganegaraan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        {/*<Item>
                            <View style={{flex: 1,
                                height: 50,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between'}}>
                                <Label>{variables.kriteriaDiri}</Label>
                                <TouchableOpacity  style={styles.loginPage.rightContainer} onPress={()=>this._showModalKriteriaDiri(true)}>
                                    <Text style={{color: styles.color.placeholderInput}}>{selectCriteriaDiri_holder.length < 1 ? variables.select : selectCriteriaDiri_holder.length + variables.criteria} </Text><Icon type="AntDesign" name="search1" style={{fontSize:10, color: styles.color.placeholderInput}}/>
                                </TouchableOpacity>
                            </View>
                        </Item>
                        {(this.state.empty_criteria_diri) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <View style={{flex: 1,
                                height: 50,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between'}}>
                                <Label>{variables.kriteriaPasangan}</Label>
                                <TouchableOpacity  style={styles.loginPage.rightContainer} onPress={() => {
                                    this._showModalKriteriaPasangan(true);
                                }}>
                                    <Text style={{color: styles.color.placeholderInput}}>{selectCriteriaPasangan_holder.length < 1 ? variables.select : selectCriteriaPasangan_holder.length + variables.criteria} </Text><Icon type="AntDesign" name="search1" style={{fontSize:10, color: styles.color.placeholderInput}}/>
                                </TouchableOpacity>
                            </View>
                        </Item>
                        {(this.state.empty_criteria_pasangan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}
                        */}
                        <Item style={{borderBottomWidth: 0}}>
                            <Input disabled={true} editable={false}/>
                        </Item>
                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={styles.registerPage.listItemHeaderText}>
                                {variables.dataFamily}
                            </Text>
                        </Item>
                        <Item>
                            <Label>{variables.whatYourOrderInFamily}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.numberCountingPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, yourOrderInFamily: this._numberOnly(text)
                                    }
                                })}
                                value={this._nullStringHandler(yourOrderInFamily, '0')}
                                style={{textAlign: 'right'}}
                            />
                        </Item>
                        {(this.state.empty_anak_ke) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Label>{variables.numberOfSiblingsPlaceHolder}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.numberCountingPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, numberOfSiblings: this._numberOnly(text)
                                    }
                                })}
                                value={numberOfSiblings}
                                style={{textAlign: 'right'}}
                            />
                        </Item>
                        {(this.state.empty_jml_saudara) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Label>{variables.numberOfChildren}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.numberCountingPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, numberOfChildren: this._numberOnly(text)
                                    }
                                })}
                                value={numberOfChildren}
                                style={{textAlign: 'right'}}
                            />
                        </Item>
                        {(this.state.empty_jml_anak) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Label>{variables.numberOfDependents}</Label>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.numberCountingPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({
                                    my_profile: {
                                        ...this.state.my_profile, numberOfDependents: this._numberOnly(text)
                                    }
                                })}
                                value={numberOfDependents}
                                style={{textAlign: 'right'}}
                            />
                        </Item>
                        {(this.state.empty_jml_tanggungan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                    </List>

                </Content>
            </Container>
        );
    }
}
