import React, { Component } from 'react';
import { NavigationEvents } from 'react-navigation';
import {
    Text,
    TouchableOpacity,
    RefreshControl,
    View,
    Dimensions,
    ScrollView,
    Image,
    Alert,
    FlatList,
    ActivityIndicator} from 'react-native';
import styles from "../../../styles/MainStyle";
import variables from "../../../variables/MyGlobalVariable";
import GlobalDataRequest from "../../../services/GlobalDataRequest";
import {Container, Content} from "native-base";
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import StatusMaritalText from "../../../components/StatusMaritaText/StatusMaritalText";
import AsyncStorage from "@react-native-community/async-storage";
import Carousel from "react-native-looped-carousel";
const { width: SCREEN_WIDTH }= Dimensions.get('window');

export default class Tab1 extends Component {

    constructor(props){
        super(props);
        props.navigation.setParams({
            onTabFocus: this.handleTabFocus
        });
        const win = Dimensions.get('window');
        //const iconheight = 90 * (win.width/3)/100;
        //const iconheight = win.width/3;
        //const iconwidth = (win.width/3);
        this.state =
            {
                isFetchFailed: false,
                fcm_token: '',
                user_id: '',
                banner_list: [
                    {
                        image: ''
                    }
                ],
                //category_pixel: (win.width/3)-30,
                category_pixel: (win.width/3)-70,
                myKuota: null,
                myNik: null,
                role_id: null,
                user_token: null,
                news_list: [],
                quota_left: null,
                isLoading: false,
                showAppIntro: null,
                recommendation_member: [],
                seconds: 0
            };

        AsyncStorage.getItem(variables.fcm_token_KEY, (err, item) => {
            console.log('fcm_token_KEY ', item);
            this.setState({
                fcm_token: item
            })
        });
        this.mounted = false;
    }

    handleTabFocus = () => {
        alert('focus')
    };

    componentDidMount() {
        this.mounted = true;
        //alert(JSON.stringify(this.state.category_pixel, null, 3));
        this.getAuth();
        // set a custom param on the navigation object
    }

    componentWillUnmount(){
        console.log('componentWillUnmount');
        this.mounted = false;
    }

    _onInterestSuccess = (indexItem, indexResponse) => {
        //alert(JSON.stringify(this.state.action_code, null, 3));
        this.state.recommendation_member[indexItem].action_code = indexResponse;
        this.setState({
            recommendation_member: this.state.recommendation_member,
        });
    };

    getAuth() {
        GlobalDataRequest.authToken().then((resolve) => {
            console.log('user_token '+resolve);
            this.setState( {user_token: resolve });
            this._getHome(this.state.user_token);
        });

    }



    _getHome(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getHome(token)
            .then((result)=>{

                console.log('home param key ', Object.keys(result));
                console.log('my_fcm_token ', result.fcm_token);
                console.log('result.customer ', result.customer);
                console.log('result.isFollowing ', result.isFollowing);
                let responseJson = result.customer.filter(data => data.users !== null).map(item => {
                    console.log('responseJson ', item);
                    let checkIsFollowing = GlobalDataRequest._checkIsFollowing(item.id, result);
                    item.action_code = checkIsFollowing.status;
                    item.interestID = checkIsFollowing.interestId;
                    item.inRelationshipStatus = GlobalDataRequest._checkIsApproved(item.id, result);
                    return item;
                });
                /*let responseJson = result.customer.map(item => {
                    console.log('responseJson ', item);

                    if(item.users === null) {
                        return;
                    }
                    item.action_code = GlobalDataRequest._checkIsFollowing(item.id, result);
                    item.inRelationshipStatus = GlobalDataRequest._checkIsApproved(item.id, result);
                    return item;
                });*/
                if(this.mounted){
                    this.setState({
                        isLoading: false,
                        banner_list: result.banner,
                        news_list: result.news,
                        recommendation_member: responseJson,
                        role_id: result.role_id,
                        myKuota: result.myKuota,
                        myNik: result.myNik,
                    });
                    console.log('responseJson ', responseJson);
                    GlobalDataRequest.userIdSave(result.myUserID.toString()).then((result)=>{
                        console.log('myUserID ', result);
                        this.setState({
                            user_id: result
                        });
                    }).catch(error=>{
                        alert(error.message);
                    });

                    GlobalDataRequest.userAvatarSave(result.myAvatar).then((result)=>{
                        console.log('myAvatar ', result);
                        this.setState({
                            user_avatar: result
                        })
                    }).catch(error=>{
                        alert(error.message);
                    });

                    GlobalDataRequest.userNameSave(result.myName).then((result)=>{
                        console.log('myName ', result);
                        this.setState({
                            user_name: result
                        })
                    }).catch(error=>{
                        alert(error.message);
                    });
                }


                //alert(' avatar '+this.state.user_avatar+' name '+this.state.user_name);

            }).catch(err => {
                GlobalDataRequest.consoleLog("_getHome", err.message);
                GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            //Alert.alert(variables.warning, err.message);
            this.setState({isLoading: false, isFetchFailed: true});
        });
    }

    logout() {
        this.setState({isLoading: true});
        GlobalDataRequest.logout(this.state.user_token).then((result)=> {
            this.setState({isLoading: false});
            if(result.success){
                console.log('logout', result);
                GlobalDataRequest.authClear().then((resolve) => {
                    if(resolve) {
                        this.props.navigation.replace('Login');
                    }else {
                        Alert.alert(variables.info, variables.tryAgain);
                    }
                });
            }
        }).catch(err=>{
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false});
            //Alert.alert(variables.warning, err.message);
        });
    }

    async _clearStorage() {
        this.logout();
        try {
            await AsyncStorage.clear();
        } catch (error) {
            console.log("Error resetting data" + error);
        }
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    _onPressPricingButton() {
        this.props.navigation.navigate('PricingPage', {user_token: this.state.user_token})
    }

    _renderAvatarRecommendation(src, gender) {
        const avatarUri = GlobalDataRequest.renderAvatar(src, gender);
        //console.log('_renderAvatarRecommendation ', avatarUri);
        return (
                <Image
                    style={{flex:1,borderRadius: 5, margin: 3}}
                    source={{uri: avatarUri}}
                    //source={{uri: item.users.avatar}}
                    resizeMode="cover"
                />
        )
    }

    listEmpty = (listEmpty) => {
        let msg = listEmpty ? variables.loading : variables.noData;
        return (
            //View to show when list is empty
            <View style={{
                alignItems: 'center',
                justifyItems: 'center',
                justifyContent: 'center',
                flex: 1,
                margin: 10,
            }}>
                <Text style={{ textAlign: 'center' }}>{msg}</Text>
            </View>
        );
    };

    _onPressTryAgainFetch =()=> {
        this.getAuth();
    };

    render() {
        let imageArraySlides2 = [], bannerHeaderList = [], marginLeft = 0, marginRight = 0, left = 0;
        this.state.news_list.forEach((item, i) => {
            if(i<=0){
                left = 0;
                marginLeft = 0;
                marginRight = 5;
            }else if (i===(this.state.news_list.length - 1)) {
                marginLeft = 5;
                marginRight = 0;
            }else{
                left = 5;
                marginLeft = 5;
                marginRight = 5;
            }
            const thisImage = (
                <TouchableOpacity
                    activeOpacity={0.7}
                    key={`image${i}`} onPress={()=> this.props.navigation.navigate('NewsDetailPage', {dataDetail: item})} style={{borderRadius: 3}}>
                    <Image
                        source={{uri: GlobalDataRequest.renderImageUri(item.image)}}
                        style={{ width: SCREEN_WIDTH * 0.8, height: SCREEN_WIDTH * 0.4, marginLeft: marginLeft, marginRight: marginRight, borderTopLeftRadius: 3, borderTopRightRadius: 3}}
                    />
                    <View style={{
                        width: SCREEN_WIDTH * 0.8,
                        borderBottomLeftRadius: 3,
                        borderBottomRightRadius: 3,
                        padding: 5,
                        left: left,
                        bottom: 0,
                        backgroundColor: '#fff'
                    }}>
                        <Text numberOfLines={1} style={{fontSize: 18, color: '#000000'}}>
                            {item.title}
                        </Text>
                        <Text style={{color: '#515151'}}>
                            { GlobalDataRequest.dateFromNow(item.created_at) }
                        </Text>
                    </View>
                </TouchableOpacity>

            );
            imageArraySlides2.push(thisImage);
        });

        this.state.banner_list.forEach((data, i) => {
            const thisImage = (
                <TouchableOpacity key={`image${i}`} onPress={() => {}}>
                    <Image
                        source={{uri: GlobalDataRequest.renderImageUri(data.image)}}
                        style={{ width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.5}}
                    />
                </TouchableOpacity>
            );
            bannerHeaderList.push(thisImage);
        });


        return (
            <Container>
                <NavigationEvents
                    onWillFocus={payload => console.log('will focus')}
                    onDidFocus={payload => console.log('did focus')}
                    onWillBlur={payload => console.log('will blur')}
                    onDidBlur={payload => console.log('did blur')}
                />
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this._onPressTryAgainFetch.bind(this)}
                    navigation={this.props.navigation}
                    showPricingButton={true}
                    onPressPricingButton={this._onPressPricingButton.bind(this)}
                />
                <Content style={styles.otherStyle.containerMain}
                         refreshControl={
                             <RefreshControl
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                             />}
                >
                    <View style={{marginVertical: 0, backgroundColor: '#ffffff'}}>
                        <Carousel
                            delay={3000}
                            style={{
                                width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.5
                            }}
                            autoplay
                            bullets={true}
                            onAnimateNextPage={(p) => console.log(p)}
                        >
                            {bannerHeaderList}
                        </Carousel>
                        {/*<BannerHeader dataBanner={this.state.banner_list}/>*/}
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 15, marginBottom: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <TouchableOpacity
                            onPress={()=>this.props.navigation.navigate('ConsultationPage', {
                                user_token: this.state.user_token
                            })}
                            activeOpacity={ 0.7 } style={{flex: 1,justifyContent: "center", alignItems: 'center'}}>
                            <View style={{
                                height: this.state.category_pixel,
                                width: this.state.category_pixel,
                                justifyContent: "center", alignItems: 'center'
                            }}>
                                <Image source={require('../../../images/Konsulicon.png')}
                                       style={{
                                           height: this.state.category_pixel/1.2,
                                           width: this.state.category_pixel/1.2
                                       }}/>
                            </View>
                            <Text style={{
                                marginTop: 7
                            }}>{variables.services}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=>this.props.navigation.navigate('IntroductionPage',
                                {
                                    myNik: this.state.myNik,
                                    role_id: this.state.role_id,
                                    user_token: this.state.user_token,
                                    myKuota: this.state.myKuota,
                                    user_id: this.state.user_id
                                })}
                            activeOpacity={ 0.7 } style={{flex: 1,justifyContent: "center", alignItems: 'center'}}>
                            <View style={{
                                height: this.state.category_pixel,
                                width: this.state.category_pixel,
                                justifyContent: "center", alignItems: 'center'
                            }}>
                                <Image source={require('../../../images/Terapiicon.png')}
                                       style={{
                                           height: this.state.category_pixel/1.2,
                                           width: this.state.category_pixel/1.2
                                       }}/>
                            </View>
                            <Text style={{
                                marginTop: 7
                            }}>{variables.introduction}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=>this.props.navigation.navigate('SchedulePage')}
                            activeOpacity={ 0.7 } style={{flex: 1,justifyContent: "center", alignItems: 'center'}}>
                            <View style={{
                                height: this.state.category_pixel,
                                width: this.state.category_pixel,
                                justifyContent: "center", alignItems: 'center'
                            }}>
                                <Image source={require('../../../images/Jadwalicon.png')}
                                       style={{
                                           height: this.state.category_pixel/1.2,
                                           width: this.state.category_pixel/1.2
                                       }}/>
                            </View>
                            <Text style={{
                                marginTop: 7
                            }}>{variables.schedule}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={ 0.7 } style={{flex: 1,justifyContent: "center", alignItems: 'center'}}
                                          onPress={()=> this.props.navigation.navigate('NewsPage', {user_token: this.state.user_token})}>
                            <View style={{
                                height: this.state.category_pixel,
                                width: this.state.category_pixel,
                                justifyContent: "center", alignItems: 'center'
                            }}>
                                <Image source={require('../../../images/Newsicon.png')}
                                       style={{
                                           height: this.state.category_pixel/1.2,
                                           width: this.state.category_pixel/1.2
                                       }}/>
                                {/*<Icon name='fax' type='FontAwesome' style={{fontSize: 30, color: '#bd5afd'}}/>*/}
                            </View>
                            <Text style={{
                                marginTop: 7
                            }}>{variables.news}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <View style={{flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between'}}>
                            <Text style={styles.otherStyle.infoSlidesText}>{variables.recommendation}</Text>
                            <TouchableOpacity activeOpacity={0.6} onPress={()=> this.props.navigation.navigate('Tab2')}>
                                <Text style={styles.otherStyle.infoSlidesTextMore}>{variables.moreComplete}</Text>
                            </TouchableOpacity>
                        </View>

                        <FlatList
                            data={ this.state.recommendation_member }
                            renderItem={ ({item, index}) =>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    key={index+''+item.id}
                                    style={{
                                        height: SCREEN_WIDTH/3,
                                        flex:1
                                    }}
                                    onPress={() => this.props.navigation.navigate('ProfileDetail', {
                                        indexItem: index,
                                        dataItemTest: item,
                                        action_code: item.action_code,
                                        inRelationshipStatus: item.inRelationshipStatus,
                                        dataDetail: item,
                                        role_id: this.state.role_id,
                                        user_token: this.state.user_token,
                                        myKuota: this.state.myKuota,
                                        _onInterestSuccess: this._onInterestSuccess,
                                        interestID: item.interestID
                                    })}
                                >
                                    {this._renderAvatarRecommendation(item.users.avatar, item.gender)}
                                    <View style={{position: 'absolute', left: 8, bottom: 8}}>
                                        <StatusMaritalText
                                            statusMarital={item.status_marital}
                                        />
                                    </View>
                                </TouchableOpacity> }
                            keyExtractor={item => item.id}
                            extraData={this.state}
                            numColumns={3}
                            ListEmptyComponent={this.listEmpty((this.state.isLoading && (this.state.recommendation_member.length <=0)))}
                        />

                        {/*<FlatGrid
                            itemDimension={styles.otherStyle.gridDimension}
                            items={this.state.category_test}
                            style={styles.otherStyle.gridViewMember}
                            // staticDimension={300}
                            // fixed
                            // spacing={20}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity
                                    activeOpacity={ 0.8 }
                                    onPress={() => this.props.navigation.navigate('ProfileDetail', {dataDetail: item})}
                                    style={[styles.otherStyle.itemContainerMember, { backgroundColor: item.code }]}>
                                    <Image
                                        style={{flex:1,borderRadius: 5}}
                                        source={{uri: item.images}}
                                        resizeMode="cover"
                                    />
                                    <View style={{position: 'absolute', left: 5, bottom: 5}}>
                                        <StatusMaritalText
                                            statusMarital={item.status_marital}
                                        />
                                    </View>
                                </TouchableOpacity>
                            )}
                        />*/}
                    </View>

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <View style={{flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between'}}>
                            <Text style={styles.otherStyle.infoSlidesText}>{variables.news}</Text>
                            <TouchableOpacity activeOpacity={0.6} onPress={()=> this.props.navigation.navigate('NewsPage', {user_token: this.state.user_token})}>
                                <Text style={styles.otherStyle.infoSlidesTextMore}>{variables.moreComplete}</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                            >

                                {imageArraySlides2.length <=0 ? this.listEmpty((this.state.isLoading && (imageArraySlides2.length <=0))) : imageArraySlides2}
                            </ScrollView>
                        </View>
                    </View>

                    {
                        /*

                        <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <Text style={styles.otherStyle.infoSlidesText}>{variables.ads}</Text>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                            >

                                {imageArraySlides2}

                            </ScrollView>
                        </View>
                    </View>


                         */
                    }

                    {/*
                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('ChangePhotoProfile') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>ChangePhotoProfile</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingGridView') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>Testing Grid</Text> }
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingGridView2') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>Testing Grid2</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingGridView3') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>Testing Grid3</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingSlides') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>Testing Slides</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingToolbarParalax' , {dataDetail: this.state.category_test[0]}) }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingTabParalax</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingToolbarParalax2') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingTabParalax2</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingPageDatePickerStrip') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingPageDatePickerStrip</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingHighlightSelectItemFlatList') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingHighlightSelectItemFlatList</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingPopover') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingPopover</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingAppIntroSlider') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingAppIntroSlider</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingChat1') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingChat1</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingChat2') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingChat2</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingChat3_Chat') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingChat3_Chat</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingPricing') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingPricing</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingPricing2') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingPricing2</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('TestingImageZoom') }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>TestingImageZoom</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this._clearStorage() }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>_clearStorage</Text> }
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.logout() }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.logout}</Text> }
                    </TouchableOpacity>
                    */}

                </Content>
            </Container>
        );
    }
}
