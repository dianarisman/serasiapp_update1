import React, { Component } from 'react';
import { Container, Content, Button, Text} from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import { FlatGrid } from 'react-native-super-grid';
import {View, RefreshControl, TouchableOpacity, Image, Dimensions, ActivityIndicator} from 'react-native';
import styles from "../../../styles/MainStyle";
import GlobalDataRequest from "../../../services/GlobalDataRequest";
import variables from "../../../variables/MyGlobalVariable";
import StatusMaritalText from "../../../components/StatusMaritaText/StatusMaritalText";
import ModalFilter from "../../../components/ModalFilter";
import {GiftedChat} from "react-native-gifted-chat";

const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');

export default class Tab2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            role_id: '',
            myKuota: '',
            myNik: '',
            user_token: '',
            isLoading: false,
            modalVisiblePasangan: false,
            loadMoreIsVisible: false,
            selectCriteriaPasangan: [],
            selectCity: '',
            selectMarital: '',
            listCriteriaPasangan: variables.criteria_list,
            list_city: variables.cityList,
            list_marital: variables.statusMaritalList,
            memberList: [],
            next_page_url: null
        }
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
        //setInterval(this.setState({isLoading: false}), 3000);
    };


    _showModalKriteriaPasangan(visible) {
        this.setState({
            modalVisiblePasangan: visible
        });
    }

    _readyFilter(status_marital) {
        let selectCriteriaPasangan_array = [];
        this.state.listCriteriaPasangan.filter(item => item.is_selected).forEach((data, i) => {
            selectCriteriaPasangan_array.push(data.title);
        });
        let select = this.state.list_city.filter(item => item.is_selected);
        let selectedCity = '';

        if(select.length > 0) {
            selectedCity = select[0].city
        }
        this.setState({
            selectCriteriaPasangan: selectCriteriaPasangan_array,
            selectCity: selectedCity
        });

        this.setState({isLoading: true});
        this._getExplore(this.state.user_token, selectCriteriaPasangan_array, selectedCity, status_marital);
    }

    /*_getSelectedCriteriaPasangan(dataList) {
        let selectCriteriaPasangan_array = [];
        this.state.listCriteriaPasangan.filter(item => item.is_selected).forEach((data, i) => {
            selectCriteriaPasangan_array.push(data.title);
        });
        this.setState({
            selectCriteriaPasangan: selectCriteriaPasangan_array
        });
    }

    _getSelectedCity() {
        let select = this.state.list_city.filter(item => item.is_selected);
        this.setState({
            selectCity: select[0].city
        });
        //this.setState({isLoading: true});
        //this._getExplore(this.state.user_token, selectCriteriaPasangan_array, '', '');
    }

    _getSelectedMarital(status_marital) {
        this.setState({
            selectMarital: status_marital
        });
        //this.setState({isLoading: true});
        //this._getExplore(this.state.user_token, selectCriteriaPasangan_array, '', '');
    }*/

    _selectCriteriaPasangan(indexSelect, dataList) {
        let criteria = [ ...dataList ];
        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }
        this.setState({
            listCriteriaPasangan: criteria
        });
    }

    _selectCity(indexSelect, dataList) {
        let criteria = [ ...dataList ];

        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            let responseJson = criteria.map(item => {
                item.is_selected = false;
                return item;
            });
            this.setState({
                list_city: responseJson
            });
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }

        this.setState({
            list_city: criteria
        });
    }

    isCloseToBottom({layoutMeasurement, contentOffset, contentSize}){
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;
    }



    isCloseToTop({layoutMeasurement, contentOffset, contentSize}){
        return contentOffset.y === 0;
    }

    renderContent = () => (
        <View style={s.content}>
            {Array(50).fill(0).map((_, i) => (
                <View style={s.content__row} key={i}>
                    <View style={s.content__avatar}>
                        <View
                            style={{ width: 50, height: 50, backgroundColor: '#f64159', borderRadius: 10 }}
                        />
                    </View>

                    <Text style={s.content__name}>Garlic tastes best with ice water and lots of green curry.</Text>
                </View>
            ))}

            <View style={s.content__button}>
                <Button onPress={this.scrollToTop} name="Scroll to Top" />
            </View>
        </View>
    );

    componentWillMount(){
        this.getAuth();
    }

    getAuth() {
        GlobalDataRequest.authToken().then((resolve) => {
            console.log('tokennya '+resolve);
            this.setState( {user_token: resolve });
            this._getHome(resolve);
        });

    }

    _getHome(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getHome(token)
            .then((result)=>{

                //console.log('home param ', result);
                //alert(JSON.stringify(result));
                this.setState({
                    role_id: result.role_id,
                    myKuota: result.myKuota,
                    myNik: result.myNik,
                });
                this._getExplore(token, this.state.selectCriteriaPasangan, this.state.selectCity, this.state.selectMarital);
                GlobalDataRequest.userIdSave(result.myUserID.toString()).then((result)=>{
                    console.log('myUserID ', result);
                    this.setState({
                        user_id: result
                    })
                }).catch(error=>{
                    alert(error.message);
                });

                GlobalDataRequest.userAvatarSave(result.myAvatar).then((result)=>{
                    console.log('myAvatar ', result);
                    this.setState({
                        user_avatar: result
                    })
                }).catch(error=>{
                    alert(error.message);
                });

                GlobalDataRequest.userNameSave(result.myName).then((result)=>{
                    console.log('myName ', result);
                    this.setState({
                        user_name: result
                    })
                }).catch(error=>{
                    alert(error.message);
                });

                //alert(' avatar '+this.state.user_avatar+' name '+this.state.user_name);

            }).catch(err => {
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    _setNextPage(data) {
        //alert('next_page_url '+data.next_page_url);
        let next_page_url = null;
        if(data.next_page_url !== undefined || data.next_page_url !== null) {
            next_page_url = data.next_page_url;
        }
        this.setState({
            next_page_url: next_page_url

        });
    }

    _getExplore(token, criteria_pasangan, city, marital) {
        //this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getExplore(token, criteria_pasangan, city, marital)
            .then((result)=>{
                this.setState({
                    isLoading: false
                });
                if(result.success){
                    /*;
                    console.log('first_page_url ', result.data.customer.first_page_url);
                    console.log('from ', result.data.customer.from);
                    console.log('last_page ', result.data.customer.last_page);
                    console.log('last_page_url ', result.data.customer.last_page_url);
                    console.log('next_page_url ', result.data.customer.next_page_url);
                    console.log('per_page ', result.data.customer.per_page);
                    console.log('prev_page_url ', result.data.customer.prev_page_url);
                    console.log('to ', result.data.customer.to);
                    console.log('total ', result.data.customer.total);*/
                    console.log('result.data', Object.keys(result.data.customer.data[0]));
                    this._setNextPage(result.data.customer);
                    let responseJson = result.data.customer.data.filter(data => data.users !== null).map(item => {
                        let checkIsFollowing = GlobalDataRequest._checkIsFollowing(item.id, result.data);
                        item.action_code = checkIsFollowing.status;
                        item.interestID = checkIsFollowing.interestId;
                        item.inRelationshipStatus = GlobalDataRequest._checkIsApproved(item.id, result.data);
                        return item;
                    });
                    GlobalDataRequest.consoleLog("_getExplore", result.data.customer.data[0]);
                    this.setState({
                        memberList: responseJson

                    });
                    //console.log('_getExploreeee ', responseJson) ;
                }

                //alert(' avatar '+this.state.user_avatar+' name '+this.state.user_name);

            }).catch(err => {
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    _loadMoreData(next_page_url, user_token, selectCriteriaPasangan, selectCity, selectMarital) {
        if(this.state.loadMoreIsVisible) {
            return;
        }
        if(this.state.next_page_url === undefined || this.state.next_page_url === null || this.state.next_page_url === '' || this.state.next_page_url === 'null' ){
            return
        }
        this.setState({
            loadMoreIsVisible: true
        });

        GlobalDataRequest.getExploreLoadMore(next_page_url, user_token, selectCriteriaPasangan, selectCity, selectMarital)
            .then((result)=>{
                this.setState({
                    isLoading: false
                });
                if(result.success){

                    this.setState({isLoading: false});
                    this._setNextPage(result.data.customer);
                    let responseJson = result.data.customer.data.map(item => {
                        let checkIsFollowing = GlobalDataRequest._checkIsFollowing(item.id, result.data);
                        item.action_code = checkIsFollowing.status;
                        item.interestID = checkIsFollowing.interestId;
                        item.inRelationshipStatus = GlobalDataRequest._checkIsApproved(item.id, result.data);
                        return item;
                    });

                    this.setState(prevState => ({
                        memberList: [ ...prevState.memberList, ...responseJson]
                    }));
                    /*this.setState(state => {
                        const list = state.memberList.push(responseJson);
                        return {
                            list,
                            value: '',
                        };
                    });*/
                }
                this.setState({
                    loadMoreIsVisible: false
                });
            }).catch(error=>{
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true, loadMoreIsVisible: false,});
        });
    }

    _getSelectedMarital(value) {
        this.setState({
            selectMarital: value
        })
    }

    _onResetFilter() {
        this._showModalKriteriaPasangan(false);
        const status_marital = '';
        const selectedCity = '';
        const selectCriteriaPasangan_array = [];
        this.setState({
            isLoading: true,
            selectCriteriaPasangan: selectCriteriaPasangan_array,
            selectCity: selectedCity,
            selectMarital: status_marital,
            listCriteriaPasangan: variables.criteria_list,
            list_city: variables.cityList,
        });
        this._getExplore(this.state.user_token, selectCriteriaPasangan_array, selectedCity, status_marital);
    }

    _onInterestSuccess = (indexItem, indexResponse) => {
        //alert(JSON.stringify(this.state.action_code, null, 3));
        this.state.memberList[indexItem].action_code = indexResponse;
        this.setState({
            memberList: this.state.memberList,
        });
    };

    render() {

        return (
            <Container>
                <MyToolbarTab
                    searchIconVisible={true}
                    onPressSearch={this._showModalKriteriaPasangan.bind(this)}
                />
                <ModalFilter
                    selectTitleMarital={variables.statusMaritalPlaceholder}
                    selectTitleCriteria={variables.kriteriaPasangan}
                    selectTitleCity={variables.city}
                    isModalVisible={this.state.modalVisiblePasangan}
                    onSelectItemCriteria={this._selectCriteriaPasangan.bind(this)}
                    onSelectItemCity={this._selectCity.bind(this)}
                    onResetFilter={this._onResetFilter.bind(this)}
                    onHideModal={this._showModalKriteriaPasangan.bind(this)}
                    onReadyFilter={this._readyFilter.bind(this)}
                    listCriteria={this.state.listCriteriaPasangan}
                    listCity={this.state.list_city}
                    listMarital={this.state.list_marital}
                    onGetSelectedItemMarital={this._getSelectedMarital.bind(this)}
                    selectedMarital={this.state.selectMarital}
                />
                <Content
                    scrollEventThrottle={16}
                    onMomentumScrollEnd={({nativeEvent})=>{
                        /*if(this.isCloseToTop(nativeEvent)){
                            //do something
                        }*/
                        if(this.isCloseToBottom(nativeEvent)){
                            this._loadMoreData(this.state.next_page_url,
                                this.state.user_token,
                                this.state.selectCriteriaPasangan,
                                this.state.selectCity,
                                this.state.selectMarital);
                        }
                    }}
                    style={styles.otherStyle.containerMain}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >

                    <View style={{marginVertical: 0}}>
                        {/*<Text>{JSON.stringify(this.state.selectCriteriaPasangan, null, 3)}</Text>
                        <Text>{JSON.stringify(this.state.selectCity, null, 3)}</Text>
                        <Text>{JSON.stringify(this.state.selectMarital, null, 3)}</Text>*/}
                        <FlatGrid
                            itemDimension={styles.otherStyle.gridDimension}
                            items={this.state.memberList}
                            style={styles.otherStyle.gridViewMember}
                            // staticDimension={300}
                            // fixed
                            // spacing={20}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity
                                    activeOpacity={ 0.8 }
                                    onPress={() => this.props.navigation.navigate('ProfileDetail', {
                                        action_code: item.action_code,
                                        inRelationshipStatus: item.inRelationshipStatus,
                                        dataDetail: item,
                                        role_id: this.state.role_id,
                                        user_token: this.state.user_token,
                                        myKuota: this.state.myKuota,
                                        indexItem: index,
                                        _onInterestSuccess: this._onInterestSuccess,
                                        interestID: item.interestID
                                    })}
                                    style={[styles.otherStyle.itemContainerMember, { backgroundColor: item.code }]}>
                                    <Image
                                        style={{flex:1,borderRadius: 5}}
                                        source={{uri: GlobalDataRequest.renderAvatar(item.users.avatar, item.gender)}}
                                        resizeMode="cover"
                                    />
                                    <View style={{position: 'absolute', left: 5, bottom: 5}}>
                                        <StatusMaritalText
                                            statusMarital={item.status_marital}
                                        />
                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                        { this.state.loadMoreIsVisible ? <ActivityIndicator style={{margin: 20, paddingBottom: 30}} color={styles.color.placeholderInput} size='large'/> : null }

                    </View>
                </Content>
            </Container>
        );
    }
}
