import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Card, CardItem, Thumbnail } from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import {FlatList, StyleSheet, TouchableOpacity, View, AsyncStorage, Image, Text} from 'react-native';
import firebase from "../../../services/FirebaseConfig";
import variables from "../../../variables/MyGlobalVariable";
import moment from 'moment';
export default class Tab3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'raditsaditya2@gmail.com',
            user_id: '',
            chatList: [],
        };
    }

    componentWillMount(): void {
        AsyncStorage.getItem(variables.user_id_KEY, (error, result) => {
            if (result) {
                this.setState({
                    user_id: result
                });

                firebase.database().ref('Chating_Room/Users/'+ result +'/chat_list')
                    .on('value', snapshot => {
                        let chat_list = null;
                        if(snapshot.val() !== null) {
                            chat_list = Object.entries(snapshot.val()).map(item => ({...item[1], key: item[0]}));
                        } else {
                            chat_list = [];
                            //alert('data null');
                        }

                        this.setState({
                            chatList: chat_list
                        })
                    })
            }
        });
    }

    FlatListItemSeparator = () => <View style={styles.line} />;

    renderReceiver(status, receive) {
        let icon = '';
        if(status) {
            if(receive){
                icon = '✓✓';
            }else{
                icon = '✓';
            }
        }
        return icon;
    }
    renderItem = data => (
        <TouchableOpacity
            key={data.item.user_id.toString()}
            onPress={() => this.props.navigation.navigate('ChatRoomPage', {
                //email: this.state.email,
                user_id: this.state.user_id,
                to_avatar: data.item.avatar,
                to_name: data.item.name,
                to_id: data.item.user_id,
                to_fcm_token: data.item.fcm_token
            })}
        >
            <Card
                transparent
                key={data.item.user_id.toString()}>
                <CardItem>
                    <Left>
                        <Thumbnail source={{uri: data.item.avatar}} />
                        <Body>
                        <Text>{data.item.name}</Text>
                        <Text note numberOfLines={1}>{this.renderReceiver(data.item.me, data.item.received)}{data.item.text}</Text>
                        </Body>
                    </Left>
                    <Right>
                        <Text>{moment(data.item.timestamp).fromNow()}</Text>
                        <Text>{moment(data.item.timestamp).format('LT')}</Text>
                    </Right>
                </CardItem>
            </Card>


        </TouchableOpacity>);


    render() {
        return (
            <Container>
                <MyToolbarTab
                />
                <Content>
                    <FlatList
                        data={this.state.chatList}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.user_id.toString()}
                        extraData={this.state}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    list: {
        paddingVertical: 5,
        margin: 3,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        zIndex: -1
    },
    lightText: {
        color: "#000000",
        width: 200,
        fontSize: 12
    },
    line: {
        height: 0.5,
        width: "100%",
        backgroundColor:"rgba(255,255,255,0.5)"
    }
});
