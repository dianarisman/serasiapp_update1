import React, { Component } from 'react';
import { Container, Content, Text, Card, Left, Body, CardItem, Thumbnail, Icon, Right} from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import GlobalDataRequest from "../../../services/GlobalDataRequest";
import variables from "../../../variables/MyGlobalVariable";
import styles from "../../../styles/MainStyle";
import {RefreshControl, Image, TouchableOpacity, View, FlatList, Button} from "react-native";
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
export default class AnatomySerasi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            notificationList: [],
            showDialog: false
        };
        this.mounted = false
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        GlobalDataRequest.authToken().then((resolve) => {
            console.log('user_token '+resolve);
            this.setState( {user_token: resolve });
            this._getNotifList(resolve);
        });
    }

    _getNotifList(user_token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getNotifList(user_token).then((result)=>{
            GlobalDataRequest.consoleLog('Result_getNotifList',result);
            if(this.mounted){
                let notifList = [];
                if(result.success){
                    let list_id = 0,type = '', id = 0, id2 = 0,image = '',title = 'default',message = '', date = '', time = '';

                    result.data.forEach((item, i) => {
                        if(item.type === 'follow') {
                            list_id = i;
                            type = item.type;
                            id = item.data.liker_id;
                            id2 = item.data.likee_id;
                            title = item.data.title;
                            message = item.data.message;
                            image = item.data.avatar;
                            date = item.date;
                            time = item.time;
                        }else if (item.type === 'payment_confirmation'){
                            list_id = i;
                            type = item.type;
                            title = item.data.title;
                            message = item.data.message;
                            image = 'https://i.ibb.co/KN3WWyq/check.png';
                            date = item.date;
                            time = item.time;
                        }

                       const arayPush = {
                           list_id: list_id,
                           id: id,
                           id2: id2,
                           type: type,
                           message: message,
                           image: image,
                           title: title,
                           date: date,
                           time: time
                       };
                       notifList.push(arayPush);
                       list_id = 0;
                       type = '';
                       id = 0;
                       id2 = 0;
                       image = '';
                       title = 'default';
                       message = '';
                       date = '';
                       time = '';
                    });
                    GlobalDataRequest.consoleLog('Result_notifList',notifList);

                    this.setState({
                        isLoading: false,
                        notificationList: notifList
                    });
                }else{
                    GlobalDataRequest.consoleLog('Failed__getNotifList',result);
                    GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
                    this.setState({isLoading: false, isFetchFailed: true});
                }

            }
        }).catch(err=>{
            GlobalDataRequest.consoleLog('CatchError__getNotifList',err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    _onPressTryAgainFetch =()=> {
        this.getAuth();
    };

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    listEmpty = (listEmpty) => {
        let msg = listEmpty ? variables.loading : variables.noData;
        return (
            //View to show when list is empty
            <View style={{
                alignItems: 'center',
                justifyItems: 'center',
                justifyContent: 'center',
                flex: 1,
                margin: 10,
            }}>
                <Text style={{ textAlign: 'center' }}>{msg}</Text>
            </View>
        );
    };

    async _notifNavigation(data) {
        if(data.type === 'follow') {
            await this.setState({
                titleDialog: variables.introduction,
                avatarDialog: GlobalDataRequest.renderImageUri(data.image),
                avatarMessage: data.title +' '+data.message
            });
            //this.props.navigation.navigate('IntroductionPage');

        }else{
            await this.setState({
                titleDialog: variables.payment,
                avatarDialog: data.image,
                avatarMessage: data.message
            });
        }

        this.openDialog(true);
    }

    openDialog = (show) => {
        this.setState({ showDialog: show });
    };

    render() {
        return (
            <Container>
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this._onPressTryAgainFetch.bind(this)}
                    navigation={this.props.navigation}
                />
                <Content
                    style={styles.otherStyle.containerMainWhite}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >
                    <ConfirmDialog
                        title={this.state.titleDialog}
                        animationType="fade"
                        contentStyle={
                            {
                                alignItems: "center",
                                justifyContent: "center",
                            }
                        }
                        onTouchOutside={ () => this.openDialog(false) }
                        visible={ this.state.showDialog }
                        positiveButton={{
                            title: "OK",
                            onPress: () => this.openDialog(false)
                        }}
                    >
                        <Thumbnail source={{uri: this.state.avatarDialog}} />
                        <Text style={ { marginVertical: 30 } }>
                            {this.state.avatarMessage}
                        </Text>
                    </ConfirmDialog>
                    <FlatList
                        data={ this.state.notificationList }
                        renderItem={ ({item, index}) =>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                  key={'notiflistid'+index+''+item.id}
                                  onPress={()=> this._notifNavigation(item)}
                            >
                                <CardItem>
                                    <Left>
                                        <Thumbnail source={{uri: (item.type !== 'follow' ? item.image : GlobalDataRequest.renderImageUri(item.image))}} />
                                        <Body>
                                            <View style={{flexDirection: 'row',
                                                justifyContent: 'space-between'}}>
                                                <Text>{item.title}</Text>
                                                <Text note>{item.date}</Text>
                                            </View>
                                            <Text note>{item.message}</Text>
                                        </Body>
                                    </Left>
                                </CardItem>
                            </TouchableOpacity> }
                        keyExtractor={item => item.list_id+''}
                        extraData={this.state}
                        ListEmptyComponent={this.listEmpty((this.state.isLoading && (this.state.notificationList.length <=0)))}
                    />

                </Content>
            </Container>
        );
    }
}
