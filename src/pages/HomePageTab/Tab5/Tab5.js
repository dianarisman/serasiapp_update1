import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import styles from "../../../styles/MainStyle";
import {ActivityIndicator, Alert, Dimensions, Image, RefreshControl, TouchableOpacity, View} from "react-native";
import variables from "../../../variables/MyGlobalVariable";
import ReadMore from "../../../components/ReadMore/ReadMore";
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import GlobalDataRequest from "../../../services/GlobalDataRequest";
import StatusMaritalText from "../Tab1/Tab1";
import moment from "moment";
import TryAgainButtonFetch from "../../../components/TryAgainButtonFetch/TryAgainButtonFetch";
import ImagePicker from "react-native-image-picker";
const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get("screen");
import PushNotification from "react-native-push-notification";

export default class Tab5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataLempar: this.props.navigation.getParam('dataLempar'),
            isLoading: false,
            isFetchFailed: false,
            user_token: null,
            my_profile: {
                user_id: null,
                emailInput: null,
                phoneInput: null,
                statusMarital: null,
                identityCard: null,
                images: null,
                fullName: null,
                genderSelect: null,
                religionSelect: null,
                placeOfBirth: null,
                birthDate: null,
                hobbyInput: null,
                educationSelect: null,
                aboutInput: null,
                jobSelect: null,
                addressInput: null,
                selectCity: null,
                selectProvince: null,
                selectCriteriaDiri: [],
                selectCriteriaPasangan: [],
                yourOrderInFamily: null,
                numberOfSiblings: null,
                bodyHeightInput: null,
                bodyWeightInput: null,
                nationality: null,
                numberOfDependents: null,
                numberOfChildren: null,
            }
        }

    }

    getAuth() {
        GlobalDataRequest.authToken().then((resolve) => {
            //console.log('tokennya '+resolve);
            this.setState( {user_token: resolve });
            this._getProfile(this.state.user_token);
            //this.getNews(this.state.tokenya);
        });

    }

    componentDidMount(): void {
        this.getAuth();

    }

    _editStateMyProfile(
        noAnggota,
        userID,
        emailInput,
        phoneInput,
        statusMarital,
        identityCard,
        images,
        fullName,
        genderSelect,
        religionSelect,
        placeOfBirth,
        birthDate,
        hobbyInput,
        educationSelect,
        aboutInput,
        jobSelect,
        addressInput,
        selectCity,
        selectProvince,
        selectCriteriaDiri,
        selectCriteriaPasangan,
        yourOrderInFamily,
        numberOfSiblings,
        numberOfChildren,
        numberOfDependents,
        bodyHeightInput,
        bodyWeightInput,
        nationalitySelect
    ) {
        console.log('avatarUri ', GlobalDataRequest.renderAvatar(images,genderSelect));
       return this.setState({
            my_profile: {
                no_anggota: this._nullStringHandler(noAnggota,''),
                user_id: userID,
                emailInput: emailInput,
                phoneInput: phoneInput,
                statusMarital: statusMarital,
                identityCard: identityCard,
                images: GlobalDataRequest.renderAvatar(images,genderSelect),
                fullName: fullName,
                genderSelect: genderSelect,
                religionSelect: religionSelect,
                placeOfBirth: placeOfBirth,
                birthDate: birthDate,
                hobbyInput: hobbyInput,
                educationSelect: educationSelect,
                aboutInput: this._nullStringHandler(aboutInput,''),
                jobSelect: jobSelect,
                addressInput: addressInput,
                selectCity: selectCity,
                selectProvince: selectProvince,
                selectCriteriaDiri: selectCriteriaDiri,
                selectCriteriaPasangan: selectCriteriaPasangan,
                yourOrderInFamily: yourOrderInFamily,
                numberOfSiblings: this._nullStringHandler(numberOfSiblings,'0'),
                bodyHeightInput: parseInt(this._nullStringHandler(bodyHeightInput,'0'), 10).toString(),
                bodyWeightInput: parseInt(this._nullStringHandler(bodyWeightInput,'0'), 10).toString(),
                nationalitySelect: this._nullStringHandler(nationalitySelect,'WNI'),
                numberOfDependents: this._nullStringHandler(numberOfDependents,'0'),
                numberOfChildren: this._nullStringHandler(numberOfChildren,'0'),
            }
        })
    }

    _nullStringHandler(handlerData, handlerString){
        let data = (handlerData+'').trim();
        return (data === 'null' || data === '' || data.length < 1 || !data) ? handlerString : data;
    }

    _openLibraryHandler() {
        const options = {
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            // Same code as in above section!
            console.log('Response = ', response);
        });
    }
    logout() {
        PushNotification.unregister();
        this.setState({isLoading: true});
        GlobalDataRequest.logout(this.state.user_token).then((result)=> {
            this.setState({isLoading: false});
            if(result.success){
                PushNotification.unregister();
                PushNotification.abandonPermissions();
                console.log('logout', result);
                GlobalDataRequest.authClear().then((resolve) => {
                    if(resolve) {
                        this.props.navigation.replace('Login');
                    }else {
                        Alert.alert(variables.info, variables.tryAgain);
                    }
                });
            }
        }).catch(err=>{
            this.setState({isLoading: false});
            Alert.alert(variables.warning, err.message);
        });
    }

    _changeAvatar() {
        GlobalDataRequest.takePhotoHandler().then((result) => {
            // You can also display the image using data:
            this.setState({
                isLoading: true,
            });
            try {
                const avatarData = 'data:image/jpeg;base64,' + result.data;
                GlobalDataRequest.changeAvatar(this.state.user_token,this.state.my_profile.user_id,avatarData).then((result)=>{
                    console.log('_changeAvatar ', result);
                    if(result.success){
                        this.getAuth();
                    }else{
                        GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
                    }
                }).catch(error=>{
                    console.log('error _changeAvatar ', error.message);
                    //Alert.alert(variables.warning, error.message);
                    GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
                });

            }catch (e) {
                GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
                this.setState({
                    isLoading: false,
                });
                alert(e.message);
            }

        }).catch(()=>{
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({
                isLoading: false,
            });
        });

    }

    _getProfile(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getProfile(token)
            .then((result)=>{
                console.log('profile param', result);
                this.setState({ isLoading: false});
                if(!result.success){
                    Alert.alert(variables.warning, result.message);
                    return;
                }

                GlobalDataRequest.userAvatarSave(result.data.users.avatar).then((result)=>{
                    console.log('myAvatar ', result);
                }).catch(error=>{
                    alert(error.message);
                });

                GlobalDataRequest.userNameSave(result.data.name).then((result)=>{
                    console.log('myName ', result);
                }).catch(error=>{
                    alert(error.message);
                });

                let criteria = result.data.criteria;
                let criteria_pasangan = result.data.criteria_pasangan;

                criteria.map(item => {
                    item.is_selected = true;
                    return item;
                });

                criteria_pasangan.map(item => {
                    item.is_selected = true;
                    return item;
                });

                console.log('criteria ', criteria);

                this._editStateMyProfile(
                    ''+result.data.no_anggota,
                    ''+result.data.users_id,
                    ''+result.data.users.email,
                    ''+result.data.phone,
                    ''+result.data.status_marital,
                    ''+result.data.nik,
                    ''+result.data.users.avatar,
                    ''+result.data.name,
                    ''+result.data.gender,
                    ''+result.data.agama,
                    ''+result.data.birth_place,
                    ''+result.data.birth_date,
                    ''+result.data.hobby,
                    ''+result.data.pendidikan,
                    ''+result.data.description,
                    ''+result.data.pekerjaan,
                    ''+result.data.address,
                    ''+result.data.city,
                    ''+result.data.provinsi,
                    criteria,
                    criteria_pasangan,
                    ''+result.data.anak_ke,
                    ''+result.data.jml_saudara,
                    ''+result.data.jml_anak,
                    ''+result.data.jml_tanggungan,
                    ''+result.data.tb,
                    ''+result.data.bb,
                    ''+result.data.kewarganegaraan
                );
            }).catch(err => {
                console.log('errorFetchProfile ', err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
        /*GlobalDataRequest.tes().then((result)=>{
            this.setState({isLoading: false});
            alert(JSON.stringify(result));
        }, () => {
            alert('cant request to server');
        });*/
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    _renderTruncatedFooter = (handlePress) => {
        return (
            <Text style={{color: '#b058ff', marginTop: 5}} onPress={handlePress}>
                {variables.readMore}
            </Text>
        );
    };

    _renderRevealedFooter = (handlePress) => {
        return (
            <Text style={{color: '#b058ff', marginTop: 5}} onPress={handlePress}>
                {variables.showLess}
            </Text>
        );
    };

    _handleTextReady = () => {
        // ...
    };

    onEdit = () => {
        this.getAuth();
    };

    renderScrollViewContent() {
        let kriteriaDiri_list = this.state.my_profile.selectCriteriaDiri.map( (data, index) => {
            return <Text key={index} style={styles.otherStyle.textBubble}>{data.criteria_name}</Text>
        });

        let kriteriaPasangan_list = this.state.my_profile.selectCriteriaPasangan.map( (data, index) => {
            return <Text key={index} style={styles.otherStyle.textBubble}>{data.criteria_name}</Text>
        });
        return (
            <View>
                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{
                            height: SCREEN_WIDTH,
                            flex:1
                        }}
                        //onPress={() => this._changeAvatar()}
                        onPress={() => this._changeAvatar()}
                    >
                        <Image
                            style={{flex:1,borderRadius: 5, margin: 3}}
                            source={{uri: this.state.my_profile.images}}
                            resizeMode="cover"
                        />
                    </TouchableOpacity>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.about}</Text>
                    <ReadMore
                        numberOfLines={3}
                        renderTruncatedFooter={this._renderTruncatedFooter}
                        renderRevealedFooter={this._renderRevealedFooter}
                        onReady={this._handleTextReady}>
                        <Text>
                            {this.state.my_profile.aboutInput}
                        </Text>
                    </ReadMore>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.accountDetail}</Text>
                    <View style={{
                        flex: 1
                    }}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.emailPlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.emailInput}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.phonePlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.phoneInput}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.passwordPlaceholder}</Text><Text
                            onPress={()=>
                                this.props.navigation.navigate('ChangePasswordPage', {
                                    user_token: this.state.user_token,
                                    onEdit: this.onEdit
                                })
                            }
                        > : <Icon style={{ fontSize: 14}} name='dots-three-horizontal' type='Entypo'/> <Icon style={{ fontSize: 14, color: '#b058ff'}} name='edit' type='FontAwesome'/></Text>
                        </View>

                    </View>
                </View>
                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.biodata}</Text>
                    <View style={{
                        flex: 1
                    }}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.fullnamePlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.fullName}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.gender}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{(this.state.my_profile.genderSelect === 'pria' ? variables.male : variables.female) }</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.religion}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.religionSelect}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.placeOfBirth}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.placeOfBirth}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.birthDatePlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{moment(this.state.my_profile.birthDate, 'YYYYMMDD').format('YYYY-MM-DD')}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.education}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.educationSelect}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.hobbyPlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.hobbyInput}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.statusMaritalPlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.statusMarital}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.selectJobPlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.jobSelect}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.adressPlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.addressInput}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.city}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.selectCity}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.height}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.bodyHeightInput}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.weight}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.bodyWeightInput}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.status}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.statusMarital}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.nationality}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.state.my_profile.nationalitySelect}</Text>
                        </View>
                    </View>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.kriteriaDiri}</Text>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {kriteriaDiri_list}
                    </View>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.kriteriaPasangan}</Text>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {kriteriaPasangan_list}
                    </View>
                </View>
            </View>

        );
    }

    _onPressTryAgainFetch() {
        this.getAuth();
    }
    _onPressEditButton() {
        this.props.navigation.navigate('EditProfilePage', {
            user_token: this.state.user_token,
            dataDetailProfile: this.state.my_profile,
            onEdit: this.onEdit
        });
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this._onPressTryAgainFetch.bind(this)}
                    navigation={this.props.navigation}
                    showEditButton={!this.state.isLoading}
                    onPressEditButton={this._onPressEditButton.bind(this)}

                />
                {/*this.state.isFetchFailed ? (<TryAgainButtonFetch onPress={this._onPressTryAgainFetch.bind(this)}/>) : null*/}
                <Content style={styles.otherStyle.containerMain}
                         refreshControl={
                             <RefreshControl
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                             />}
                >


                    {this.renderScrollViewContent()}
                    <View style={{marginVertical: 5, paddingHorizontal: 10}}>
                        <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.logout() }>
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.logout}</Text> }
                        </TouchableOpacity>
                    </View>

                </Content>
            </Container>
        );
    }
}
