import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
    Card, CardItem, Thumbnail} from 'native-base';
import variables from "../../variables/MyGlobalVariable";
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import styles from "../../styles/MainStyle";
import {RefreshControl, Image, View, TouchableOpacity, ActivityIndicator} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import moment from 'moment';
import 'moment/min/locales';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
export default class IntroductionPage extends Component {

    constructor(props) {
        super(props);
        //randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
        moment.locale('id');
        this.state = {
            isLoading: false,
            isFetchFailed: false,
            loadMoreIsVisible: false,
            user_id: this.props.navigation.getParam('user_id'),
            user_token: this.props.navigation.getParam('user_token'),
            myNik: this.props.navigation.getParam('myNik'),
            introductionList: [],
            role_id: this.props.navigation.getParam('role_id'),
            myKuota: this.props.navigation.getParam('myKuota')
        };
        this.mounted = false;
    }

    _onContentRefresh = () => {
        this.getIntroduction(this.state.user_token)
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    componentDidMount() {
        this.mounted = true;
        this.getIntroduction(this.state.user_token)
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    _onInterestSuccess = (indexItem, indexResponse) => {
        //alert(JSON.stringify(this.state.action_code, null, 3));
        this.state.introductionList[indexItem].likes_box.status = indexResponse;

        this.setState({
            introductionList: this.state.introductionList
        });
    };

    getIntroduction(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.introduction(token).then((result) => {
            //alert(JSON.stringify(result));
            this.setState({ isLoading: false});
            if(this.mounted){

                if(result.success){
                    this.setState({
                        introductionList: result.data.perkenalan,
                    });
                    GlobalDataRequest.consoleLog("getIntroduction", result.data.perkenalan[0].likes_box.status);
                }else{
                    GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
                }
            }
        }).catch(err => {
            GlobalDataRequest.consoleLog("getIntroduction", err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        });
    }



    onEdit = () => {
        this.getIntroduction(this.state.user_token);
    };

    _openChat(likerData) {
        this.props.navigation.navigate('ChatRoomPage', {
            user_id: this.state.user_id,
            to_avatar: GlobalDataRequest.renderAvatar(likerData.users.avatar, likerData.gender),
            to_name: likerData.users.name,
            to_fcm_token: likerData.users.fcm_token === null ? 'NULL' : likerData.users.fcm_token,
            to_id: likerData.users.id.toString()
        });
    }

    _renderIntroduction() {
        return this.state.introductionList.map((item, index) => {
            const {
                likee,
                liker,
                likes_box,
                id
            } = item;

            const like = liker.nik === this.state.myNik ? likee : liker;
            //console.log('likee ',likee);
            //console.log('liker ',liker);
            const interestID = id;
            const status = likes_box === null ? 'NULL' : likes_box.status;
            const fcm_token = like.users.fcm_token === null ? 'NULL' : like.users.fcm_token;

            //const randomColor = "000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
            return (
                <TouchableOpacity key={index} activeOpacity={0.5} onPress={()=>
                    this.props.navigation.navigate('ProfileDetail',
                        {
                            action_code: status,
                            dataDetail: like,
                            role_id: this.state.role_id,
                            user_token: this.state.user_token,
                            myKuota: this.state.myKuota,
                            indexItem: index,
                            _onInterestSuccess: this._onInterestSuccess,
                            interestID: interestID,
                            //onEdit: this.onEdit
                        })}>
                    {/*<Text>fcm_token {fcm_token}</Text>*/}
                    <Card transparent>
                        <CardItem>
                            <Left>
                                <Thumbnail source={{uri: GlobalDataRequest.renderAvatar(like.users.avatar, like.gender)}} />
                                <Body>
                                <Text>{like.name}</Text>
                                <Text note>{like.status_marital}</Text>
                                </Body>
                            </Left>
                            <Right>
                                {status === 'finishing' || status === 'approved' ? null :
                                    <Text
                                        style={{
                                            color: '#fff',
                                            height: 20,
                                            backgroundColor: 'rgba(0, 0, 0, 0.251)',
                                            paddingHorizontal: 20,
                                            borderRadius: 10
                                        }}>{status}</Text>
                                }
                                {status === 'finishing' || status === 'approved' ? <TouchableOpacity onPress={()=> this._openChat(like)} style={{
                                    height: 20,
                                    backgroundColor: '#f44500',
                                    paddingHorizontal: 20,
                                    borderRadius: 10
                                }}>
                                    <Text style={{color: '#fff'}}>{variables.chat}</Text>
                                </TouchableOpacity> : null}
                            </Right>
                        </CardItem>
                    </Card>
                </TouchableOpacity>
            );
        });
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this.getIntroduction.bind(this, this.state.user_token)}
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content
                    style={{flex: 1, zIndex: -1}}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >
                    <View style={{marginVertical: 5, backgroundColor: 'transparent', padding: 10}}>
                        {this._renderIntroduction()}
                    </View>

                    { this.state.loadMoreIsVisible ? <ActivityIndicator style={{margin: 20}} color={styles.color.placeholderInput} size='large'/> : null }
                </Content>
            </Container>
        );
    }
}
