import React, { Component } from 'react';
import GlobalDataRequest from "../../services/GlobalDataRequest";
import styles from "../../styles/MainStyle";
import {View, TouchableOpacity, ActivityIndicator, Animated} from 'react-native';
import {
    Container,
    Content,
    Icon,
    Text,
    Item,
    Input,
    Toast,
    Button
} from 'native-base';
import variables from "../../variables/MyGlobalVariable";
import Logo from "../../components/Logo/Logo";
import MyStatusBar from "../../components/StatusBar/MyStatusBar";
import FadeInView from "../../components/FadeInView/FadeInView";
import AsyncStorage from "@react-native-community/async-storage";
import PushNotification from "react-native-push-notification";

export default class Login extends Component {

    // noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: null ,
    };
    constructor(props){
        super(props);
        this.state =
            {
                token: "",
                hidePassword: true,
                secureTextEntry: true,
                is_password: true,
                isLoading: false,
                username: '',
                password: '',
                emptyEmail: false,
                emptyPassword: false,
                showToast: false,
                fcm_token: 'default'
            };
    }

    componentDidMount() {
        PushNotification.requestPermissions();
    }

    loginTest() {
        if(this.state.username === "") {
            this.setState({
                emptyEmail: true
            });
        }else {
            this.setState({
                emptyEmail: false
            });
        }

        if(this.state.password === "") {
            this.setState({
                emptyPassword: true
            });
        }else {
            this.setState({
                emptyPassword: false
            });
        }

        if(this.state.username === "" || this.state.password === ""){
            return
        }

        this.setState({
            isLoading: true
        });
        AsyncStorage.getItem(variables.fcm_token_KEY, (err, item) => {
            //alert(item);
            if(item){
                this.setState({
                    fcm_token: item
                });
                GlobalDataRequest.login(this.state.username.trim(),this.state.password.trim(), item).then((result) => {
                    this.setState({
                        isLoading: false,
                        dataSource: JSON.stringify(result),
                    });
                    //alert("result " + JSON.stringify(result));
                    // noinspection JSUnresolvedVariable
                    console.log(result);
                    if(result.access_token) {
                        GlobalDataRequest.AuthSave(JSON.stringify(result), "true", result.access_token).then(() => {
                            if(result.avatar === 'users/ava_man.png' ||result.avatar === 'users/ava_woman.png'  ) {
                                this.props.navigation.replace('ChangePhotoProfile', {
                                    user_id: result.user_id,
                                    user_token: result.access_token
                                });
                            }else {
                                this.props.navigation.replace('Home');
                            }
                            //this.props.navigation.replace('ChangePhotoProfile');
                        }, err => {
                            alert("Somthing Error " + JSON.stringify(err));
                        });
                    }else {
                        GlobalDataRequest.toastHandler(variables.loginErrorMessage,'danger','top');
                        this.setState({
                            isLoading: false
                        });
                    }
                }).catch((err)=> {
                    GlobalDataRequest.consoleLog('Error Login',err.message);
                    GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
                    this.setState({
                        isLoading: false
                    });
                });
            }

        });
    }

    abandon() {
        PushNotification.abandonPermissions();
        PushNotification.unregister();
    }

    managePasswordVisibility = () =>
    {
        this.setState({
            hidePassword: !this.state.hidePassword,
            secureTextEntry: !this.state.secureTextEntry,
            is_password: !this.state.is_password,
        });
    };
    render() {
        return (
            <Container>
                <Content contentContainerStyle={{ justifyContent: 'center', flex: 1, marginHorizontal: 30 }}>
                    <MyStatusBar/>
                    <FadeInView>
                        <Logo/>
                    </FadeInView>
                    <Item>
                        <Icon active name='mail' />
                        <Input
                            disabled={this.state.isLoading}
                            placeholder={variables.loginPlaceholder}
                            placeholderTextColor={styles.color.placeholderInput}
                            onChangeText={(text) => this.setState({username: text})}
                            value={this.state.username}
                        />

                    </Item>
                    {(this.state.emptyEmail) ? <Text style={{ color: styles.color.errorInput}}>{variables.emailEmptyErrorMessage}</Text> : null}
                    <Item>
                        <Icon active name='key' />
                        <Input
                            disabled={this.state.isLoading}
                            secureTextEntry={ this.state.secureTextEntry}
                            password={ this.state.is_password }
                            placeholder={ variables.passwordPlaceholder}
                            placeholderTextColor={styles.color.placeholderInput}
                            onChangeText={(text) => this.setState({password: text})}
                            value={this.state.password}
                        />
                        <TouchableOpacity activeOpacity = { 0.8 } onPress = { this.managePasswordVisibility }>
                            <Icon active name={( this.state.hidePassword ) ? 'eye' : 'eye-off'} />
                        </TouchableOpacity>
                    </Item>
                    {(this.state.emptyPassword) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordEmptyErrorMessage}</Text> : <Text/>}
                    <View style={styles.loginPage.content_bottom}>
                        {/*<TouchableOpacity style={styles.loginPage.leftContainer} onPress={() => this.props.navigation.navigate('RegisterStep1')}>
                            <Text style={styles.loginPage.registerText('#4c4c4c')}>
                                {variables.register}
                            </Text>
                        </TouchableOpacity>*/}
                        <TouchableOpacity style={styles.loginPage.rightContainer} onPress={()=>this.props.navigation.navigate('ResetPassword')}>
                            <Text style={styles.loginPage.forgotPasswordText('#4c4c4c')}>
                                {variables.forgotPassword}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this.loginTest() }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.login}</Text> }
                    </TouchableOpacity>

                    <View style={styles.testingLogin.separatorWrap}>
                        <View style={styles.testingLogin.separator('#000')}/>
                        <Text style={styles.testingLogin.separatorText('#3b3b3b')}>{variables.or}</Text>
                        <View style={styles.testingLogin.separator('#000')}/>
                    </View>

                    <TouchableOpacity disabled={this.state.isLoading} activeOpacity={ 0.8 } style={styles.formLogin.buttonBorder(styles.color.serasiColorButton)} onPress={() => this.props.navigation.navigate('RegisterStep1') }>
                        <Text style={styles.formLogin.buttonTextBorder(styles.color.serasiColorButton)}>{variables.register}</Text>
                    </TouchableOpacity>


                    {/*
                    <TouchableOpacity
                        onPress={()=> PushNotification.requestPermissions()}
                    >
                        <Text>requestPermissions</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={()=> this.abandon()}
                    >
                        <Text>abandonPermissions</Text>
                    </TouchableOpacity>
                    */}

                </Content>
            </Container>
        );
    }
}
