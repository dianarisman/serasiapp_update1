import React, { Component } from 'react';
import Tab1 from '../HomePageTab/Tab1/Tab1';
import Tab2 from '../HomePageTab/Tab2/Tab2';
import Tab3 from '../HomePageTab/Tab3/Tab3';
import Tab4 from '../HomePageTab/Tab4/Tab4';
import Tab5 from '../HomePageTab/Tab5/Tab5';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import TabBar from "../../components/TabBar/TabBar";


export default createAppContainer(createBottomTabNavigator({
        Tab1: { screen: Tab1 },
        Tab2: { screen: Tab2 },
        Tab3: { screen: Tab3 },
        Tab4: { screen: Tab4 },
        Tab5: { screen: Tab5 }
    },
    {
        lazy: false,
        tabBarPosition: 'bottom',
        showIcon: false,
        swipeEnabled: true,
        tabBarComponent: props => {
            return (
                <TabBar {...props}/>
            );
        }
    }
));
