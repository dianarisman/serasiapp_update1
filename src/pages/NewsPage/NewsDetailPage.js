import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import variables from "../../variables/MyGlobalVariable";
import styles from "../../styles/MainStyle";
import {Image, Dimensions, View} from 'react-native';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import HTML from "react-native-render-html";
import GlobalDataRequest from "../../services/GlobalDataRequest";
const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');
export default class NewsDetailPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imageWidth: 0,
            imageHeight: 0,
        };
        this.dataDetail = this.props.navigation.getParam('dataDetail');

    }

    onImageLoaded(){
        try{
            Image.getSize(this.dataDetail.news_header_image, (width, height) => {
                const imageHeight = height * (SCREEN_WIDTH)/width;
                const imageWidth = SCREEN_WIDTH;
                this.setState(
                    {
                        imageWidth: imageWidth,
                        imageHeight: imageHeight
                    }
                )
            });
        }catch(e){
            alert('failed load image');
        }
    }

    render() {
        const defaultRenderer = {
            renderers: {
                img: (htmlAttribs, children, convertedCSSStyles, passProps) => <Image key={passProps.key} style={{width:90, height: 90}} source={{ uri: htmlAttribs.src }}/>
            }
        };
        return (
            <Container>
                {/*<MyToolbar navigation={this.props.navigation} title={this.dataDetail.news_title}/>*/}
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content
                    style={styles.otherStyle.containerMain}
                >
                    <Image source={{uri: this.dataDetail.image}} resizeMode='contain' onLoadEnd={()=>this.onImageLoaded()}
                           style={{width:this.state.imageWidth,height:this.state.imageHeight}} />

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <Text style={{fontWeight: 'bold', fontSize: 18}}>{this.dataDetail.title}</Text>
                        <Text style={{fontWeight: 'bold', fontSize: 14, marginVertical: 10, color: styles.color.placeholderInput}}>
                            {GlobalDataRequest.dateFromNow(this.dataDetail.created_at)}
                        </Text>
                        <HTML
                            html={this.dataDetail.body}
                            debug={true}
                            allowedStyles={[]}
                            imagesMaxWidth={Dimensions.get('window').width-20}
                            //{...defaultRenderer}
                        />
                    </View>

                </Content>
            </Container>
        );
    }
}
