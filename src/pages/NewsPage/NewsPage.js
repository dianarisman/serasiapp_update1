import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
    Card, CardItem, Thumbnail} from 'native-base';
import variables from "../../variables/MyGlobalVariable";
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import styles from "../../styles/MainStyle";
import {RefreshControl, Image, View, TouchableOpacity, ActivityIndicator} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import moment from 'moment';
import 'moment/min/locales';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import TryAgainButtonFetch from "../../components/TryAgainButtonFetch/TryAgainButtonFetch";
export default class NewsPage extends Component {

    constructor(props) {
        super(props);
        //randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
        moment.locale('id');
        this.state = {
            isLoading: false,
            isFetchFailed: false,
            user_token: this.props.navigation.getParam('user_token'),
            loadMoreIsVisible: false,
            newsList: []
        }
        this.mounted = false;
    }


    _onContentRefresh = () => {
        this.getNews(this.state.user_token)
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    componentDidMount(): void {
        this.mounted = true;
       this.getNews(this.state.user_token)
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    getNews(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getNews(token).then((result) => {
            //console.log('newsList Object',Object.keys(result.data[0]));
            //console.log('newsList', result);
            if(this.mounted) {
                this.setState({
                    isLoading: false,
                    newsList: result.data
                    //my_profile: result
                });
            }
        }).catch(err => {
            GlobalDataRequest.consoleLog('getNews', err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    renderNews() {
        return this.state.newsList.map((item, index) => {
            //const randomColor = "000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
            return (
                <TouchableOpacity key={index} activeOpacity={0.5} onPress={()=> this.props.navigation.navigate('NewsDetailPage', {dataDetail: item})}>
                    <Card>
                        <CardItem cardBody>
                            <Image source={{uri: GlobalDataRequest.renderImageUri(item.image)}} style={{height: 250, width: null, flex: 1}}/>
                            <View style={{
                                position: 'absolute',
                                width: '100%',
                                padding: 5,
                                left: 0,
                                bottom: 0,
                                backgroundColor: 'rgba(0, 0, 0, 0.251)'
                            }}>
                                <Text style={{color: '#fff'}}>
                                    {item.title}
                                </Text>
                                <Text style={{color: '#fff'}}>
                                    { moment(item.created_at).fromNow() }
                                </Text>
                            </View>
                        </CardItem>
                    </Card>
                </TouchableOpacity>
            );
        });
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this.getNews.bind(this)}
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content
                    style={styles.otherStyle.containerMainWhite}
                         refreshControl={
                             <RefreshControl
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                             />}
                >

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        {this.renderNews()}
                    </View>
                    { this.state.loadMoreIsVisible ? <ActivityIndicator style={{margin: 20}} color={styles.color.placeholderInput} size='large'/> : null }
                </Content>
            </Container>
        );
    }
}
