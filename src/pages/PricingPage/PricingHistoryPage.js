import React, { Component } from 'react';
import { Container, Content, Text, Card, CardItem, Body} from 'native-base';
import styles from "../../styles/MainStyle";
import {FlatList, RefreshControl, TouchableOpacity, View, Image} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import moment from 'moment';
import 'moment/min/locales';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import TryAgainButtonFetch from "../../components/TryAgainButtonFetch/TryAgainButtonFetch";
import variables from "../../variables/MyGlobalVariable";

export default class PricingHistoryPage extends Component {

    constructor(props) {
        super(props);
        //randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
        moment.locale('id');
        this.state = {
            isMount: false,
            isLoading: false,
            isFetchFailed: false,
            user_token: this.props.navigation.getParam('user_token'),
            loadMoreIsVisible: false,
            pricingHistoryList: []
        };
        this.mounted = false;
    }


    _onContentRefresh = () => {
        this.getPricingHistory(this.state.user_token)
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    componentDidMount() {
        this.mounted = true;
        this.getPricingHistory(this.state.user_token);
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    pricingTryAgain(){
        this.getPricingHistory(this.state.user_token);
    }

    getPricingHistory(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getHistoryPackage(token).then((result) => {
            console.log('getPricingHistory ', result);
            this.setState({isLoading: false});
            if(this.mounted){
                if(result.success){
                    console.log('getPricingHistory', result);
                    this.setState({
                        pricingHistoryList: result.data

                    });
                }
            }
        }).catch(err => {
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    renderHistory() {
        /*return this.state.newsList.map((item, index) => {
            //const randomColor = "000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
            return (
                <TouchableOpacity key={index} activeOpacity={0.5} onPress={()=> this.props.navigation.navigate('NewsDetailPage', {dataDetail: item})}>
                    <Card>
                        <CardItem cardBody>
                            <Image source={{uri: item.image}} style={{height: 250, width: null, flex: 1}}/>
                            <View style={{
                                position: 'absolute',
                                width: '100%',
                                padding: 5,
                                left: 0,
                                bottom: 0,
                                backgroundColor: 'rgba(0, 0, 0, 0.251)'
                            }}>
                                <Text style={{color: '#fff'}}>
                                    {item.title}
                                </Text>
                                <Text style={{color: '#fff'}}>
                                    { moment(item.created_at).fromNow() }
                                </Text>
                            </View>
                        </CardItem>
                    </Card>
                </TouchableOpacity>
            );
        });*/
    }
    renderItem = data => (
        <TouchableOpacity
            key={data.item.id.toString()}
            style={{marginVertical: 5, backgroundColor: '#fff', paddingVertical: 5, paddingHorizontal: 10}}
            onPress={() => this.props.navigation.navigate('UploadProofOfPayment', {
                paymentData: data.item})}
        >
            <Card>
                <CardItem bordered>
                    <Body>
                    <Text>{data.item.order_detail[0].layanan.categories.name} {data.item.order_detail[0].layanan.roles.display_name}</Text>
                    <Text note>{variables.code} : {data.item.order_code}</Text>
                    <Text note>{variables.price} : Rp. {GlobalDataRequest.numberFormat(data.item.order_detail[0].layanan.price)}</Text>
                    </Body>
                </CardItem>
                <CardItem bordered>
                    <Text>{GlobalDataRequest.statusPaymentHandler(data.item.status)}</Text>
                </CardItem>
            </Card>
        </TouchableOpacity>);

    renderItem2 = data => {
        let bgColor = data.item.order_detail[0].layanan.background_color;
        if(bgColor === null || bgColor === undefined || bgColor === ''){
            bgColor = '#b4dcef'
        }
        return (
        <TouchableOpacity
            key={data.item.id.toString()}
            onPress={() => this.props.navigation.navigate('UploadProofOfPayment', {
                paymentData: data.item})}
            style={{
                margin: 10,
                backgroundColor: bgColor,
                borderRadius: 13,
                padding: 10,
                alignItems: 'center'
            }}>
            <View style={{
                position: 'absolute',
                left: 20,
                top: 20
            }}>
                <Image
                    style={{
                        width: 40,
                        height: 40
                    }}
                    source={{uri: GlobalDataRequest.renderImageUri(data.item.order_detail[0].layanan.background_image)}}/>
            </View>
            <Text
                style={{
                    fontSize: 16,
                    fontWeight: 'bold'
                }}
            >{data.item.order_detail[0].layanan.categories.name} {data.item.order_detail[0].layanan.roles.display_name}</Text>
            <View
                style={{
                    flexDirection: 'row',
                    paddingVertical: 10
                }}>
                <Text
                    style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        textTransform: 'uppercase'
                    }}>Rp. {GlobalDataRequest.numberFormat(data.item.order_detail[0].layanan.price/1000)}</Text>
                <View
                    style={{
                        flexDirection: 'row'
                    }}>
                    <Text
                        style={{
                            fontSize: 12,
                            fontWeight: 'bold',
                            textTransform: 'uppercase'
                        }}>.000</Text>
                    <Text
                        style={{
                            fontSize: 12,
                        }}> / {data.item.order_detail[0].layanan.durasi} {variables.month}</Text>
                </View>
            </View>
            <Text
                style={{
                    marginTop: -10,
                    backgroundColor: 'rgba(0, 0, 0, 0.251)',
                    paddingHorizontal: 10,
                    borderRadius: 10,
                    color: '#fff',
                    fontSize: 13
                }}
            >{GlobalDataRequest.statusPaymentHandler(data.item.status)}</Text>
        </TouchableOpacity>)};

    render() {
        return (
            <Container>
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this.pricingTryAgain.bind(this)}
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content
                    style={styles.otherStyle.containerMainWhite}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >

                    <FlatList
                        data={this.state.pricingHistoryList}
                        renderItem={item => this.renderItem2(item)}
                        keyExtractor={item => item.id.toString()}
                        extraData={this.state}
                    />
                </Content>
            </Container>
        );
    }
}
