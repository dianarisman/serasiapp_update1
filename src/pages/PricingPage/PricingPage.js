import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    CardItem, Card
} from 'native-base';
import styles from "../../styles/MainStyle";
import {ActivityIndicator, Dimensions, Image, RefreshControl, ScrollView, TouchableOpacity, View, Alert} from "react-native";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import variables from "../../variables/MyGlobalVariable";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import TryAgainButtonFetch from "../../components/TryAgainButtonFetch/TryAgainButtonFetch";

const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');

export default class PricingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isFetchFailed: false,
            loadMoreIsVisible: false,
            packageSelect: null,
            user_token: this.props.navigation.getParam('user_token'),
            count_package: 0,
            packageList: [],
            packageExisting: [],
        };
        this.mounted = false;
    }

    componentDidMount(): void {
        this.mounted = true;
        this._getPackageList(this.state.user_token);
        GlobalDataRequest.getCountBadgePackage().then((result)=>{
            this.setState({
                count_package: result
            });
        })
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    _onPressChoosePakcage(user_token, layanan_id) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.selectPackage(user_token, layanan_id).then((result) => {
            console.log('_onPressChoosePakcage', result);
            //alert(JSON.stringify(result));
            if(result.success){
                this.setState({
                    isLoading: false,
                    packageSelect: result.data
                });
                /*GlobalDataRequest.saveCountBadgePackage(1).then((result)=>{

                   this.setState({
                       count_package: (parseInt(result, 10)+1)
                   })
                });*/

                Alert.alert(variables.info, variables.pleasePayment, [
                        {text: 'OK', onPress: () => this.props.navigation.navigate('UploadProofOfPayment', {
                            paymentData: result.data
                            })},
                    ],
                    {cancelable: true},
                );
                //alert(JSON.stringify(result.data))
            }else{
                this.setState({
                    isLoading: false,
                });
                Alert.alert(variables.info, result.message)
            }
        }).catch(err => {
            GlobalDataRequest.consoleLog('ErrorCatch_onPressChoosePakcage', err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        });



        /*this.props.navigation.navigate('UploadProofOfPayment',
            {
                user_token: this.state.user_token
            }
            );*/
    }



    _getPackageList(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.getPackageList(token).then((result) => {
            if(this.mounted){
                if(result.success){
                    this.setState({
                        isLoading: false,
                        packageExisting: result.paket.currentPaket,
                        packageList: result.paket.paket
                    });
                }
            }

        }).catch(err => {
            GlobalDataRequest.consoleLog('ErrorCatch_getPackageList', err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({isLoading: false, isFetchFailed: true});
        });
    }

    _onContentRefresh = () => {
        this._getPackageList(this.state.user_token);
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    _onPressHistoryButton() {
        this.props.navigation.navigate('PricingHistoryPage', {
            user_token: this.state.user_token
        })
    }

    numberFormat(number){
        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    render() {
        /*let imageArraySlides2 = [], marginLeft = 0, marginRight = 0;
        this.state.packageList.forEach((data, i) => {
            if(i<=0){
                marginLeft = 0;
                marginRight = 5;
            }else if (i===(this.state.packageList.length - 1)) {
                marginLeft = 5;
                marginRight = 0;
            }else{
                marginLeft = 5;
                marginRight = 5;
            }

            let benefit = data.benefit.map((item, key) =>
                <Text
                    key={`benefit${key}`}
                    style={{
                        color: '#525252',
                        fontSize: 17
                    }}>{item}</Text>
            );

            let renderSolidButton = (
                <TouchableOpacity
                    onPress={() => this._onPressChoosePakcage(this.state.user_token, data.id)}
                    style={{
                        marginVertical: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '80%',
                        backgroundColor: data.color,
                        borderRadius: 3
                    }}
                >
                    <Text
                        style={{
                            color: '#fff',
                            padding: 10,
                            fontWeight: 'bold'
                        }}
                    >Pilih Program</Text>
                </TouchableOpacity>
            );

            let renderBorderButton = (
                <TouchableOpacity

                    onPress={() => this._onPressChoosePakcage(this.state.user_token, data.id)}
                    style={{
                        marginVertical: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '80%',
                        backgroundColor: '#fff',
                        borderRadius: 3,
                        borderWidth: 2,
                        borderColor: data.color
                    }}
                >
                    {(this.state.isLoading) ? <ActivityIndicator size="small" color="#323232" /> : <Text style={styles.formLogin.buttonText2}>{variables.chooseProgram}</Text> }

                </TouchableOpacity>
            );

            const thisImage = (
                <View key={`image${i}`}>
                    <View
                        style={{
                            width: SCREEN_WIDTH * 0.7,
                            height: SCREEN_WIDTH * 0.8,
                            marginLeft: marginLeft,
                            marginRight: marginRight,
                            borderRadius: 5,
                            borderWidth: 1.2,
                            borderColor: '#dddfe1',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text style={{
                            color: data.color,
                            fontSize: 32,
                            fontWeight: 'bold',
                            marginVertical: 3
                        }}>{data.display_name}</Text>
                        <View style={{flexDirection: 'row',
                            marginVertical: 3}}>
                            <Text
                                style={{
                                    color: '#000',
                                    fontSize: 32,
                                    fontWeight: 'bold'
                                }}>{this.numberFormat(data.price)}</Text>
                        </View>
                        <Text
                            style={{
                                color: '#525252',
                                fontSize: 17
                            }}>Benefit 1</Text>
                        <Text
                            style={{
                                color: '#525252',
                                fontSize: 17
                            }}>Benefit 2</Text>
                        <Text
                            style={{
                                color: '#525252',
                                fontSize: 17
                            }}>Benefit 3</Text>
                        <Text
                            style={{
                                color: '#525252',
                                fontSize: 17
                            }}>Benefit 4</Text>
                        {data.id !==4 ? renderBorderButton : renderSolidButton}
                    </View>

                </View>

            );
            imageArraySlides2.push(thisImage);
        });*/

        let renderButton = (data) => {
            let existing = false;
            let text = 'undefined';
            if(this.state.packageExisting.length >= 1){
                const index = this.state.packageExisting.findIndex(
                    item => data.id === item.layanan_id
                );

                if(index >= 0){
                    existing = this.state.packageExisting[index].layanan_id === data.id;
                }else {
                    existing = false;
                }

                if(existing){
                    if(this.state.packageExisting[index].status === 'approved'){
                        text = variables.packageExisting;
                    }else if(this.state.packageExisting[index].status === 'waiting'){
                        text = variables.waitingPayment
                    }else if(this.state.packageExisting[index].status === 'waiting_payment'){
                        text = variables.waitingPaymentApproved
                    }
                }else {
                    text = variables.chooseUpper
                }
            }else{
                text = variables.chooseUpper
            }
            //let existing = this.state.packageExisting.layanan_id === data.id;
            let textColor = existing ? '#000' : '#fff';
            let backgroundColor = existing ? '#fff' : styles.color.serasiColorButton;
            return (
            <TouchableOpacity disabled={existing} activeOpacity={ 0.8 } style={{...styles.formLogin.button(backgroundColor),
                width: '100%',
            }} onPress={() => this._onPressChoosePakcage(this.state.user_token, data.id)}>
                {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonTextColor(textColor)}>{text}</Text> }
            </TouchableOpacity>)
        };
        let renderPackage = [];
        let renderBenefit = [];
        this.state.packageList.forEach((data, i) => {
            data.layanan_detail.forEach((dataBenefit, i) => {
               const benefitData = (
                   <Text style={{
                       textAlign: 'center',
                       paddingVertical: 10
                   }} key={dataBenefit.id.toString()}>{dataBenefit.item}</Text>
               );
                renderBenefit.push(benefitData);
            });
            //let renderBenefit = <Text>{JSON.stringify(data.layanan_detail)}</Text>;
            const packageData2 = (
                <View
                    key={`renderPackage${i}`}
                    style={styles.otherStyle.packageContainer(data.background_color)}
                >
                    <Image
                        style={{
                            height: 150,
                            width: 150,
                        }}
                        source={{uri: GlobalDataRequest.renderImageUri(data.background_image)}}/>
                    <Text
                        style={{
                            paddingVertical: 10,
                            fontSize: 25,
                            fontWeight: 'bold',
                            textTransform: 'uppercase'
                        }}>{data.display_name}</Text>
                    <View
                        style={{
                            paddingVertical: 10,
                            flexDirection: 'row'
                        }}>
                        <Text
                            style={{
                                fontSize: 25,
                                fontWeight: 'bold',
                                textTransform: 'uppercase'
                            }}>Rp. {GlobalDataRequest.numberFormat(data.price/1000) }</Text>
                        <View
                            style={{
                                flexDirection: 'row'
                            }}>
                            <Text
                                style={{
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    textTransform: 'uppercase'
                                }}>.000</Text>
                            <Text
                                style={{
                                    fontSize: 12,
                                }}> / {data.durasi}</Text>
                        </View>
                    </View>
                    {renderBenefit}
                    {renderButton(data)}

                </View>
            );
            const packageData = (
                <Card style={{marginBottom: 20}} key={`renderPackage${i}`}>
                    <CardItem>
                        <Body >
                        <View style={{flexDirection: 'row'}}>
                            <Text
                                style={{
                                    color: styles.color.serasiColorButton,
                                    fontSize: 16
                                }}
                            >Rp. {GlobalDataRequest.numberFormat(data.price/1000) }</Text>
                            <Text style={{fontSize: 10, color: styles.color.serasiColorButton}}>.000</Text>
                            <Text
                                style={{
                                    color: styles.color.serasiColorButton,
                                    fontSize: 16,
                                }}
                            > / {data.durasi}</Text>
                        </View>
                        <Text
                            style={{
                                color: styles.color.serasiColorButton,
                                fontSize: 20,
                                fontWeight: 'bold',
                                fontStyle: 'italic',
                                marginBottom: 20
                            }}
                        >{data.display_name} </Text>
                        {renderBenefit}
                        {/*<Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    animation={true}
                                    expanded={true}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent}
                                />*/}
                        <TouchableOpacity
                            onPress={() => this._onPressChoosePakcage(this.state.user_token, data.id)}
                            style={{
                                borderWidth: 2,
                                borderColor: styles.color.serasiColorButton,
                                borderRadius: 5,
                                marginVertical: 10,
                                paddingVertical: 13,
                                width: '100%', flexDirection: 'row', justifyContent: 'center'
                            }}
                        >
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="#323232" /> : <Text
                                style={{
                                    color: styles.color.serasiColorButton
                                }}>{variables.choose}</Text> }
                        </TouchableOpacity>
                        </Body>
                    </CardItem>
                </Card>
            );
            renderBenefit = [];
            renderPackage.push(packageData2);
        });
        return (
            <Container>

                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this._getPackageList.bind(this, this.state.user_token)}
                    backIconVisible={true}
                    showHistoryPricingButton={true}
                    badgeCount={this.state.count_package}
                    onPressHistoryButton={this._onPressHistoryButton.bind(this)}
                    navigation={this.props.navigation}
                />
                <Content
                    style={{flex: 1, zIndex: -1}}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >
                    {/*<Content contentContainerStyle={{ justifyContent: 'center', flex: 1, marginHorizontal: 30 }}>*/}
                    <View style={{marginVertical: 5, padding: 10}}>
                        <View
                            style={{backgroundColor: '#fff'}}
                            //flex={1}
                        >
                            {
                                /*
                                <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                            >

                                {imageArraySlides2}

                            </ScrollView>
                                */
                            }
                            {renderPackage}
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}


