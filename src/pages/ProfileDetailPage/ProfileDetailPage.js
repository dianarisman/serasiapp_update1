import React, { Component } from 'react';
import {
    ActivityIndicator,
    Animated,
    Dimensions, Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text, TouchableOpacity,
    View,
    Alert
} from 'react-native';
import Dialog from "react-native-dialog";
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import {Button, Container, Content, Footer, FooterTab, Icon} from "native-base"
import StatusMaritalDetailProfile from "../../components/StatusMaritaText/StatusMaritalDetailProfile";
import ReadMore from "../../components/ReadMore/ReadMore";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import Lightbox from "react-native-lightbox";
import ImageZoom from 'react-native-image-pan-zoom';
import AsyncStorage from "@react-native-community/async-storage";
import {NavigationEvents} from "react-navigation";

const HEADER_EXPANDED_HEIGHT = 300;
const HEADER_COLLAPSED_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT;
const str = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.
In eros est, sollicitudin sit amet risus eget, porttitor pulvinar ipsum. Nulla eget quam arcu. Mauris vel odio cursus, hendrerit augue et, ultricies massa. Phasellus pharetra et libero id semper. Sed sollicitudin commodo mi, nec efficitur sem congue vitae. Ut pellentesque augue ut lacus finibus sollicitudin. Donec a auctor augue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam vitae convallis nulla. Maecenas venenatis lorem at mi commodo pharetra. Mauris finibus hendrerit magna, sit amet ultrices turpis aliquet nec. Proin et diam suscipit, sollicitudin risus ac, porta nibh.
Aliquam pretium, elit maximus vehicula lobortis, neque dolor tempor nisl, sit amet interdum erat turpis eu metus. Sed semper libero ac diam finibus, ac interdum orci placerat. Donec nec erat ac erat rhoncus blandit. Nunc felis dui, semper eu porttitor in, porttitor vitae eros. In vel mattis est, vel molestie dui. Nulla semper nisl tempor scelerisque egestas. Duis faucibus, elit id accumsan aliquet, turpis felis scelerisque felis, quis tincidunt felis massa nec eros. Vivamus pellentesque congue velit finibus porttitor. Pellentesque eu mi lacinia sapien fermentum tincidunt sit amet eu nisl. Suspendisse pharetra ex in magna molestie venenatis.
Suspendisse non gravida tortor. Donec tristique ipsum eget arcu aliquet molestie. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam cursus purus eget accumsan maximus. Duis eu iaculis arcu. Donec iaculis, sem vel condimentum maximus, lectus nisl pellentesque dolor, non ullamcorper sapien lectus sed enim. Aenean et leo nisi. Nulla viverra magna id luctus fermentum. Donec et mauris placerat, mollis elit lacinia, cursus lacus. Donec aliquet libero arcu, non consectetur elit maximus sit amet. Quisque lacinia, libero et fermentum rutrum, lorem arcu tincidunt ante, sed iaculis velit tortor non lacus.
Sed accumsan lectus laoreet mollis cursus. Phasellus sagittis vulputate erat, non tempus dui pellentesque vel. Fusce imperdiet nulla vitae mauris facilisis bibendum. Fusce vestibulum fringilla orci, sit amet euismod nunc eleifend id. Curabitur mattis dolor at odio maximus lacinia. Vivamus ornare lorem sed augue faucibus, vel volutpat lacus elementum. Suspendisse potenti.`;


const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get("screen");


class DialogAlert extends Component {
    render() {
        const isInputEmpty = this.props.isInputEmpty;
        return (
            <Dialog.Container visible={this.props.dialogVisible}>
                <Dialog.Title>{this.props.alertTitle}</Dialog.Title>
                <Dialog.Input
                    multiline={true}
                    placeholder={variables.sendNote}
                    underlineColorAndroid='#d4d4d4'
                    numberOfLines = {3}
                    onChangeText={(text)=> this.props.onInputAlert(text)}
                    //style={{borderBottom: 1, borderColor: '#000'}}
                />
                {isInputEmpty ? <Text>{variables.noteCantBeNull}</Text> : null}
                <Dialog.Button label={variables.cancel} onPress={this.props.handleCancel} />
                <Dialog.Button label={variables.ok} onPress={this.props.handleSubmit} />
            </Dialog.Container>
        );
    }
}

export default class ProfileDetailPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dialogVisible: false,
            alertInputTextNote: '',
            showAlertRequestKeserasian: false,
            showAlertRequestCanceling: false,
            scrollY: new Animated.Value(0),
            inRelationshipStatus: this.props.navigation.getParam('inRelationshipStatus', 'undefine'),
            action_code: this.props.navigation.getParam('action_code', 'default'),
            role_id: this.props.navigation.getParam('role_id'),
            indexItem: this.props.navigation.getParam('indexItem'),
            user_token: this.props.navigation.getParam('user_token'),
            myKuota: this.props.navigation.getParam('myKuota'),
            interestID: this.props.navigation.getParam('interestID'),
            myName: ''
        };

        this.dataDetail = this.props.navigation.getParam('dataDetail');

        console.log('dataDetail ', this.dataDetail);
        //alert(JSON.stringify(this.dataDetail, null, 3));
        AsyncStorage.getItem(variables.user_name_KEY, (error, result) => {
            if (result) {
                this.setState({
                    myName: result
                })
            }
        });
    }

    componentDidMount(): void {
        //alert(this.state.role_id+'\n'+JSON.stringify(this.props.navigation.getParam('dataItemTest'), null, 3))
        //alert(JSON.stringify(this.state.action_code, null, 3));
    }

    _renderTruncatedFooter = (handlePress) => {
        return (
            <Text style={{color: '#b058ff', marginTop: 5}} onPress={handlePress}>
                {variables.readMore}
            </Text>
        );
    };

    _renderRevealedFooter = (handlePress) => {
        return (
            <Text style={{color: '#b058ff', marginTop: 5}} onPress={handlePress}>
                {variables.showLess}
            </Text>
        );
    };

    _handleTextReady = () => {
        // ...
    };

    _onPressInterested() {
        this.setState({
            isLoading: true
        });
        /*if(this.state.role_id === 8) {
            Alert.alert(variables.info,variables.pleaseBuyPackage);
            this.setState({
                isLoading: false
            });
            return;
        }*/

        if(this.state.myKuota < 1) {
            Alert.alert(variables.info,variables.pleaseBuyPackage);
            //Alert.alert(variables.info,variables.emptyQuotaMessage);
            this.setState({
                isLoading: false
            });
            return;
        }
        GlobalDataRequest.interest(this.state.user_token, this.dataDetail.id).then((result)=>{
            GlobalDataRequest.consoleLog('_onPressInterested',result);
            if(result.success) {
                //Alert.alert(variables.info,result.message);
                this.setState({
                    action_code: 'waiting',
                    interestID: result.data.likes.id
                });
                GlobalDataRequest.toastHandler(variables.success,'success','top');
                GlobalDataRequest.sendNotifInteresting(this.state.myName,variables.requestInterestingMessage,this.dataDetail.users.fcm_token,this.dataDetail.id)
            }else {
                Alert.alert(variables.info,result.message);
            }
            this.setState({
                isLoading: false
            });
        }).catch(err=>{
            Alert.alert(variables.info,err.message);
            this.setState({
                isLoading: false
            });
        });

    }

    _onRequestCanceling(userToken, interestID, status, note) {
        this.setState({
            isLoading: true
        });
        GlobalDataRequest.interestCanceling(userToken, interestID, status, note).then((result)=>{
            GlobalDataRequest.consoleLog("_onRequestCanceling", result);
            console.log('_onRequestCanceling ',result);
            if(result.success) {
                Alert.alert(variables.info,result.message);
                this.setState({
                    action_code: 'canceling'
                });
                GlobalDataRequest.sendNotifInteresting(this.dataDetail.name,variables.requestCancelingMessage,this.dataDetail.users.fcm_token,this.dataDetail.id)
            }else {
                Alert.alert(variables.info,result.message);
            }
            this.setState({
                isLoading: false
            });
        }).catch(err=>{
            Alert.alert(variables.info,err.message);
            this.setState({
                isLoading: false
            });
        });

    }

    _onRequestKeserasian(userToken, interestID, status, note) {
        this.setState({
            isLoading: true
        });
        GlobalDataRequest.interestCanceling(userToken, interestID, status, note).then((result)=>{
            console.log('_onRequestKeserasian ',result);
            if(result.success) {
                Alert.alert(variables.info,result.message);
                this.setState({
                    action_code: 'finishing'
                });
                GlobalDataRequest.sendNotifInteresting(this.dataDetail.name,variables.requestMatchMessage,this.dataDetail.users.fcm_token,this.dataDetail.id)

            }else {
                Alert.alert(variables.info,result.message);
            }
            this.setState({
                isLoading: false
            });
        }).catch(err=>{
            Alert.alert(variables.info,err.message);
            this.setState({
                isLoading: false
            });
        });

    }

    _showAlertRequestKetertarikan(name) {
        Alert.alert(variables.info, variables.alertInterestMessage+' '+name,
            [
                {
                    text: variables.cancel,
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {text: variables.ok, onPress: () => this._onPressInterested()},
            ],
            {cancelable: false},
        );

    }

    _showAlertRequestCanceling() {
        this.setState({
            showAlertRequestCanceling:  true
        });
    }

    _handleCancelRequestCanceling() {
        this.setState({
            showAlertRequestCanceling:  false
        });
    }

    _submitRequestCanceling() {
        if(this.state.alertInputTextNote === '' || this.state.alertInputTextNote === null){
            return;
        }
        this._onRequestCanceling(this.state.user_token, this.state.interestID, 'canceling', this.state.alertInputTextNote);
        this.setState({
            alertInputTextNote: '',
            showAlertRequestCanceling: false
        })
    }

    _showAlertRequestKeserasian() {
        this.setState({
            showAlertRequestKeserasian:  true
        });

    }

    _handleCancelRequestKeserasian() {
        this.setState({
            showAlertRequestKeserasian:  false
        });

    }

    _submitRequestKeserasian() {
        if(this.state.alertInputTextNote === '' || this.state.alertInputTextNote === null){
            return;
        }
        this._onRequestKeserasian(this.state.user_token, this.state.interestID, 'finishing', this.state.alertInputTextNote);
        this.setState({
            alertInputTextNote: '',
            showAlertRequestKeserasian: false
        })
    }

    _onInputAlert(text) {
        this.setState({
            alertInputTextNote: text
        })
    }


    renderScrollViewContent() {

        let kriteriaDiri_list = this.dataDetail.criteria.map( (data, index) => {
            return <Text key={index} style={styles.otherStyle.textBubble}>{data.criteria_name}</Text>
        });

        let kriteriaPasangan_list = this.dataDetail.criteria_pasangan.map( (data, index) => {
            return <Text key={index} style={styles.otherStyle.textBubble}>{data.criteria_name}</Text>
        });
        return (
            <View>
                <Text>{this.state.alertInputText}</Text>

                <DialogAlert
                    alertTitle={variables.requestCanceling}
                    isInputEmpty={this.state.alertInputTextNote === ''}
                    dialogVisible={this.state.showAlertRequestCanceling}
                    onInputAlert={this._onInputAlert.bind(this)}
                    handleCancel={this._handleCancelRequestCanceling.bind(this)}
                    handleSubmit={this._submitRequestCanceling.bind(this)}
                />

                <DialogAlert
                    alertTitle={variables.requestSerasi}
                    isInputEmpty={this.state.alertInputTextNote === ''}
                    dialogVisible={this.state.showAlertRequestKeserasian}
                    onInputAlert={this._onInputAlert.bind(this)}
                    handleCancel={this._handleCancelRequestKeserasian.bind(this)}
                    handleSubmit={this._submitRequestKeserasian.bind(this)}
                />

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.about}</Text>
                    <ReadMore
                        numberOfLines={3}
                        renderTruncatedFooter={this._renderTruncatedFooter}
                        renderRevealedFooter={this._renderRevealedFooter}
                        onReady={this._handleTextReady}>
                        <Text>
                            {this.dataDetail.description}
                        </Text>
                    </ReadMore>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.biodata}</Text>
                    <View style={{
                        flex: 1
                    }}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.education}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.dataDetail.pendidikan}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.job}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.dataDetail.pekerjaan}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.hobbyPlaceholder}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.dataDetail.hobby}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.height}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{parseInt(this.dataDetail.tb, 10)}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.weight}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{parseInt(this.dataDetail.bb, 10)}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.status}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.dataDetail.status_marital}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.city}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.dataDetail.city}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.nationality}</Text><Text> : </Text><Text style={styles.otherStyle.textWrap}>{this.dataDetail.kewarganegaraan}</Text>
                        </View>
                    </View>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.kriteriaDiri}</Text>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {kriteriaDiri_list}
                    </View>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.kriteriaPasangan}</Text>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {kriteriaPasangan_list}
                    </View>
                </View>

                {this._renderButtonLike()}
            </View>

        );
    }

    _renderButtonLike() {
        let buttonLike = null;
        if(this.state.inRelationshipStatus === 'approved'){
            buttonLike = (
                <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button(styles.color.serasiColorButton4), margin: 5}} onPress={() => {} }>
                    <Text style={styles.formLogin.buttonText2}>{variables.in_relationship}</Text>
                </TouchableOpacity>
            );
        }else{
            if(this.state.action_code === 'waiting') {
                buttonLike = (
                    <View>
                        <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton4), margin: 5}} onPress={() => {} }>
                            <Text style={styles.formLogin.buttonText2}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.waitingApproved}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton), margin: 5}} onPress={() => this._showAlertRequestCanceling() }>
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.requestCanceling}</Text> }
                        </TouchableOpacity>
                    </View>
                );
            }else if(this.state.action_code === 'canceling') {
                buttonLike = (
                    <View>
                        <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton4), margin: 5}} onPress={() => {} }>
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="'#323232'" /> : <Text style={styles.formLogin.buttonText2}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.waitingCanceling}</Text> }
                        </TouchableOpacity>
                    </View>

                );
            }else if(this.state.action_code === 'approved') {
                buttonLike = (
                    <View>
                        <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton), margin: 5}} onPress={() => this._showAlertRequestKeserasian() }>
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.requestSerasi}</Text> }
                        </TouchableOpacity>
                        <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton4), margin: 5}} onPress={() => this._showAlertRequestCanceling() }>
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText2}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.requestCanceling}</Text> }
                        </TouchableOpacity>
                    </View>

                );
            }else if(this.state.action_code === 'finishing') {
                buttonLike = (
                    <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton4), margin: 5}} onPress={() => {} }>
                        <Text style={styles.formLogin.buttonText2}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.waitingApproved}</Text>
                    </TouchableOpacity>
                );
            }else{
                buttonLike = (
                    <View>
                        <TouchableOpacity  activeOpacity={ 0.8 } style={{...styles.formLogin.button((this.dataDetail.in_relationship) ? styles.color.placeholderInput : styles.color.serasiColorButton3), margin: 5}} onPress={() => this._showAlertRequestKetertarikan(this.dataDetail.name) }>
                            {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{this.dataDetail.in_relationship ? variables.in_relationship : variables.interested}</Text> }
                        </TouchableOpacity>
                    </View>

                );
            }
        }

        return buttonLike;
    }

    componentWillMount() {
        const profile_url_image = GlobalDataRequest.renderAvatar(this.dataDetail.users.avatar, this.dataDetail.gender);
        Image.getSize(profile_url_image, (width, height) => {
            this.setState({
                image_width: width,
                image_height: height,
                image_width_transform: SCREEN_WIDTH,
                image_height_transform: height * (SCREEN_WIDTH/width),
            })
        });
    }

    _pageOnBlur(index, response) {
        const { navigation } = this.props;
        navigation.state.params._onInterestSuccess(index, response);
    }

    renderImageZoom(imageUrl) {
        return (
            <ImageZoom
                clickDistance={1}
                doubleClickInterval={0}
                ref={ref => (this.ImageZoom = ref)}
                minScale={1}
                cropWidth={SCREEN_WIDTH}
                cropHeight={SCREEN_HEIGHT}
                imageWidth={this.state.image_width_transform}
                imageHeight={this.state.image_height_transform}
                panToMove={true}
                >
                <Image
                    style={{width:this.state.image_width_transform, height:this.state.image_height_transform}}
                    source={{uri: imageUrl}}/>
            </ImageZoom>
        );
    }
    render() {


        return (
            <Container>
                <NavigationEvents
                    onWillFocus={payload => console.log('will focus')}
                    onDidFocus={payload => console.log('did focus')}
                    onWillBlur={payload => this._pageOnBlur(this.state.indexItem, this.state.action_code)}
                    onDidBlur={payload => console.log('did blur')}
                />
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content  style={styles.otherStyle.containerMain}>
                    <View>
                        <Lightbox
                            renderHeader={close => (
                                <TouchableOpacity onPress={close}>
                                    <Text style={styles.otherStyle.closeButtonImageView}>Close</Text>
                                </TouchableOpacity>
                            )}
                            swipeToDismiss={false}
                            renderContent={this.renderImageZoom.bind(this, GlobalDataRequest.renderAvatar(this.dataDetail.users.avatar, this.dataDetail.gender))}>
                            <Image
                                style={
                                    styless.backgroundImage}
                                source={{uri: GlobalDataRequest.renderAvatar(this.dataDetail.users.avatar, this.dataDetail.gender)}}
                            />

                        </Lightbox>

                        <View style={{position: 'absolute', bottom: 10, left: 10,
                            borderRadius: 6,
                            padding: 6,
                            backgroundColor: 'rgba(0, 0, 0, 0.251)'}}>
                            <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>{this.dataDetail.name}, {GlobalDataRequest.getAge(this.dataDetail.birth_date)}</Text>
                            <StatusMaritalDetailProfile
                                statusMarital={this.dataDetail.status_marital}
                                city={this.dataDetail.city}
                            />
                        </View>
                    </View>
                    {this.renderScrollViewContent()}
                </Content>
            </Container>
        );
    }
}

const styless = StyleSheet.create({
    scrollContainer: {
        //padding: 16,
        paddingTop: HEADER_EXPANDED_HEIGHT
    },
    header: {
        flex: 1,
        backgroundColor: styles.color.statBarColor,
        position: 'absolute',
        width: SCREEN_WIDTH,
        top: 0,
        left: 0,
        zIndex: 9999
    },
    backgroundImage: {
        width: SCREEN_WIDTH,
        height: HEADER_EXPANDED_HEIGHT,
        resizeMode: 'cover',
    },
    title: {
        marginVertical: 16,
        color: "black",
        fontWeight: "bold",
        fontSize: 24
    }
});
