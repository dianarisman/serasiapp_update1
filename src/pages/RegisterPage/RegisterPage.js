import React, { Component } from 'react';
import styles from "../../styles/MainStyle";
import {TouchableOpacity, ActivityIndicator, Image} from 'react-native';
import {
    Container,
    Content,
    Icon,
    Text,
    Item,
    Input,
    List} from 'native-base';
import variables from "../../variables/MyGlobalVariable";
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import AsyncStorage from "@react-native-community/async-storage";

export default class RegisterPage extends Component {

    // noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: null ,
    };
    constructor(props){
        super(props);
        this.state =
            {
                token: "",
                hidePassword: true,
                secureTextEntry: true,
                emptyPassword: false,
                emptyPassword2: false,
                invalidPasswordLength: false,
                emptyEmail: false,
                emptyPhone: false,
                passwordIncorrect: false,
                is_password: true,
                isLoading: false,
                password: '',
                passwordConfirm: '',
                email: '',
                phone: '',
                cacheRegist: '',

            };
        //console.log('constructor ', new Date().getSeconds(), new Date().getMilliseconds());
    }

    componentWillMount(): void {

    }

    managePasswordVisibility = () =>
    {
        this.setState({
            hidePassword: !this.state.hidePassword,
            secureTextEntry: !this.state.secureTextEntry,
            is_password: !this.state.is_password,
        });
    };

    next() {
        const param = {
            email: this.state.email,
            phone: this.state.phone,
            password: this.state.password,
            password_confirm: this.state.passwordConfirm
        };

        if(this.state.password.length < 6) {
            this.setState({
                invalidPasswordLength: true
            });
        }

        if(this.state.email === "") {
            this.setState({
                emptyEmail: true
            });
        }else {
            this.setState({
                emptyEmail: false
            });
        }

        if(this.state.phone === "") {
            this.setState({
                emptyPhone: true
            });
        }else {
            this.setState({
                emptyPhone: false
            });
        }

        if(this.state.password === "") {
            this.setState({
                emptyPassword: true
            });
        }else {
            this.setState({
                emptyPassword: false
            });
        }

        if(this.state.passwordConfirm === "") {
            this.setState({
                emptyPassword2: true
            });
        }else {
            this.setState({
                emptyPassword2: false
            });
        }


        if(this.state.password !== this.state.passwordConfirm) {
            this.setState({
                passwordIncorrect: true
            });
            return
        }else{
            this.setState({
                passwordIncorrect: false
            });
        }

        if(this.state.email === "" || this.state.password === "" || this.state.password.length < 6){
            return
        }
        //alert(JSON.stringify(param));
        this.props.navigation.navigate('RegisterStep3', param);
    }
    render() {
        /*const {cacheRegist} = this.state;
        const input = {
            email: cacheRegist === null ? null : cacheRegist.email,
            phone: cacheRegist === null ? null : cacheRegist.phone,
            password: cacheRegist === null ? null : cacheRegist.password,
            password_confirmation: cacheRegist === null ? null : cacheRegist.password_confirmation,
            photo_ktp: cacheRegist === null ? null : cacheRegist.photo_ktp,
            photo_ktp_selfie: cacheRegist === null ? null : cacheRegist.photo_ktp_selfie,
            nik: cacheRegist === null ? null : cacheRegist.nik,
            name: cacheRegist === null ? null : cacheRegist.name,
            gender: cacheRegist === null ? null : cacheRegist.gender,
            agama: cacheRegist === null ? null : cacheRegist.agama,
            birth_place: cacheRegist === null ? null : cacheRegist.birth_place,
            birth_date: cacheRegist === null ? null : cacheRegist.birth_date,
            pendidikan: cacheRegist === null ? null : cacheRegist.pendidikan,
            hobby: cacheRegist === null ? null : cacheRegist.hobby,
            status_marital: cacheRegist === null ? null : cacheRegist.status_marital,
            pekerjaan: cacheRegist === null ? null : cacheRegist.pekerjaan,
            address: cacheRegist === null ? null : cacheRegist.address,
            city: cacheRegist === null ? null : cacheRegist.city,
            criteria_diri: cacheRegist === null ? [] : (cacheRegist.criteria_diri.length === 0 ? null : cacheRegist.criteria_diri),
            criteria_pasangan: cacheRegist === null ? [] : (cacheRegist.criteria_pasangan.length === 0 ? null : cacheRegist.criteria_pasangan),
            anak_ke: cacheRegist === null ? null : cacheRegist.anak_ke,
            jml_saudara: cacheRegist === null ? null : cacheRegist.jml_saudara,
            kewarganegaraan: 'WNA',
            description: 'Hay saya baru bergabung di serasi app'
        };*/
        return (
            <Container>
                <MyToolbar navigation={this.props.navigation} title="Data Akun"/>
                <Content contentContainerStyle={{ marginHorizontal: 30 }}>
                    <List>
                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={styles.registerPage.listItemHeaderText}>
                                {variables.accountDetail}
                            </Text>
                        </Item>
                        <Item>
                            <Input
                                keyboardType='email-address'
                                disabled={this.state.isLoading}
                                placeholder={variables.emailPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({email: text})}
                                value={this.state.email}
                            />
                        </Item>
                        {(this.state.emptyEmail) ? <Text style={{ color: styles.color.errorInput}}>{variables.emailEmptyErrorMessage}</Text> : null}
                        <Item>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.phonePlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({phone: text})}
                                value={this.state.phone}
                            />
                        </Item>
                        {(this.state.emptyPhone) ? <Text style={{ color: styles.color.errorInput}}>{variables.phoneEmptyErrorMessage}</Text> : null}
                        <Item>
                            <Input
                                disabled={this.state.isLoading}
                                secureTextEntry={ this.state.secureTextEntry}
                                password={ this.state.is_password }
                                placeholder={variables.passwordPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({password: text})}
                                value={this.state.password}
                            />
                            <TouchableOpacity activeOpacity = { 0.8 } onPress = { this.managePasswordVisibility }>
                                <Icon active name={( this.state.hidePassword ) ? 'eye' : 'eye-off'} />
                            </TouchableOpacity>
                        </Item>
                        {(this.state.invalidPasswordLength) ? <Text style={{ color: styles.color.errorInput}}>{variables.invalidPasswordLengthErrorMessage}</Text> : null}
                        {(this.state.emptyPassword) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordEmptyErrorMessage}</Text> : null}
                        <Item>
                            <Input
                                disabled={this.state.isLoading}
                                secureTextEntry={ this.state.secureTextEntry}
                                password={ this.state.is_password }
                                placeholder={variables.passwordConfirmPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({passwordConfirm: text})}
                                value={this.state.passwordConfirm}
                            />
                            <TouchableOpacity activeOpacity = { 0.8 } onPress = { this.managePasswordVisibility }>
                                <Icon active name={( this.state.hidePassword ) ? 'eye' : 'eye-off'} />
                            </TouchableOpacity>
                        </Item>
                        {(this.state.emptyPassword2) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordEmptyErrorMessage}</Text> : <Text/>}
                        {(this.state.passwordIncorrect) ? <Text style={{ color: styles.color.errorInput}}>{variables.passwordIncorrectErrorMessage}</Text> : null}
                        <Item style={{borderBottomWidth: 0}}>
                            <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.buttonItem(styles.color.serasiColorButton)} onPress={() => this.next() }>
                                {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.next}</Text> }
                            </TouchableOpacity>
                        </Item>
                    </List>

                </Content>
            </Container>
        );
    }
}
