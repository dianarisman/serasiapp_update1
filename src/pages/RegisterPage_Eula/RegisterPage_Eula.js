import React, { Component } from 'react';
import { Container, Content, Card, CardItem, Text, Body, ListItem, CheckBox } from 'native-base';
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import styles from "../../styles/MainStyle";
import {ActivityIndicator, TouchableOpacity, View, ScrollView, Dimensions } from "react-native";
import variables from "../../variables/MyGlobalVariable";
import HTML from 'react-native-render-html';
import eula from '../../assets/eula.html';

/*const htmlContent = `<div id="placeholders">

<h2>End-User License Agreement (EULA) of <span class="app_name"><span class="highlight preview_app_name">My App</span></span></h2>

<p>This End-User License Agreement ("EULA") is a legal agreement between you and <span class="company_name"><span class="highlight preview_company_name">My Company</span></span></p>

<p>This EULA agreement governs your acquisition and use of our <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software ("Software") directly from <span class="company_name"><span class="highlight preview_company_name">My Company</span></span> or indirectly through a <span class="company_name"><span class="highlight preview_company_name">My Company</span></span> authorized reseller or distributor (a "Reseller").</p>

<p>Please read this EULA agreement carefully before completing the installation process and using the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software. It provides a license to use the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software and contains warranty information and liability disclaimers.</p>

<p>If you register for a free trial of the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software, this EULA agreement will also govern that trial. By clicking "accept" or installing and/or using the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software, you are confirming your acceptance of the Software and agreeing to become bound by the terms of this EULA agreement.</p>

<p>If you are entering into this EULA agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity and its affiliates to these terms and conditions. If you do not have such authority or if you do not agree with the terms and conditions of this EULA agreement, do not install or use the Software, and you must not accept this EULA agreement.</p>

<p>This EULA agreement shall apply only to the Software supplied by <span class="company_name"><span class="highlight preview_company_name">My Company</span></span> herewith regardless of whether other software is referred to or described herein. The terms also apply to any <span class="company_name"><span class="highlight preview_company_name">My Company</span></span> updates, supplements, Internet-based services, and support services for the Software, unless other terms accompany those items on delivery. If so, those terms apply.</p>

<h3>License Grant</h3>

<p><span class="company_name"><span class="highlight preview_company_name">My Company</span></span> hereby grants you a personal, non-transferable, non-exclusive licence to use the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software on your devices in accordance with the terms of this EULA agreement.</p>

<p>You are permitted to load the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software (for example a PC, laptop, mobile or tablet) under your control. You are responsible for ensuring your device meets the minimum requirements of the <span class="app_name"><span class="highlight preview_app_name">My App</span></span> software.</p>

<p>You are not permitted to:</p>

<ul>
<li>Edit, alter, modify, adapt, translate or otherwise change the whole or any part of the Software nor permit the whole or any part of the Software to be combined with or become incorporated in any other software, nor decompile, disassemble or reverse engineer the Software or attempt to do any such things</li>
<li>Reproduce, copy, distribute, resell or otherwise use the Software for any commercial purpose</li>
<li>Allow any third party to use the Software on behalf of or for the benefit of any third party</li>
<li>Use the Software in any way which breaches any applicable local, national or international law</li>
<li>use the Software for any purpose that <span class="company_name"><span class="highlight preview_company_name">My Company</span></span> considers is a breach of this EULA agreement</li>
</ul>

<h3>Intellectual Property and Ownership</h3>

<p><span class="company_name"><span class="highlight preview_company_name">My Company</span></span> shall at all times retain ownership of the Software as originally downloaded by you and all subsequent downloads of the Software by you. The Software (and the copyright, and other intellectual property rights of whatever nature in the Software, including any modifications made thereto) are and shall remain the property of <span class="company_name"><span class="highlight preview_company_name">My Company</span></span>.</p>

<p><span class="company_name"><span class="highlight preview_company_name">My Company</span></span> reserves the right to grant licences to use the Software to third parties.</p>

<h3>Termination</h3>

<p>This EULA agreement is effective from the date you first use the Software and shall continue until terminated. You may terminate it at any time upon written notice to <span class="company_name"><span class="highlight preview_company_name">My Company</span></span>.</p>

<p>This EULA was created by <a href="http://allbestsistem.com">allbestsistem.com</a> for <span class="app_name"><span class="highlight preview_app_name">My App</span></span></p>

<p>It will also terminate immediately if you fail to comply with any term of this EULA agreement. Upon such termination, the licenses granted by this EULA agreement will immediately terminate and you agree to stop all access and use of the Software. The provisions that by their nature continue and survive will survive any termination of this EULA agreement.</p>

<h3>Governing Law</h3>

<p>This EULA agreement, and any dispute arising out of or in connection with this EULA agreement, shall be governed by and construed in accordance with the laws of <span class="country"><span class="highlight preview_country">My Country</span></span>.</p>


</div>
`;*/

const htmlContent = `<p style="font-size: 20px;font-weight: bold"><strong>Syarat dan ketentuan Aplikasi SERASI</strong></p>
<p>&nbsp;<strong>Tujuan dari Ruang Serasi</strong></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ruang Serasi adalah Lembaga yang menjadi media mempertemukan anggota yang berkeinginan membentuk keluarga yang harmonis dan diridhoi Tuhan YME. Ruang Serasi selalu mendoakan agar anggotanya mendapatkan pasangan yang diinginkan untuk pernikahan yang bahagia dan legal.</p>
<p>&nbsp;<strong>KETENTUAN-KETENTUAN ANGGOTA SERASI</strong></p>
<ol>
<li>Bagi calon anggota Serasi yang ingin bergabung menjadi anggota Serasi adalah yang <strong>benar-benar serius</strong> ingin menikah dengan cara yang benar.</li>
<li>Anggota Serasi sanggup dan bersedia mengikuti ketentuan yang berlaku di Ruang Serasi.</li>
<li>Anggota Serasi ialah anggota yang telah memilih Program (<strong>PREMIUM</strong>, <strong>EKSEKUTIF</strong>, dan <strong>VIP</strong>).</li>
<li>Sebelum menjadi anggota Serasi, pihak Serasi akan memverifikasi data dan bukti pembayaran sebelum anggota disahkan menjadi anggota Serasi.</li>
<li>Sebelum anggota Serasi memulai percakapan dengan anggota Serasi lainnya, pihak Serasi akan memverifikasi data Anda terlebih dahulu.</li>
<li>Apabila kuota perkenalan telah habis tanpa ada kelanjutan dari kedua pihak, maka pihak yang bersangkutan diwajibkan mengikuti Konsultasi dan Terapi.</li>
<li><strong>Dilarang keras melakukan SARA, berkata kasar, malakukan hal yang tidak menyenangkan dan modus kejahatan apapun kepada anggota Serasi lainnya.</strong></li>
<li>Bagi anggota yang sudah mendaftar sebagai anggota Program, maka uang tidak dapat dikembalikan.</li>
<li>Anggota wajib aktif berkomunikasi jika sudah disetujui oleh pihak Serasi.</li>
<li>Kuota perkenalan yang sudah terpakai tidak dapat kembalikan.</li>
<li>Layanan Konsultasi dan Terapi dimulai pada jam kerja, Hari Senin-Sabtu pukul 08.00 &ndash; 16.00 WIB.</li>
<li>Data yang sudah diserahkan tidak dapat dikembalikan.</li>
<li>Hal-hal yang terjadi diluar aplikasi Serasi diluar tanggung jawab pihak Serasi.</li>
<li>Saya telah membaca, mengerti dan menyetujui sepenuhnya untuk tunduk kepada ketentuan-ketentuan tersebut diatas, berikut dengan segala penambahan dan perubahannya.</li>
</ol>
`;

export default class RegisterPage_Eula extends Component {
// noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: null ,
    };
    constructor(props){
        super(props);
        this.state = {
            isLoading : false,
            isAgree : false
        };
    }
    _next() {
        this.props.navigation.navigate('RegisterStep2');
    }

    _onCheck() {
        this.setState(
            {
                isAgree: !this.state.isAgree
            }
        );
    }

    render() {
        return (
            <Container>
                <MyToolbar navigation={this.props.navigation} title="Syarat & Ketentuan"/>
                <Content contentContainerStyle={{ marginHorizontal: 30 }}>
                    <ScrollView style={{ flex: 1 }}>
                        <HTML html={htmlContent} imagesMaxWidth={Dimensions.get('window').width} />
                        <ListItem style={{marginLeft: 1, borderBottomWidth: 0}}>
                            <CheckBox
                                style={styles.otherStyle.checkBox(
                                    this.state.isAgree ? styles.color.serasiColorButton : styles.color.placeholderInput,
                                    this.state.isAgree ? styles.color.serasiColorButton : styles.color.transparentColor
                                )}
                                checked={this.state.isAgree}
                                onPress={() => this._onCheck()}
                            />
                            <Body>
                            <Text>{variables.iAgree}</Text>
                            </Body>
                        </ListItem>
                    </ScrollView>
                    <TouchableOpacity  disabled={!this.state.isAgree} activeOpacity={ 0.8 } style={styles.formLogin.buttonItem((this.state.isAgree) ? styles.color.serasiColorButton : styles.color.placeholderInput)} onPress={() => this._next() }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.next}</Text> }
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}
