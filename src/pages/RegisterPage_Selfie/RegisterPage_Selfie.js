import React, { Component } from 'react';
import {
    Image,
    View,
    TouchableOpacity,
    Alert,
    Modal,
    TouchableHighlight,
    ScrollView, ActivityIndicator, FlatList
} from 'react-native';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Toast, Input, Segment, Card, CardItem, Item, List, ListItem, Picker
} from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import { Dimensions } from 'react-native';
import GlobalDataRequest from "../../services/GlobalDataRequest";
import variables from "../../variables/MyGlobalVariable";
import styles from "../../styles/MainStyle";
import pictureKtp from '../../images/foto_ktp.png';
import moment from 'moment'
import ModalSelectCriteria from "../../components/ModalSelectCriteria/ModalSelectCriteria";
import AsyncStorage from "@react-native-community/async-storage";

/*class ItemList extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <View style={{height: 50, justifyContent: "center"}}>
                <Text>{this.props.title}</Text>
            </View>
        )
    }

}*/
export default class RegisterPage_Selfie extends Component {
// noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: null ,
    };

    constructor(props){
        super(props);
        const win = Dimensions.get('window');
        const {width, height} = Image.resolveAssetSource(pictureKtp);
        const iconheight = height * (win.width/2)/width;
        const iconwidth = (win.width/2);

        const email = this.props.navigation.getParam('email', null);
        const phone = this.props.navigation.getParam('phone', null);
        const password = this.props.navigation.getParam('password', null);
        const password_confirm = this.props.navigation.getParam('password_confirm', null);

        this.state = {
            email: email,
            phone: phone,
            password: password,
            password_confirm: password_confirm,
            iconWidth: iconwidth/1.8,
            iconHeight: iconheight/1.8,
            imageHeightKTP: null,
            imageWidthKTP: null,
            imageDataKTP: null,
            imageHeightSELFIE: null,
            imageWidthKTPSELFIE: null,
            imageDataKTPSELFIE: null,
            modalVisiblePasangan:false,
            isDateTimePickerVisible: false,
            birthDate: null,  //input
            placeOfBirth: null,   //input
            hobbyInput: null,   //input
            //hobbyInput: null,  //inputt
            statusMarital: null,   //input
            statusmaritalNoSelect: true,
            modalVisibleDiri:false,
            modalVisibleCity: false,
            list: [ "test1", "test2", "test3" ],
            listCriteriaDiri: variables.criteria_list,
            listCriteriaPasangan: variables.criteria_list,
            listCity: variables.cityList,
            citySearchText: undefined,
            selectCriteriaPasangan: [], //input
            selectCriteriaDiri: [], //input
            selectCity: null,   //input
            tester: 0,
            invalidNik: false,
            identityCard: null, //input
            addressInput: null, //input
            fullName: null, //input
            genderSelect: null, //input
            jobSelect: null,   //input
            jobNoSelect: true,
            educationSelect: null,
            educationNoSelect: true,
            religionSelect: null,  //input
            religionNoSelect: true,
            yourOrderInFamily: null,   //input anak ke
            numberOfSiblings: null, //input jumlah saudara
            widthMatchParent: win.width,
            cacheRegist: null

        };

        this.arrayHolderCity = variables.cityList;
        //this.getImageDimensions = this.getImageDimensions.bind(this);
        this._getCache();
    }

    _getCache() {
        GlobalDataRequest.getUserRegistrationCache().then((result)=>{
            console.log('cache ready');
            let cache = result;
            if(cache === null || cache === undefined || cache === '' || cache.length <=0 ){
                this.setState({
                    cacheRegist: null
                });
            }else{
                this.setState({
                    cacheRegist: JSON.parse(cache)
                });
                let cacheRegist = JSON.parse(cache);
                this.setState({
                    //email,
                    //phone,
                    //password,
                    //password_confirm,
                    imageDataKTP: cacheRegist === null ? null : cacheRegist.photo_ktp,
                    imageDataKTPSELFIE: cacheRegist === null ? null : cacheRegist.photo_ktp_selfie,
                    identityCard: cacheRegist === null ? null : cacheRegist.nik,
                    fullName: cacheRegist === null ? null : cacheRegist.name,
                    genderSelect: cacheRegist === null ? null : cacheRegist.gender,
                    religionSelect: cacheRegist === null ? null : cacheRegist.agama,
                    placeOfBirth: cacheRegist === null ? null : cacheRegist.birth_place,
                    birthDate: cacheRegist === null ? null : cacheRegist.birth_date,
                    educationSelect: cacheRegist === null ? null : cacheRegist.pendidikan,
                    hobbyInput: cacheRegist === null ? null : cacheRegist.hobby,
                    statusMarital: cacheRegist === null ? null : cacheRegist.status_marital,
                    jobSelect: cacheRegist === null ? null : cacheRegist.pekerjaan,
                    addressInput: cacheRegist === null ? null : cacheRegist.address,
                    selectCity: cacheRegist === null ? null : cacheRegist.city,
                    selectCriteriaDiri: cacheRegist === null ? [] : (cacheRegist.criteria_diri.length === 0 ? null : cacheRegist.criteria_diri),
                    selectCriteriaPasangan: cacheRegist === null ? [] : (cacheRegist.criteria_pasangan.length === 0 ? null : cacheRegist.criteria_pasangan),
                    yourOrderInFamily: cacheRegist === null ? null : cacheRegist.anak_ke,
                    numberOfSiblings: cacheRegist === null ? null : cacheRegist.jml_saudara,
                },()=>{
                    this.checkNik(cacheRegist.nik);
                })
            }
        }).catch(()=>{
            this.setState({
                cacheRegist: null
            });
        });

    }

    selectKtpPhotoTapped() {
        GlobalDataRequest.takePhotoHandler().then((result) => {
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + result.data };
            // const source = { uri: result.uri };
            const win = Dimensions.get('window');
            const ratio = win.width/result.width;
            const imgBase64 = 'data:image/jpeg;base64,' + result.data;
            this.setState({
                imageHeightKTP: result.height * ratio,
                imageWidthKTP: win.width,
                imageDataKTP: imgBase64
            });
        });
    }

    selectPhotoTapped() {
        GlobalDataRequest.takePhotoHandler().then((result) => {
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + result.data };
            // const source = { uri: result.uri };
            const win = Dimensions.get('window');
            const ratio = win.width/result.width;
            const imgBase64 = 'data:image/jpeg;base64,' + result.data;
            this.setState({
                imageHeightSELFIE: result.height * ratio,
                imageWidthKTPSELFIE: win.width,
                imageDataKTPSELFIE: imgBase64
            });
        });
    }

    uploadBase64() {
        GlobalDataRequest.testingSaveBase64(this.state.imageData).then((resolve) => {
            Toast.show({
                text: "Status save : "+resolve.status
            });
        }, err => {
            Toast.show({
                text: "Error"
            });
        }).catch((err)=> Toast.show({
            text: "Fatal Error"
        }));
    }

    _showModalKriteriaDiri(visible) {
        this.setState({
            modalVisibleDiri: visible
        });
    }
    _getSelectedCriteriaDiri(dataList) {
        /*
        const data = [];
        const criteria = dataList;
        for(let i=0;i<criteria.length;i++){
            if (criteria[i].is_selected){
                data.push(criteria[i]);
            }
        }
        this.setState({
            selectCriteriaDiri: data
        });*/

        let selectCriteriaDiri_array = [];
        this.state.listCriteriaDiri.filter(item => item.is_selected).forEach((data, i) => {
            selectCriteriaDiri_array.push(data.title);
        });
        this.setState({
            selectCriteriaDiri: selectCriteriaDiri_array
        });
        /*
        this.setState({
            selectCriteriaDiri: this.state.listCriteriaDiri.filter(item => item.is_selected)
        });*/
    }

    _selectCriteriaDiri(indexSelect, dataList) {
        let criteria = [ ...dataList ];
        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }
        this.setState({
            listCriteriaDiri: criteria
        });
    }

    _showModalKriteriaPasangan(visible) {
        this.setState({
            modalVisiblePasangan: visible
        });
    }
    _getSelectedCriteriaPasangan(dataList) {
        /*const data = [];
        const criteria = dataList;
        for(let i=0;i<criteria.length;i++){
            if (criteria[i].is_selected){
                data.push(criteria[i]);
            }
        }
        this.setState({
            selectCriteriaPasangan: data
        });*/
        let selectCriteriaPasangan_array = [];
        this.state.listCriteriaPasangan.filter(item => item.is_selected).forEach((data, i) => {
            selectCriteriaPasangan_array.push(data.title);
        });
        this.setState({
            selectCriteriaPasangan: selectCriteriaPasangan_array
        });
    }

    _selectCriteriaPasangan(indexSelect, dataList) {
        let criteria = [ ...dataList ];
        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }
        this.setState({
            listCriteriaPasangan: criteria
        });
    }

    _showModalCity(visible) {
        this.setState({
            modalVisibleCity: visible
        });
    }

    _searchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayHolderCity.filter(function(item) {
            const itemData = `${item.city.toUpperCase()}`;
            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            listCity: newData,
            citySearchText: text,
        });
    }

    _selectCity(indexSelect, selectItem) {
        let listCity = [ ...this.state.listCity ];
        this._showModalCity(!this.state.modalVisibleCity);
        this.setState({
            selectCity: listCity[indexSelect].city,
            listCity: listCity,
        });
    }

    identityCardInput(text){

        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                // your call back function
                Alert.alert(null, variables.numberOnlyErrorMessage);
            }
        }
        this.setState({ identityCard: newText });
        this.checkNik(newText);
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        let age = GlobalDataRequest.getAge(date);
        //let age2 = moment().subtract(4, 'year').format('YYYY MM DD');
        if (age <= 17 ) {
            this._hideDateTimePicker();
            Alert.alert(variables.warning, variables.underAgeWarningMessage);
            return;
        }
        this.setState({
            birthDate: moment(date).format("YYYY-MM-DD")
        });
        this._hideDateTimePicker();
    };

    selectGender(value) {
        this.setState({ genderSelect: value });
    }

    selectJob(value: string) {
        let noSelect = value === variables.job_list[0].name;
        this.setState({
            jobNoSelect: noSelect,
            jobSelect: value
        });
    }

    selectMarital(value: string) {
        let statusmaritalNoSelect = value === variables.statusMaritalList[0].name;
        this.setState({
            statusmaritalNoSelect: statusmaritalNoSelect,
            statusMarital: value
        });
    }

    selectEducation(value: string) {
        let educationNoSelect = value === variables.education_list[0].name;
        this.setState({
            educationNoSelect: educationNoSelect,
            educationSelect: value
        });
    }

    selectRelegion(value: string) {
        let noSelect = value === variables.religion_list[0].name;
        this.setState({
            religionNoSelect: noSelect,
            religionSelect: value
        });
    }

    _yourOrderInFamily(text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            } else {
                // your call back function
                Alert.alert(null, variables.numberOnlyErrorMessage);
            }
        }
        this.setState({
            yourOrderInFamily: newText
        })
    }

    _numberOfSiblings(text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            } else {
                // your call back function
                Alert.alert(null, variables.numberOnlyErrorMessage);
            }
        }
        this.setState({
            numberOfSiblings: newText
        })
    }

    _onChangeHobby(value) {
        this.setState({ hobiNya: value });
    }

    checkNik(nik){
        if((nik+'').length < 16 || (nik+'').length >= 17 ) {
            this.setState({
                invalidNik: true
            })
        }else{
            this.setState({
                invalidNik: false
            })
        }
    }

    checkProperties(obj) {
        /*email: email,
            phone: phone,
            password: password,
            password_confirmation: password_confirm,
            photo_ktp: imageDataKTP,
            photo_ktp_selfie: imageDataKTPSELFIE,
            nik: identityCard,
            name: fullName,
            gender: genderSelect,
            agama: religionSelect,
            birth_place: placeOfBirth,
            birth_date: birthDate,
            pendidikan: educationSelect,
            hobby: hobbyInput,
            status_marital: statusMarital,
            pekerjaan: jobSelect,
            address: addressInput,
            city: selectCity,
            criteria_diri: selectCriteriaDiri,
            criteria_pasangan: selectCriteriaPasangan,
            anak_ke: yourOrderInFamily,
            jml_saudara: numberOfSiblings,*/

        let empty_email = obj.email === null || obj.email === '';
        let empty_phone = obj.phone === null || obj.phone === '';
        let empty_password = obj.password === null || obj.password === '';
        let empty_password_confirmation = obj.password_confirmation === null || obj.password_confirmation === '';
        let empty_photo_ktp = obj.photo_ktp === null || obj.photo_ktp === '';
        let empty_photo_ktp_selfie = obj.photo_ktp_selfie === null || obj.photo_ktp_selfie === '';
        let empty_nik = obj.nik === null || obj.nik === '';
        let empty_name = obj.name === null || obj.name === '';
        let empty_gender = obj.gender === null || obj.gender === '';
        let empty_agama = obj.agama === null || obj.agama === '';
        let empty_birth_place = obj.birth_place === null || obj.birth_place === '';
        let empty_birth_date = obj.birth_date === null || obj.birth_date === '';
        let empty_pendidikan = obj.pendidikan === null || obj.pendidikan === '';
        let empty_hobby = obj.hobby === null || obj.hobby === '';
        let empty_status_marital = obj.status_marital === null || obj.status_marital === '';
        let empty_pekerjaan = obj.pekerjaan === null || obj.pekerjaan === '';
        let empty_address = obj.address === null || obj.address === '';
        let empty_city = obj.city === null || obj.city === '';
        let empty_criteria_diri = obj.criteria_diri === null || obj.criteria_diri === '';
        let empty_criteria_pasangan = obj.criteria_pasangan === null || obj.criteria_pasangan === '';
        let empty_anak_ke = obj.anak_ke === null || obj.anak_ke === '';
        let empty_jml_saudara = obj.jml_saudara === null || obj.jml_saudara === '';

        const input = {
            empty_email: empty_email,
            empty_phone: empty_phone,
            empty_password: empty_password,
            empty_password_confirmation: empty_password_confirmation,
            empty_photo_ktp: empty_photo_ktp,
            empty_photo_ktp_selfie: empty_photo_ktp_selfie,
            empty_nik: empty_nik,
            empty_gender: empty_gender,
            empty_name: empty_name,
            empty_agama: empty_agama,
            empty_birth_place: empty_birth_place,
            empty_birth_date: empty_birth_date,
            empty_pendidikan: empty_pendidikan,
            empty_hobby: empty_hobby,
            empty_status_marital: empty_status_marital,
            empty_pekerjaan: empty_pekerjaan,
            empty_address: empty_address,
            empty_city: empty_city,
            empty_criteria_diri: empty_criteria_diri,
            empty_criteria_pasangan: empty_criteria_pasangan,
            empty_anak_ke: empty_anak_ke,
            empty_jml_saudara: empty_jml_saudara
        };
        this.setState({
            empty_email: empty_email,
            empty_phone: empty_phone,
            empty_password: empty_password,
            empty_password_confirmation: empty_password_confirmation,
            empty_photo_ktp: empty_photo_ktp,
            empty_photo_ktp_selfie: empty_photo_ktp_selfie,
            empty_nik: empty_nik,
            empty_gender: empty_gender,
            empty_name: empty_name,
            empty_agama: empty_agama,
            empty_birth_place: empty_birth_place,
            empty_birth_date: empty_birth_date,
            empty_pendidikan: empty_pendidikan,
            empty_hobby: empty_hobby,
            empty_status_marital: empty_status_marital,
            empty_pekerjaan: empty_pekerjaan,
            empty_address: empty_address,
            empty_city: empty_city,
            empty_criteria_diri: empty_criteria_diri,
            empty_criteria_pasangan: empty_criteria_pasangan,
            empty_anak_ke: empty_anak_ke,
            empty_jml_saudara: empty_jml_saudara
        });

        for(let key in input){
            if(input[key]) {
                console.log(key + ' masih kosong');
                return false;
            }
        }
        return true;
    }

    _next() {
        /*Alert.alert('Warning',
            'DATA AKUN'+'\n'+
            '-Email : '+ this.state.email +'\n'+
            '-Phone : '+ this.state.phone +'\n'+
            '-password : '+ this.state.password +'\n'+
            '-password_confirm : '+ this.state.password_confirm +'\n'+
            'DATA DIRI'+'\n'+
            '-KTP : '+ (this.state.imageDataKTP !== null) +'\n'+
            '-KTPSelfie : '+ (this.state.imageDataKTPSELFIE !== null) +'\n'+
            '-Nomor ktp : '+ this.state.identityCard +'\n'+
            '-Nama Lengkap : '+ this.state.fullName +'\n'+
            '-Jenis Kelamin : '+ this.state.genderSelect +'\n'+
            '-Agama : '+ this.state.religionSelect +'\n'+
            '-Tempat Lahir : '+ this.state.placeOfBirth +'\n'+
            '-Tanggal Lahir : '+ this.state.birthDate +'\n'+
            '-Pendidikan : '+ this.state.educationSelect +'\n'+
            '-Hobi : '+ this.state.hobbyInput +'\n'+
            '-Status Perkawinan : '+ this.state.statusMarital +'\n'+
            '-Pekerjaan : '+ this.state.jobSelect +'\n'+
            '-Alamat : '+ this.state.addressInput +'\n'+
            '-City : '+ this.state.selectCity +'\n'+
            '-Kriteria diri : '+ JSON.stringify(this.state.selectCriteriaDiri) +'\n'+
            '-Kriteria pasangan : '+ JSON.stringify(this.state.selectCriteriaPasangan) +'\n'+
            '-Anak : '+ this.state.yourOrderInFamily +'\n'+
            '-Jumlah Saudara : '+ this.state.numberOfSiblings +'\n'
        );*/

        const {
            email,
            phone,
            password,
            password_confirm,
            imageDataKTP,
            imageDataKTPSELFIE,
            identityCard,
            fullName,
            genderSelect,
            religionSelect,
            placeOfBirth,
            birthDate,
            educationSelect,
            hobbyInput,
            statusMarital,
            jobSelect,
            addressInput,
            selectCity,
            selectCriteriaDiri,
            selectCriteriaPasangan,
            yourOrderInFamily,
            numberOfSiblings,
        } = this.state;
        const input = {
            email: email,
            phone: phone,
            password: password,
            password_confirmation: password_confirm,
            photo_ktp: imageDataKTP,
            photo_ktp_selfie: imageDataKTPSELFIE,
            nik: identityCard,
            name: fullName,
            gender: genderSelect,
            agama: religionSelect,
            birth_place: placeOfBirth,
            birth_date: birthDate,
            pendidikan: educationSelect,
            hobby: hobbyInput,
            status_marital: statusMarital,
            pekerjaan: jobSelect,
            address: addressInput,
            city: selectCity,
            criteria_diri: selectCriteriaDiri.length === 0 ? null : selectCriteriaDiri,
            criteria_pasangan: selectCriteriaPasangan.length === 0 ? null : selectCriteriaPasangan,
            anak_ke: yourOrderInFamily,
            jml_saudara: numberOfSiblings,
            kewarganegaraan: 'WNA',
            description: 'Hay saya baru bergabung di serasi app'
        };



        //alert(JSON.stringify(this.checkProperties(input)));
        //alert(JSON.stringify(input));
        //console.log('data registrasi ', input);
        //alert(JSON.stringify(this.isEmpty(input)));
        this.setState({
            isLoading: true
        });

        if(!this.checkProperties(input) || this.state.invalidNik){
            this.setState({isLoading: false});
            GlobalDataRequest.toastHandler(variables.dataNotValid,'danger','top');
            return;
        }
        GlobalDataRequest.userRegistrationCache(JSON.stringify(input)).then((result)=> {
            console.log('userRegistrationCache ',result);
        });
        GlobalDataRequest.signUp(input).then((result)=>{
            if(result.success) {
                GlobalDataRequest.toastHandler(variables.successSignUp,'success','top');
                this.props.navigation.navigate('Login');
                console.log(result.message);
                AsyncStorage.removeItem(variables.user_registration_cache_KEY);
            }else{
                let obj = result.data;
                let error_list = '';
                for (var key in obj) {
                    error_list=error_list.concat('\n'+ obj[key]);
                }
                Alert.alert(variables.warning, error_list);
                console.log('error log result ',result);
            }

            this.setState({
                isLoading: false
            });
        }).catch(err=>{
            this.setState({
                isLoading: false
            });
            alert(variables.failedSignUpErrorMessage +' Error Message : '+err.message);
        });
    }
    render() {
        let job_list = variables.job_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let religion_list = variables.religion_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let marital_list = variables.statusMaritalList.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        let education_list = variables.education_list.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name} />
        });

        return (
            <Container>
                <MyToolbar navigation={this.props.navigation} title="Form Pendaftaran"/>
                <Content contentContainerStyle={{ marginHorizontal: 30 }}>
                    <ModalSelectCriteria
                        selectTitle={variables.kriteriaDiri}
                        isModalVisible={this.state.modalVisibleDiri}
                        onSelectItem={this._selectCriteriaDiri.bind(this)}
                        onHideModal={this._showModalKriteriaDiri.bind(this)}
                        onGetSelectedItem={this._getSelectedCriteriaDiri.bind(this)}
                        listCriteria={this.state.listCriteriaDiri}
                    />

                    <ModalSelectCriteria
                        selectTitle={variables.kriteriaPasangan}
                        isModalVisible={this.state.modalVisiblePasangan}
                        onSelectItem={this._selectCriteriaPasangan.bind(this)}
                        onHideModal={this._showModalKriteriaPasangan.bind(this)}
                        onGetSelectedItem={this._getSelectedCriteriaPasangan.bind(this)}
                        listCriteria={this.state.listCriteriaPasangan}
                    />

                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisibleCity}
                        onRequestClose={() => {
                            //Alert.alert('Modal has been closed.');
                            this._showModalCity(!this.state.modalVisibleCity);
                        }}>
                        <View style={{flex: 1,  marginHorizontal: 30,marginTop: 30}}>
                            <Item>
                                <Icon type='FontAwesome' name='search' style={{color: styles.color.serasiColorButton}}/>
                                <Input
                                    placeholder={variables.findCityPlaceholder}
                                    placeholderTextColor={styles.color.placeholderInput}
                                    onChangeText={text => this._searchFilterFunction(text)}
                                    autoCorrect={false}
                                    value={this.state.citySearchText}
                                />
                            </Item>
                            <FlatList
                                data={this.state.listCity}
                                keyExtractor={item => item.id.toString()}
                                renderItem={({item, index}) =>
                                    <ListItem onPress={()=> this._selectCity(index, item.id)}>
                                        <Left>
                                            <Text>{item.city}</Text>
                                        </Left>
                                    </ListItem>}
                            />
                        </View>
                    </Modal>

                    <List>
                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={styles.registerPage.listItemHeaderText}>
                                {variables.dataDiri}
                            </Text>
                        </Item>
                        <Item style={{borderBottomWidth: 0}}>
                            <TouchableOpacity onPress={this.selectKtpPhotoTapped.bind(this)} style={{flexDirection:'row'}}>
                                <View style={{flex: 1, borderWidth: 1, borderColor: '#ccc', borderRadius: 5, marginVertical: 5}}>
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', padding: 10}}>
                                        <Icon type='Entypo' name='v-card' style={{color: '#ccc', paddingRight:10}}/>
                                        <Text style={{color: '#c4c4c4', fontWeight: 'bold'}}>{variables.takeIdentityCardPhoto}</Text>
                                        {this.state.imageDataKTP !== null ? <Icon type="Entypo" name="check" style={{fontSize:15, paddingLeft: 5, justifyContent: 'flex-end', color: styles.color.serasiColorButton}} /> : null}

                                    </View>
                                </View>

                            </TouchableOpacity>
                        </Item>
                        {(this.state.empty_photo_ktp) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item style={{borderBottomWidth: 0}}>
                            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{flexDirection:'row'}}>
                                <View style={{flex: 1, borderWidth: 1, borderColor: '#ccc', borderRadius: 5, marginVertical: 5}}>
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', padding: 10}}>
                                        <Icon type='Entypo' name='camera' style={{color: '#ccc', paddingRight:10}}/>
                                        <Text style={{color: '#c4c4c4', fontWeight: 'bold'}}>{variables.takeIdentityCardPhotoSelfie}</Text>
                                        {this.state.imageDataKTPSELFIE !== null ? <Icon type="Entypo" name="check" style={{fontSize:15, paddingLeft: 5, justifyContent: 'flex-end', color: styles.color.serasiColorButton}} /> : null}

                                    </View>
                                </View>
                            </TouchableOpacity>
                        </Item>
                        {(this.state.empty_photo_ktp_selfie) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.identityCardPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.identityCardInput(text)}
                                value={this.state.identityCard}
                            />
                        </Item>
                        {(this.state.invalidNik) ? <Text style={{ color: styles.color.errorInput}}>{variables.invalidNikMessage}</Text> : null}
                        {(this.state.empty_nik) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Input
                                disabled={this.state.isLoading}
                                placeholder={variables.fullnamePlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({fullName: text})}
                                value={this.state.fullName}
                            />
                        </Item>
                        {(this.state.empty_name) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <View style={styles.loginPage.content_bottom}>
                                <View style={styles.loginPage.leftContainer}>
                                    <Input disabled={true} value={variables.gender}/>
                                </View>
                                <View style={styles.loginPage.rightContainer}>
                                    <Segment
                                        style={{backgroundColor: null}}
                                    >
                                        <Button
                                            style={styles.loginPage.segmentButtonFirst((this.state.genderSelect === 'pria'))}
                                            onPress={() => this.selectGender('pria')}
                                        ><Text style={styles.loginPage.segmentTextActive(this.state.genderSelect === 'pria')}>{variables.male}</Text></Button>
                                        <Button
                                            style={styles.loginPage.segmentButtonLast((this.state.genderSelect === 'wanita'))}
                                            onPress={() => this.selectGender('wanita')}
                                        ><Text style={styles.loginPage.segmentTextActive(this.state.genderSelect === 'wanita')}>{variables.female}</Text>
                                        </Button>
                                    </Segment>
                                </View>
                            </View>
                        </Item>
                        {(this.state.empty_gender) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Picker
                                disabled={this.state.isLoading}
                                mode="dialog"
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.religionPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.religionNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={this.state.religionSelect}
                                onValueChange={this.selectRelegion.bind(this)}
                            >
                                {religion_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_agama) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Input
                                disabled={this.state.isLoading}
                                placeholder={variables.placeOfBirth}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({placeOfBirth: text})}
                                value={this.state.placeOfBirth}
                            />
                        </Item>
                        {(this.state.empty_birth_place) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item onPress={this._showDateTimePicker}>
                            <Input
                                editable={false}
                                placeholder={variables.birthDatePlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                value={this.state.birthDate}
                            />
                            <Icon type='Entypo' name='calendar' style={{color: styles.color.placeholderInput}}/>

                            <DateTimePicker
                                datePickerModeAndroid='spinner'
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this._handleDatePicked}
                                onCancel={this._hideDateTimePicker}
                                date={new Date(moment().subtract(18, 'year'))}
                            />
                        </Item>
                        {(this.state.empty_birth_date) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Picker
                                disabled={this.state.isLoading}
                                mode="dialog"
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.educationPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.educationNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={this.state.educationSelect}
                                onValueChange={this.selectEducation.bind(this)}
                            >
                                {education_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_pendidikan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Input
                                disabled={this.state.isLoading}
                                placeholder={variables.hobbyPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({hobbyInput: text})}
                                value={this.state.hobbyInput}
                            />
                        </Item>
                        {(this.state.empty_hobby) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            {/**<Input disabled={true} value={variables.statusMarital}/>**/}
                            <Picker
                                mode="dialog"
                                disabled={this.state.isLoading}
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.statusMaritalPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.statusmaritalNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={this.state.statusMarital}
                                onValueChange={this.selectMarital.bind(this)}
                            >
                                {marital_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_status_marital) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Picker
                                mode="dialog"
                                disabled={this.state.isLoading}
                                iosIcon={<Icon type="AntDesign" name="caretdown" style={{color: styles.color.placeholderInput}} />}
                                placeholder={variables.selectJobPlaceholder}
                                placeholderStyle={{ color: styles.color.placeholderInput }}
                                style={(this.state.jobNoSelect) ? styles.otherStyle.pickerSelect(styles.color.placeholderInput) : styles.otherStyle.pickerSelect('#000')}
                                selectedValue={this.state.jobSelect}
                                onValueChange={this.selectJob.bind(this)}
                            >
                                {job_list}
                            </Picker>
                        </Item>
                        {(this.state.empty_pekerjaan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item >
                            <Input
                                multiline={true}
                                disabled={this.state.isLoading}
                                placeholder={variables.adressPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this.setState({addressInput: text})}
                                value={this.state.addressInput}
                            />
                        </Item>
                        {(this.state.empty_address) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <View style={styles.loginPage.content_bottom}>
                                <View style={styles.loginPage.leftContainer}>
                                    <Input disabled={true} value={variables.city}/>
                                </View>
                                <TouchableOpacity  style={styles.loginPage.rightContainer} onPress={() => {
                                    this._showModalCity(true);
                                }}>
                                    <Text style={{color: styles.color.placeholderInput}}>{this.state.selectCity === null ? variables.select : this.state.selectCity} </Text><Icon type="AntDesign" name="search1" style={{fontSize:10, color: styles.color.placeholderInput}}/>
                                </TouchableOpacity>
                            </View>
                        </Item>
                        {(this.state.empty_city) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <View style={styles.loginPage.content_bottom}>
                                <View style={styles.loginPage.leftContainer}>
                                    <Input disabled={true} value={variables.kriteriaDiri}/>
                                </View>
                                <TouchableOpacity  style={styles.loginPage.rightContainer} onPress={()=>this._showModalKriteriaDiri(true)}>
                                    <Text style={{color: styles.color.placeholderInput}}>{this.state.selectCriteriaDiri.length < 1 ? variables.select : this.state.selectCriteriaDiri.length + variables.criteria} </Text><Icon type="AntDesign" name="search1" style={{fontSize:10, color: styles.color.placeholderInput}}/>
                                </TouchableOpacity>
                            </View>
                        </Item>
                        {(this.state.empty_criteria_diri) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <View style={styles.loginPage.content_bottom}>
                                <View style={styles.loginPage.leftContainer}>
                                    <Input disabled={true} value={variables.kriteriaPasangan}/>
                                </View>
                                <TouchableOpacity  style={styles.loginPage.rightContainer} onPress={() => {
                                    this._showModalKriteriaPasangan(true);
                                }}>
                                    <Text style={{color: styles.color.placeholderInput}}>{this.state.selectCriteriaPasangan.length < 1 ? variables.select : this.state.selectCriteriaPasangan.length + variables.criteria} </Text><Icon type="AntDesign" name="search1" style={{fontSize:10, color: styles.color.placeholderInput}}/>
                                </TouchableOpacity>
                            </View>
                        </Item>
                        {(this.state.empty_criteria_pasangan) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item style={{borderBottomWidth: 0}}>
                            <Input disabled={true} editable={false}/>
                        </Item>
                        <Item style={styles.registerPage.listItemHeader}>
                            <Text style={styles.registerPage.listItemHeaderText}>
                                {variables.dataFamily}
                            </Text>
                        </Item>
                        <Item>
                            <Input disabled={true} placeholder={variables.whatYourOrderInFamily}/>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.numberCountingPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this._yourOrderInFamily(text)}
                                value={this.state.yourOrderInFamily}
                                style={{textAlign: 'right'}}
                            />
                        </Item>
                        {(this.state.empty_anak_ke) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item>
                            <Input disabled={true} placeholder={variables.numberOfSiblingsPlaceHolder}/>
                            <Input
                                keyboardType='phone-pad'
                                disabled={this.state.isLoading}
                                placeholder={variables.numberCountingPlaceholder}
                                placeholderTextColor={styles.color.placeholderInput}
                                onChangeText={(text)=> this._numberOfSiblings(text)}
                                value={this.state.numberOfSiblings}
                                style={{textAlign: 'right'}}
                            />
                        </Item>
                        {(this.state.empty_jml_saudara) ? <Text style={{ color: styles.color.errorInput}}>{variables.emptyFieldErrorMessage}</Text> : null}

                        <Item style={{borderBottomWidth: 0}}>
                            <TouchableOpacity disabled={this.state.isLoading} activeOpacity={ 0.8 } style={styles.formLogin.buttonItem(styles.color.serasiColorButton)} onPress={() => this._next() }>
                                {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.next}</Text> }
                            </TouchableOpacity>
                        </Item>
                    </List>

                </Content>
            </Container>
        );
    }
}
