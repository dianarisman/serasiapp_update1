import React, { Component } from 'react';
import GlobalDataRequest from "../../services/GlobalDataRequest";
import styles from "../../styles/MainStyle";
import {TouchableOpacity, ActivityIndicator, Alert} from 'react-native';
import {
    Container,
    Content,
    Icon,
    Text,
    Item,
    Input} from 'native-base';
import variables from "../../variables/MyGlobalVariable";
import Logo from "../../components/Logo/Logo";
import MyStatusBar from "../../components/StatusBar/MyStatusBar";
import FadeInView from "../../components/FadeInView/FadeInView";
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";

export default class ResetPasswordPage extends Component {

    // noinspection JSUnusedGlobalSymbols
    static navigationOptions = {
        header: null ,
    };
    constructor(props){
        super(props);
        this.state =
            {
                isLoading: false,
                emptyEmail: false,
                email: ''
            };
    }

    _resetPasswordRequest() {
        if(this.state.email.trim() === "") {
            this.setState({
                emptyEmail: true
            });
        }else {
            this.setState({
                emptyEmail: false
            });
        }

        if(this.state.email.trim() === ""){
            return
        }

        this.setState({
            isLoading: true
        });

        GlobalDataRequest.resetPassword(this.state.email.trim()).then((result) => {
            this.setState({ isLoading: false });
            GlobalDataRequest.consoleLog('Result_resetPassword',result);
            if(result.success){
                GlobalDataRequest.toastHandler(variables.successSendResetPassword,'success','top');
                this.props.navigation.goBack();
            }else{
                GlobalDataRequest.toastHandler(variables.failedSendResetPassword,'danger','top');
            }
            /*Alert.alert(
                variables.emailSend,
                variables.pleaseCheckEmail +this.state.email + '\nMessage '+JSON.stringify(result),
                [
                    {text: 'OK', onPress: () => console.log('ok')},
                ],
                {cancelable: false},
            );*/
        }).catch((err)=> {
            GlobalDataRequest.consoleLog('ErrorCatch_resetPassword',err.message);
            GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            this.setState({
                isLoading: false
            });
        });
    }

    render() {
        return (
            <Container>
                {/*<MyToolbar navigation={this.props.navigation} title={variables.resetPassword}/>*/}
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content contentContainerStyle={{ justifyContent: 'center', flex: 1, marginHorizontal: 30 }}>
                    <MyStatusBar/>
                    <FadeInView>
                        <Logo/>
                    </FadeInView>
                    <Item>
                        <Icon active name='mail' />
                        <Input
                            autoComplete='email'
                            disabled={this.state.isLoading}
                            placeholder={variables.emailPlaceholder}
                            placeholderTextColor={styles.color.placeholderInput}
                            onChangeText={(text) => this.setState({email: text})}
                            value={this.state.email}
                        />

                    </Item>
                    {(this.state.emptyEmail) ? <Text style={{ color: styles.color.errorInput}}>{variables.emailEmptyErrorMessage}</Text> : <Text/>}
                    <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => this._resetPasswordRequest() }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.send}</Text> }
                    </TouchableOpacity>

                </Content>
            </Container>
        );
    }
}
