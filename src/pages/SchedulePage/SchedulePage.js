import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
    Card,
    CardItem,
    Thumbnail} from 'native-base';
import styles from "../../styles/MainStyle";
import {RefreshControl, View, Image, FlatList, TouchableOpacity, Linking, Dimensions} from "react-native";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import moment from 'moment';
import GlobalDataRequest from "../../services/GlobalDataRequest";
import variables from "../../variables/MyGlobalVariable";

const {width: SCREEN_WIDTH} = Dimensions.get('window');
export default class SchedulePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            scheduleList: [
                {
                    id: 1,
                    schedule_id: 1,
                    schedule_name: 'Konsultasi Chat',
                    schedule_value: '60 Menit',
                    schedule_date: '20190406',
                    schedule_time: new Date().getTime(),

                },
                {
                    id: 2,
                    schedule_id: 2,
                    schedule_name: 'Konsultasi Bertemu Langsung',
                    schedule_value: 'Bertemu Langsung',
                    schedule_date: '20190407',
                    schedule_time: new Date().getTime(),

                },
                {
                    id: 3,
                    schedule_id: 3,
                    schedule_name: 'Terapi',
                    schedule_value: 'Bertemu Langsung',
                    schedule_date: '20190407',
                    schedule_time: new Date().getTime(),

                }
            ]
        }
    }

    _renderIcon(id){
        let icon = null
        if(id===1 || id===2) {
            icon = <Thumbnail source={require('../../images/Konsulicon.png')} />;
        }else{
            icon = <Thumbnail source={require('../../images/Terapiicon.png')} />;
        }
        return icon;
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        GlobalDataRequest.tes().then((result)=>{
            this.setState({isLoading: false});
            alert(JSON.stringify(result));
        }, () => {
            alert('cant request to server');
        });
        //setInterval(this.setState({isLoading: false}), 3000);
    };


    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                {/*<Content style={styles.otherStyle.containerMain}
                         refreshControl={
                             <RefreshControl
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                             />}
                >
                    <View style={{marginVertical: 5, padding: 10}}>
                        <FlatList
                            data={ this.state.scheduleList }
                            renderItem={ ({item, index}) =>
                                <TouchableOpacity
                                    key={index}
                                    onPress={()=> alert(JSON.stringify(item))}>
                                    <Card
                                        key={index}>
                                        <CardItem>
                                            <Left>
                                                {this._renderIcon(item.schedule_id)}
                                                <Body>
                                                <Text>{item.schedule_name}</Text>
                                                <Text note>{item.schedule_value}</Text>
                                                </Body>
                                            </Left>
                                            <Right>
                                                <Text>{moment(item.schedule_date, 'YYYYMMDD').format('ll')}</Text>
                                                <Text>{moment(item.schedule_time).format('LT')}</Text>
                                            </Right>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity> }
                            keyExtractor={item => JSON.stringify(item)}
                            extraData={this.state}
                        />
                    </View>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                        <Text>Masih Dalam Tahap Development</Text>
                    </View>
                </Content>*/}
                <Content
                    style={{...styles.otherStyle.containerMain}}
                    contentContainerStyle={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Image source={require('../../images/jadwalpage.png')}
                           style={{
                               resizeMode: 'contain',
                               height: SCREEN_WIDTH/2,
                               width: SCREEN_WIDTH/2
                           }}/>
                    <Text note style={{fontSize: 20, textAlign: 'center'}}>Bermain bersama keluarga memang hal yang menyenangkan :)</Text>

                    <View style={{position: 'absolute', bottom: 10, left: 10, right: 10}}>
                        <TouchableOpacity activeOpacity={ 0.8 } style={styles.formLogin.button(styles.color.serasiColorButton)} onPress={() => Linking.openURL("https://play.google.com/store/apps/details?id=com.serasiapp_update1") }>
                            <Text style={styles.formLogin.buttonText}>{variables.giveMeComment}</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}
