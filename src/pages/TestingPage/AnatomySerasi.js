import React, { Component } from 'react';
import { Container, Content, Text} from 'native-base';
import styles from "../../styles/MainStyle";
import {RefreshControl, View} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import moment from 'moment';
import 'moment/min/locales';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import TryAgainButtonFetch from "../../components/TryAgainButtonFetch/TryAgainButtonFetch";

export default class AnatomySerasi extends Component {

    constructor(props) {
        super(props);
        moment.locale('id');
        this.state = {
            isLoading: false,
            isFetchFailed: false,
            user_token: this.props.navigation.getParam('user_token'),
            loadMoreIsVisible: false,
            pricingHistoryList: []
        }
    }

    _onContentRefresh = () => {
        this.getPricingHistory(this.state.user_token)
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    componentDidMount(): void {
        this.getPricingHistory(this.state.user_token)
    }

    getPricingHistory(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.tes().then((result) => {
            this.setState({
                isLoading: false,
                //pricingHistoryList: result.data
                //my_profile: result

            });
            alert(JSON.stringify(result));
        }).catch(err => {
            alert('error 1 a '+err.message);
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                {this.state.isFetchFailed ? (<TryAgainButtonFetch onPress={this.getNews.bind(this)}/>) : null}
                <Content
                    style={styles.otherStyle.containerMain}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >
                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <Text>PricingHistoryPage</Text>
                    </View>
                </Content>
            </Container>
        );
    }
}
