import React, { Component } from 'react';
import {List, Form, Item, Label, Input, Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
export default class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null
        }
    }

    _inputEmail() {
        let email = this.state.email;
        if(email === '' || !email) {
            alert('isi dulu gan');
            return;
        }
        let temp_uid = email.trim().toLowerCase().substring(0, email.trim().toLowerCase().indexOf("@"));
        this.props.navigation.navigate('TestingChat3_ChatList', {
            email: email.trim().toLowerCase(),
            user_id: temp_uid+'_uid_1234'
        })
    }
    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content contentContainerStyle={{ justifyContent: 'center', flex: 1, marginHorizontal: 30 }}>
                    <List>
                        <Item>
                            <Item floatingLabel>
                                <Label>email</Label>
                                <Input
                                    value={this.state.email}
                                    onChangeText={(text) => this.setState({email: text})
                                    }
                                />
                            </Item>
                        </Item>
                    </List>
                    <Button style={{
                        marginVertical: 10
                    }} full success onPress={
                        ()=> this._inputEmail()}>
                        <Text>Next</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
