import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import firebase from "./FirebaseConfigChat";
import {GiftedChat} from "react-native-gifted-chat";
import SlackMessage from './components/SlackMessage';
import emojiUtils from 'emoji-utils';
import { Platform } from 'react-native';
export default class ChatConversation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: this.props.navigation.getParam('email'),
            user_id: this.props.navigation.getParam('user_id'),
            dataItem: this.props.navigation.getParam('dataItem'),
            messages: [],
        };

        /*console.log('firebase.name ', firebase.name);
        console.log('firebase.database ', firebase.database())
        alert(this.state.user_id+'_'+this.state.dataItem.user_id);*/
    }

    fromUID() {
        return this.state.user_id
    }

    toUID() {
        return this.state.dataItem.user_id
    }

    get ref_FROM() {
        return firebase.database().ref('Chating_Room/Users/'+this.fromUID()+'/to/'+this.toUID()+'/messages/');
    }

    get ref_TO() {
        return firebase.database().ref('Chating_Room/Users/'+this.toUID()+'/to/'+this.fromUID()+'/messages/');
    }

    get ref_chat_list_from() {
        return firebase.database().ref('Chating_Room/Users/'+this.fromUID()+'/chat_list/');
    }

    get ref_chat_list_to() {
        return firebase.database().ref('Chating_Room/Users/'+this.toUID()+'/chat_list/');
    }

    parse = snapshot => {
        const { timestamp: numberStamp, createdAt, text, user, sent, received } = snapshot.val();
        const { key: _id } = snapshot;
        const timestamp = new Date(numberStamp);
        const message = {
            _id,
            timestamp,
            createdAt,
            text,
            sent,
            received,
            user,
        };
        return message;
    };

    on = callback =>
        this.ref_FROM
            .limitToLast(20)
            .on('child_added', snapshot => callback(this.parse(snapshot)));

    onChanged = callback =>
        this.ref_FROM
            .limitToLast(20)
            .on('child_changed', snapshot => callback(this.parse(snapshot)));


    timestamp() {
        return new Date().getTime();
    }
    // send the message to the Backend
    send = messages => {
        //alert(JSON.stringify(messages));
        for (let i = 0; i < messages.length; i++) {
            const { text, user, _id: message_id } = messages[i];
            const createdAt = this.timestamp();
            const sent = true;
            const received = false;
            const timestamp = this.timestamp();
            const message = {
                text,
                user,
                message_id,
                createdAt,
                sent,
                received,
                timestamp
            };
            this.append(message);
        }

        const from = {
            user_id: this.fromUID(),
            user_email: this.state.email
        };

        const to = {
            user_id: this.toUID(),
            user_email: this.state.dataItem.email
        };

        this.ref_chat_list_from.orderByChild("user_id").equalTo(to.user_id).once("value",snapshot => {
            if (!snapshot.exists()){
                this.ref_chat_list_from.push(to);
            }
        });

        this.ref_chat_list_to.orderByChild("user_id").equalTo(from.user_id).once("value",snapshot => {
            if (!snapshot.exists()){
                this.ref_chat_list_to.push(from);
            }
        });
    };

    append(message){

        /*this.ref_chat_list_from.push(to);
        this.ref_chat_list_from.on('value', (snapshot) => {
            const chat_list = snapshot.val();
            if(chat_list === null){
                this.ref_chat_list_from.push(to);
            }else{
                Object.keys(chat_list).forEach(key => {
                    if (chat_list[key].user_id !== to.user_id) {
                        this.ref_chat_list_from.push(to);
                    }
                });
            }
        });

        this.ref_chat_list_to.on('value', (snapshot) => {
            const chat_list = snapshot.val();
            if(chat_list === null){
                this.ref_chat_list_to.push(from);
            }else{
                Object.keys(chat_list).forEach(key => {
                    if (chat_list[key].user_id !== from.user_id) {
                        this.ref_chat_list_to.push(from);
                    }
                });
            }
        });*/
        this.ref_FROM.push(message);
        this.ref_TO.push(message);
    }

    // close the connection to the Backend
    off() {
        this.ref_FROM.off();
    }

    get user() {
        return {
            name: this.state.email,
            _id: this.state.user_id,
        };
    }


    renderMessage(props) {
        console.log('renderMessage ', props);
        const { currentMessage: { text: currText } } = props;

        let messageTextStyle;

        // Make "pure emoji" messages much bigger than plain text.
        if (currText && emojiUtils.isPureEmojiString(currText)) {
            messageTextStyle = {
                fontSize: 28,
                // Emoji get clipped if lineHeight isn't increased; make it consistent across platforms.
                lineHeight: Platform.OS === 'android' ? 34 : 30,
            };
        }

        return (
            <SlackMessage {...props} messageTextStyle={messageTextStyle} />
        );
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <GiftedChat
                    messages={this.state.messages}
                    onSend={this.send}
                    user={this.user}
                    //renderMessage={this.renderMessage}
                />
            </Container>
        );
    }

    componentDidMount() {
        this.on(message =>
            this.setState(previousState => ({
                messages: GiftedChat.append(previousState.messages, message),
            }))
        );

        this.onChanged(message => {
            this.setState(previousState => ({
                messages: this._replaceAt(
                    previousState.messages,
                    previousState.messages.findIndex(x => x._id === message._id),
                    message
                    )
            }));
        });
    }

    _replaceAt(array, index, value) {
        const ret = array.slice(0);
        ret[index] = value;
        return ret;
    }

    componentWillUnmount() {
        this.off();
    }
}
