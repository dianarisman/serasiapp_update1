import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    ListItem
} from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import {FlatList, Image, StyleSheet, TouchableOpacity, View} from "react-native";

export default class ChatFindFriends extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: this.props.navigation.getParam('email'),
            user_id: this.props.navigation.getParam('user_id'),
            filter_user: [],
            user_list : [
                {
                    email: 'krisnakal@gmail.com',
                    name: 'krisnakal',
                    user_id: ('krisnakal@gmail.com').trim().toLowerCase().substring(0, ('krisnakal@gmail.com').trim().toLowerCase().indexOf("@"))+'_uid_1234'
                },
                {
                    email: 'raditsaditya2@gmail.com',
                    name: 'raditsaditya2',
                    user_id: ('raditsaditya2@gmail.com').trim().toLowerCase().substring(0, ('raditsaditya2@gmail.com').trim().toLowerCase().indexOf("@"))+'_uid_1234',
                },
                {
                    email: 'raditsan@gmail.com',
                    name: 'raditsan',
                    user_id: ('raditsan@gmail.com').trim().toLowerCase().substring(0, ('raditsan@gmail.com').trim().toLowerCase().indexOf("@"))+'_uid_1234',
                },
                {
                    email: 'dian@gmail.com',
                    name: 'dian',
                    user_id: ('dian@gmail.com').trim().toLowerCase().substring(0, ('dian@gmail.com').trim().toLowerCase().indexOf("@"))+'_uid_1234',
                },
                {
                    email: 'disa@gmail.com',
                    name: 'disa',
                    user_id: ('disa@gmail.com').trim().toLowerCase().substring(0, ('disa@gmail.com').trim().toLowerCase().indexOf("@"))+'_uid_1234',
                },
                {
                    email: 'inu@gmail.com',
                    name: 'inu',
                    user_id: ('inu@gmail.com').trim().toLowerCase().substring(0, ('inu@gmail.com').trim().toLowerCase().indexOf("@"))+'_uid_1234'
                }
            ]
        }
    }

    componentDidMount() {
        let user_list = [];
        this.state.user_list.filter(item => item.user_id !== this.state.user_id).forEach((data, i) => {
            user_list.push(data);
        });
        this.setState({
            filter_user: user_list
        });
    }

    FlatListItemSeparator = () => <View style={styles.line} />;

    renderItem = data => (
        <TouchableOpacity
            style={styles.list}
            onPress={() => this.props.navigation.navigate('TestingChat3_ChatConversation', {
                email: this.state.email,
                user_id: this.state.user_id,
                dataItem: data.item
            })}
        >
            <View
                //source={{ uri: data.item.thumbnailUrl }}
                style={{borderRadius: 40, backgroundColor: '#ef415a', width: 40, height: 40, margin: 6, alignItems: 'center', justifyContent: 'center' }}
            >
                <Text style={{color: '#fff', fontWeight: 'bold'}}>{data.item.name.charAt(0).toUpperCase()}</Text>
            </View>
            <View>
                <Text style={styles.lightText}>  {data.item.name}  </Text>
                <Text style={styles.lightText}>  {data.item.email}  </Text>
            </View>
        </TouchableOpacity>);
    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content>
                    <Text>Login As : {this.state.email}</Text>
                    <FlatList
                        data={this.state.filter_user}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.email}
                        extraData={this.state}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    list: {
        paddingVertical: 5,
        margin: 3,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        zIndex: -1
    },
    lightText: {
        color: "#000000",
        width: 200,
        paddingLeft: 15,
        fontSize: 12
    },
    line: {
        height: 0.5,
        width: "100%",
        backgroundColor:"rgba(255,255,255,0.5)"
    }
});
