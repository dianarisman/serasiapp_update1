import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    ListItem
} from 'native-base';
import MyToolbarTab from "../../../components/ToolbarTransparent/MyToolbarTab";
import {FlatList, Image, StyleSheet, TouchableOpacity, View} from "react-native";
import firebase from "./FirebaseConfigChat";
import MainStyle from "../../../styles/MainStyle";
export default class ChatList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: this.props.navigation.getParam('email'),
            user_id: this.props.navigation.getParam('user_id'),
            chatList: []
        };
        //alert(this.state.user_id)
    }

    componentDidMount() {
        firebase.database().ref('Chating_Room/Users/'+this.state.user_id+'/chat_list')
            .on('value', snapshot => {
                let chat_list = null;
                if(snapshot.val() !== null) {
                    chat_list = Object.entries(snapshot.val()).map(item => ({...item[1], key: item[0]}));
                } else {
                    chat_list = [];
                }

                this.setState({
                    chatList: chat_list
                })
            })
    }

    FlatListItemSeparator = () => <View style={styles.line} />;

    renderItem = data => (
        <TouchableOpacity
            key={data.item.user_id}
            style={styles.list}
            onPress={() => this.props.navigation.navigate('TestingChat3_ChatConversation', {
                email: this.state.email,
                user_id: this.state.user_id,
                dataItem: data.item
            })}
        >
            <View
                //source={{ uri: data.item.thumbnailUrl }}
                style={{borderRadius: 40, backgroundColor: '#ef415a', width: 40, height: 40, margin: 6, alignItems: 'center', justifyContent: 'center' }}
            >
                <Text style={{color: '#fff', fontWeight: 'bold'}}>{data.item.user_email.charAt(0).toUpperCase()}</Text>
            </View>
            <View>
                <Text style={styles.lightText}>  {data.item.user_email}  </Text>
                <Text style={styles.lightText}>  {data.item.user_id}  </Text>
            </View>
        </TouchableOpacity>);


    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content>
                    <Text>Login As : {this.state.email}</Text>
                    <Text>User ID : {this.state.user_id}</Text>
                    <Button style={{
                        marginVertical: 10
                    }} full success onPress={
                        ()=> this.props.navigation.navigate('TestingChat3_ChatFindFriends',
                            {
                                email: this.state.email,
                                user_id: this.state.user_id
                            })}>
                        <Text>Find Friends</Text>
                    </Button>
                    <FlatList
                        data={this.state.chatList}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.user_email}
                        extraData={this.state}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    list: {
        paddingVertical: 5,
        margin: 3,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        zIndex: -1
    },
    lightText: {
        color: "#000000",
        width: 200,
        paddingLeft: 15,
        fontSize: 12
    },
    line: {
        height: 0.5,
        width: "100%",
        backgroundColor:"rgba(255,255,255,0.5)"
    }
});
