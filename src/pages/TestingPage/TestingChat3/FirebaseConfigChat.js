import * as firebase from "firebase";
import {YellowBox} from "react-native";
import _ from "lodash";

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
    if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
    }
};
const config = {
    apiKey: "AIzaSyAC-Xi3zr4rOQ90THh6ZRyq2H0C2y3Pu9k",
    authDomain: "fir-ufo.firebaseapp.com",
    databaseURL: "https://fir-ufo.firebaseio.com",
    projectId: "fir-ufo",
    storageBucket: "fir-ufo.appspot.com",
    messagingSenderId: "437341759867"
};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
