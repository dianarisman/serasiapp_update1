import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text} from 'native-base';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import { FlatGrid } from 'react-native-super-grid';
import {View, StyleSheet, RefreshControl, TouchableOpacity, Image, Dimensions, ActivityIndicator} from 'react-native';
import styles from "../../styles/MainStyle";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import variables from "../../variables/MyGlobalVariable";
import StatusMaritalText from "../../components/StatusMaritaText/StatusMaritalText";
import ModalSelectCriteria from "../../components/ModalSelectCriteria/ModalSelectCriteria";

const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');

export default class TestingGridView3 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisiblePasangan: false,
            loadMoreIsVisible: false,
            selectCriteriaPasangan: [],
            listCriteriaPasangan: variables.criteria_list,
            memberList: [
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Fella',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Fera',
                    about: 'Fera Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Fera',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Fera',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Viera',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Viera',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Viera',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Michele',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Michele',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Michele',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Ayu',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Ayu',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Ayu',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Yuni',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Yuni',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Yuni',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Septi',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Septi',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Septi',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Tania',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Tania',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Tania',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Fella',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Fera',
                    about: 'Fera Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Fera',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Fera',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Viera',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Viera',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Viera',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Michele',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Michele',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Michele',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Ayu',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Ayu',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Ayu',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Yuni',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Yuni',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Yuni',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Janda',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Septi',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Septi',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Septi',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                },
                {
                    status_marital: 'Single',
                    images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
                    name: 'Tania',
                    about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
                    kriteriaDiri: [
                        'Tania',
                        'jujur',
                        'baik',
                        'hot',
                        'wik wik everyday',
                        'ah she up',
                        'nget tenget tenget tenget'
                    ],
                    kriteriaPasangan: [
                        'Tania',
                        'kuat',
                        'kokoh',
                        'tahan lama',
                        'semen tiga roda'
                    ],
                    height: 165,
                    weight: 58,
                    nationality: 'Indonesia',
                    city: 'Bogor',
                    age: 25,
                    in_relationship: true
                }
            ]
        }
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        GlobalDataRequest.tes().then((result)=>{
            this.setState({isLoading: false});
            alert(JSON.stringify(result));
        }, () => {
            alert('cant request to server');
        });
        //setInterval(this.setState({isLoading: false}), 3000);
    };


    _showModalKriteriaPasangan(visible) {
        this.setState({
            modalVisiblePasangan: visible
        });
    }
    _getSelectedCriteriaPasangan(dataList) {
        const data = [];
        const criteria = dataList;
        for(let i=0;i<criteria.length;i++){
            if (criteria[i].is_selected){
                data.push(criteria[i]);
            }
        }
        this.setState({
            selectCriteriaPasangan: data
        });

    }

    _selectCriteriaPasangan(indexSelect, dataList) {
        let criteria = [ ...dataList ];
        //let criteria = [ ...this.state.listCriteriaDiri ];
        if(criteria[indexSelect].is_selected){
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: false};
        }else{
            criteria[indexSelect] = {...criteria[indexSelect], is_selected: true};
        }
        this.setState({
            listCriteriaPasangan: criteria
        });
    }

    isCloseToBottom({layoutMeasurement, contentOffset, contentSize}){
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;
    }



    isCloseToTop({layoutMeasurement, contentOffset, contentSize}){
        return contentOffset.y === 0;
    }

    renderContent = () => (
        <View style={s.content}>
            {Array(50).fill(0).map((_, i) => (
                <View style={s.content__row} key={i}>
                    <View style={s.content__avatar}>
                        <View
                            style={{ width: 50, height: 50, backgroundColor: '#f64159', borderRadius: 10 }}
                        />
                    </View>

                    <Text style={s.content__name}>Garlic tastes best with ice water and lots of green curry.</Text>
                </View>
            ))}

            <View style={s.content__button}>
                <Button onPress={this.scrollToTop} name="Scroll to Top" />
            </View>
        </View>
    );

    _loadMoreData() {
        const temp = {
            status_marital: 'Janda',
            images: "https://cdn0-a.production.vidio.static6.com/uploads/video/image/302552/toracinocoolexpression_menyayi_ervanpasha-aldirachenasexy_medan-6e12e2.jpg",
            name: 'Michele',
            about: 'Ingin selalu dihangatkan, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.',
            kriteriaDiri: [
                'Michele',
                'jujur',
                'baik',
                'hot',
                'wik wik everyday',
                'ah she up',
                'nget tenget tenget tenget'
            ],
            kriteriaPasangan: [
                'Michele',
                'kuat',
                'kokoh',
                'tahan lama',
                'semen tiga roda'
            ],
            height: 165,
            weight: 58,
            nationality: 'Indonesia',
            city: 'Bogor',
            age: 25,
            in_relationship: true
        };
        this.setState({
            loadMoreIsVisible: true
        });
        GlobalDataRequest.tes().then((result)=>{
            this.setState({isLoading: false});
            //alert(JSON.stringify(result));

            this.setState(state => {
                const list = state.memberList.push(temp);
                return {
                    list,
                    value: '',
                };
            });

            this.setState({
                loadMoreIsVisible: false
            });
        }, () => {
            alert('cant request to server');
            this.setState({
                loadMoreIsVisible: false
            });
        });
    }

    render() {

        return (
            <Container>
                <MyToolbarTab
                    searchIconVisible={true}
                    onPressSearch={this._showModalKriteriaPasangan.bind(this)}
                />
                <ModalSelectCriteria
                    selectTitle={variables.kriteriaPasangan}
                    isModalVisible={this.state.modalVisiblePasangan}
                    onSelectItem={this._selectCriteriaPasangan.bind(this)}
                    onHideModal={this._showModalKriteriaPasangan.bind(this)}
                    onGetSelectedItem={this._getSelectedCriteriaPasangan.bind(this)}
                    listCriteria={this.state.listCriteriaPasangan}
                />
                <Content
                    scrollEventThrottle={100}
                    onMomentumScrollEnd={({nativeEvent})=>{
                        /*if(this.isCloseToTop(nativeEvent)){
                            //do something
                        }*/
                        if(this.isCloseToBottom(nativeEvent)){
                            this._loadMoreData();
                        }
                    }}
                    style={styles.otherStyle.containerMain}
                         refreshControl={
                             <RefreshControl
                                 style={{backgroundColor: '#E0FFFF'}}
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor='#f9f9f9'
                             />}
                >

                    <View style={{marginVertical: 0, backgroundColor: '#ffffff'}}>
                        <Text>{JSON.stringify(this.state.selectCriteriaPasangan)}</Text>
                        <FlatGrid
                            itemDimension={styles.otherStyle.gridDimension}
                            items={this.state.memberList}
                            style={styles.otherStyle.gridViewMember}
                            // staticDimension={300}
                            // fixed
                            // spacing={20}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity
                                    activeOpacity={ 0.8 }
                                    onPress={() => this.props.navigation.navigate('ProfileDetail', {dataDetail: item})}
                                    style={[styles.otherStyle.itemContainerMember, { backgroundColor: item.code }]}>
                                    <Image
                                        style={{flex:1,borderRadius: 5}}
                                        source={{uri: item.images}}
                                        resizeMode="cover"
                                    />
                                    <View style={{position: 'absolute', left: 5, bottom: 5}}>
                                        <StatusMaritalText
                                            statusMarital={item.status_marital}
                                        />
                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                        { !this.state.loadMoreIsVisible ? <ActivityIndicator color={styles.color.placeholderInput} size='large'/> : null }

                    </View>
                </Content>
            </Container>
        );
    }
}
