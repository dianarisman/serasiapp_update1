import React, { Component } from 'react';
import { Container, Content} from 'native-base';
import {View, Image, Dimensions, Text, TouchableHighlight,TouchableOpacity, Modal, Alert, StyleSheet, ScrollView} from "react-native";
import 'moment/min/locales';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import ImageZoom from 'react-native-image-pan-zoom';
import Lightbox from "react-native-lightbox";
const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');
const BASE_PADDING = 10;
import Carousel from 'react-native-looped-carousel';
import MainStyle from "../../styles/MainStyle";
export default class TestingImageZoom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            image_uri: 'https://www.samehadaku.tv/wp-content/uploads/2019/04/One-Punch-Man-Season-2-Episode-3-Subtitle-Indonesia.jpg',
            modalVisible: false,
        }
    }

    componentWillMount(){
        Image.getSize(this.state.image_uri, (width, height) => {
            this.setState({
                image_width: width,
                image_height: height,
                image_width_transform: SCREEN_WIDTH,
                image_height_transform: height * (SCREEN_WIDTH/width),
            })
        });
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        const renderCarousel = () => (
            <Carousel style={{ width: SCREEN_WIDTH, height: SCREEN_WIDTH }}>
                <Image
                    style={{ flex: 1 }}
                    resizeMode="contain"
                    source={{ uri: 'http://cdn.lolwot.com/wp-content/uploads/2015/07/20-pictures-of-animals-in-hats-to-brighten-up-your-day-1.jpg' }}
                />
                <View style={{ backgroundColor: '#6C7A89', flex: 1 }}/>
                <View style={{ backgroundColor: '#019875', flex: 1 }}/>
                <View style={{ backgroundColor: '#E67E22', flex: 1 }}/>
            </Carousel>
        );
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({
                            modalVisible: !this.state.modalVisible
                        })
                    }}>
                    <View style={{marginTop: 22}}>
                        <View>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                <Text>Hide Modal</Text>
                            </TouchableHighlight>
                            <ImageZoom
                                clickDistance={1}
                                doubleClickInterval={0}
                                ref={ref => (this.ImageZoom = ref)}
                                minScale={1}
                                cropWidth={SCREEN_WIDTH}
                                       cropHeight={SCREEN_HEIGHT-70}
                                       imageWidth={this.state.image_width_transform}
                                       imageHeight={this.state.image_height_transform}
                                       responderRelease={() => {
                                           this.ImageZoom.reset();
                                           this.ImageZoom.resetScale();
                                       }}>
                                <Image

                                    enableHorizontalBounce={true}
                                    style={{width:this.state.image_width_transform, height:this.state.image_height_transform}}
                                       source={{uri: this.state.image_uri}}/>
                            </ImageZoom>
                        </View>
                    </View>
                </Modal>


                <Content
                    style={MainStyle.otherStyle.containerMainWhite}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setModalVisible(true);
                        }}>
                        <Image

                            enableHorizontalBounce={true}
                            style={{width:this.state.image_width_transform, height:this.state.image_height_transform}}
                            source={{uri: this.state.image_uri}}/>
                    </TouchableOpacity>

                    <ScrollView style={styles.container}>
                        <View style={styles.text}><Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text></View>
                        <Lightbox underlayColor="white">
                            <Image
                                style={styles.contain}
                                resizeMode="contain"
                                source={{ uri: 'https://www.yayomg.com/wp-content/uploads/2014/04/yayomg-pig-wearing-party-hat.jpg' }}
                            />
                        </Lightbox>
                        <View style={styles.text}><Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text></View>
                        <Lightbox springConfig={{tension: 15, friction: 7}} swipeToDismiss={false} renderContent={renderCarousel}>
                            <Image
                                style={styles.carousel}
                                resizeMode="contain"
                                source={{ uri: 'http://cdn.lolwot.com/wp-content/uploads/2015/07/20-pictures-of-animals-in-hats-to-brighten-up-your-day-1.jpg' }}
                            />
                        </Lightbox>
                        <View style={styles.text}><Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text></View>
                        <Lightbox
                            springConfig={
                                {
                                    tension: 30, friction: 7
                                }
                            }
                            renderHeader={close => (
                                <TouchableOpacity onPress={close}>
                                    <Text style={styles.closeButton}>Close</Text>
                                </TouchableOpacity>
                            )}>
                            <View style={styles.customHeaderBox}>
                                <Text>I have a custom header</Text>
                            </View>
                        </Lightbox>
                        <View style={styles.text}><Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text></View>
                        <View style={styles.row}>
                            <Lightbox style={styles.col}>
                                <View style={[styles.square, styles.squareFirst]}><Text style={styles.squareText}>I'm a square</Text></View>
                            </Lightbox>
                            <Lightbox style={styles.col}>
                                <View style={[styles.square, styles.squareSecond]}><Text style={styles.squareText}>I'm a square</Text></View>
                            </Lightbox>
                        </View>
                        <View style={styles.text}><Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text></View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: BASE_PADDING,
    },
    closeButton: {
        color: 'white',
        borderWidth: 1,
        borderColor: 'white',
        padding: 8,
        borderRadius: 3,
        textAlign: 'center',
        margin: 10,
        alignSelf: 'flex-end',
    },
    customHeaderBox: {
        height: 150,
        backgroundColor: '#6C7A89',
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        marginLeft: -BASE_PADDING,
        marginRight: -BASE_PADDING,
    },
    col: {
        flex: 1,
    },
    square: {
        width: SCREEN_WIDTH / 2,
        height: SCREEN_WIDTH / 2,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    squareFirst: {
        backgroundColor: '#C0392B',
    },
    squareSecond: {
        backgroundColor: '#019875',
    },
    squareText: {
        textAlign: 'center',
        color: 'white',
    },
    carousel: {
        height: SCREEN_WIDTH - BASE_PADDING * 2,
        width: SCREEN_WIDTH - BASE_PADDING * 2,
        backgroundColor: 'white',
    },
    contain: {
        flex: 1,
        height: 150,
    },
    text: {
        marginVertical: BASE_PADDING * 2,
    },
});
