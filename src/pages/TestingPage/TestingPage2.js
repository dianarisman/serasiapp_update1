import React, { Component } from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import GlobalDataRequest from "../../services/GlobalDataRequest";

export default class TestingPage2 extends Component {
    constructor(props){
        super(props);
        this.state =
            {
                token: ""
            };
        this.getAuth();
    }

    getAuth() {
        GlobalDataRequest.authToken().then((resolve) => {
            this.setState({
                token: resolve
            }, function(){

            });
        });
    }
    logout() {
        GlobalDataRequest.authClear().then((resolve) => {
            if(resolve) {
                this.props.navigation.replace('Login');
            }
        })
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>Hello, world!</Text>
                <Text>{this.state.token}</Text>
                <TouchableOpacity style={styles.formLogin.button}>
                    <Text style={styles.formLogin.buttonText} onPress={() => this.logout() }>{variables.logout}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
