import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import CalendarStrip from 'react-native-calendar-strip';
import {Alert, Dimensions, FlatList, StyleSheet, View, TouchableOpacity} from 'react-native';
import moment from 'moment';
import styles from "../../styles/MainStyle";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";

const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');
const imageHeight = 100 * (SCREEN_WIDTH/4)/100;
const imageWidth = SCREEN_WIDTH/4;

export default class TestingPageDatePickerStrip extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectDate: null,
            selectTime: null,
            timeListItem: [
                { id: 1, key: "1", name: '08:00 AM', is_selected: false, is_disabled: false },
                { id: 2, key: "2", name: '09:00 AM', is_selected: false, is_disabled: false },
                { id: 3, key: "3", name: '10:00 AM', is_selected: false, is_disabled: true },
                { id: 4, key: "4", name: '11:00 AM', is_selected: false, is_disabled: false },
                { id: 5, key: "5", name: '12:00 PM', is_selected: false, is_disabled: true },
                { id: 6, key: "6", name: '01:00 PM', is_selected: false, is_disabled: false },
                { id: 7, key: "7", name: '02:00 PM', is_selected: false, is_disabled: true },
                { id: 8, key: "7", name: '03:00 PM', is_selected: false, is_disabled: true },
                { id: 9, key: "8", name: '04:00 PM', is_selected: false, is_disabled: true },
                { id: 10, key: "9", name: '05:00 PM', is_selected: false, is_disabled: false },
                { id: 11, key: "10", name: '06:00 PM', is_selected: false, is_disabled: false }
            ]
        };
    }

    _selectTime(dataItem) {
        const { timeListItem } = this.state;
        timeListItem.forEach((elem) => {
            elem.is_selected = false;
            if (elem.id === dataItem.id) {
                elem.is_selected = true
            }
        });
        this.setState({
            timeListItem,
            selectTime: dataItem.name
        });
    }

    _selectDate(event) {
        this.setState({
            selectDate: moment(event).format('YYYYMMDD')
        });
    }

    render() {
        let datesWhitelist = [{
            start: moment(),
            end: moment().add(7, 'days')  // total 4 days enabled
        }];
        let datesBlacklist = [
            moment().add((6-moment().weekday()), 'days'), //disabled sabtu & minggu
            moment().add((7-moment().weekday()), 'days') //disabled sabtu & minggu
        ];
        let date = [{
            date1: moment(20190401, 'YYYYMMDD').add(1, 'hour').format('hh:mm:ss A'),
            date2: moment().add(
                (7-moment().weekday()), 'days')  // total 4 days enabled
        }];

        const locale = {
            name: 'id',
            config: []
        };


        return (
            <Container>
                <MyToolbarTab/>
                <Content>
                    <View>
                        <CalendarStrip
                            calendarAnimation={{type: 'sequence', duration: 30}}
                            daySelectionAnimation={{type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'white'}}
                            style={{height: 100, paddingTop: 20, paddingBottom: 10, textAlign: 'left'}}
                            calendarHeaderStyle={{color: 'white', textAlign: 'right',alignSelf: 'flex-start'}}
                            calendarColor={'#7743CE'}
                            dateNumberStyle={{color: 'white'}}
                            dateNameStyle={{color: 'white'}}
                            highlightDateNumberStyle={{color: 'yellow'}}
                            highlightDateNameStyle={{color: 'yellow'}}
                            disabledDateNameStyle={{color: 'grey'}}
                            disabledDateNumberStyle={{color: 'grey'}}
                            datesWhitelist={datesWhitelist}
                            datesBlacklist={datesBlacklist}
                            iconContainer={{flex: 0.1}}
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        <CalendarStrip
                            updateWeek={false}
                            onDateSelected={(ev)=> this._selectDate(ev)}
                            locale={locale}
                            datesWhitelist={datesWhitelist}
                            datesBlacklist={datesBlacklist}
                            highlightDateNameStyle={{color: '#0b00ef'}}
                            daySelectionAnimation={{type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#0b00ef'}}
                            highlightDateNumberStyle={{color: '#0b00ef'}}
                            iconContainer={{flex: 0.1}}
                            style={{height:150, paddingTop: 20, paddingBottom: 10}}
                        />

                    </View>
                    <View>
                        <Text>
                            selectDate {this.state.selectDate} selectTime {this.state.selectTime}
                        </Text>
                    </View>
                    <View style={styless.container}>
                        <FlatList
                            data={ this.state.timeListItem }
                            renderItem={ ({item, index}) =>
                                <TouchableOpacity
                                    disabled={item.is_disabled}
                                    key={index} style={[
                                    styless.GridViewContainer,
                                    item.is_selected ? styless.itemSelected : null,
                                    item.is_disabled ? styless.itemDisabled : null,
                                ]}
                                                  onPress={()=> this._selectTime(item)}>
                                    <Text style={styless.GridViewTextLayout} > {item.name} </Text>
                                </TouchableOpacity> }
                            keyExtractor={item => item.key}
                            extraData={this.state}
                            numColumns={4}
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

const styless = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#e5e5e5"
    },
    GridViewContainer: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        margin: 5
    },
    itemSelected: {
        backgroundColor: '#7B1FA2'
    },
    itemDisabled: {
        backgroundColor: '#6e6e6e'
    },
    GridViewTextLayout: {
        fontSize: 12,
        fontWeight: 'bold',
        justifyContent: 'center',
        color: '#fff',
    }
});
