import React, { Component } from 'react';
import Popover from 'react-native-popover-view';
import {
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    ScrollView,
    Switch,
    TouchableOpacity, Picker
} from 'react-native';
import variables from "../../variables/MyGlobalVariable";

let shouldShowInPopover = true;

class Settings extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={{padding: 20, minWidth: 400}}>
                <Text style={{fontSize: 16, fontWeight: 'bold', marginBottom: 10}}>Popover Navigation</Text>
                <View style={styles.row}>
                    <View style={{flex: 1}}>
                        <Text>Show Stack Views in Popover</Text>
                        <Text style={{fontSize: 10, color: 'gray', marginTop: 5}}>You will need to change the device orientation for this setting to take effect.</Text>
                    </View>
                    <Switch value={shouldShowInPopover} onValueChange={() => {
                        shouldShowInPopover = !shouldShowInPopover;
                        this.forceUpdate();
                    }} />
                </View>
            </ScrollView>
        )
    }
}

class CustomComponent extends Component {
    render() {
        let renderServiceList = this.props.serviceList.map( (data, index) => {
            return (
                <TouchableOpacity
                    style={{marginVertical: 6}}
                    activeOpacity={0.7}
                    key={index}
                    onPress={()=> this.props.onSelectService(data)}
                >
                    <Text>- {data.name} | {data.price}</Text>
                </TouchableOpacity>
            );
        });

        return (
            <ScrollView contentContainerStyle={{padding: 20, minWidth: 400}}>
                <Text style={{fontSize: 16, fontWeight: 'bold', marginBottom: 10}}>Pilih Layanan</Text>
                <View style={styles.row}>
                    <View style={{flex: 1}}>
                        {renderServiceList}
                    </View>
                </View>
            </ScrollView>
        )
    }
}

export default class TestingPopover extends Component {
    state = {
        isVisible: false,
        isVisible2: false,
        isVisible3: false,
        selectService: variables.consultation_service_list[0].name,
        selectServiceData: variables.consultation_service_list[0],
        consultationServiceList: variables.consultation_service_list,
    };

    showPopover() {
        this.setState({isVisible: true});
    }

    closePopover() {
        this.setState({isVisible: false});
    }

    _selectService(data) {
        const index = this.state.consultationServiceList.findIndex(
            item => data.id === item.id
        );
        this.setState({
            selectServiceData: this.state.consultationServiceList[index],
            selectService: data.name,
            isVisible3: false
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text>Select service {this.state.selectService}</Text>
                    <Text>selectServiceData {JSON.stringify(this.state.selectServiceData)}</Text>
                </View>
                <View>
                    <TouchableHighlight ref={ref => this.touchable = ref} style={styles.button} onPress={() => this.showPopover()}>
                        <Text>Custom Component</Text>
                    </TouchableHighlight>

                    <Popover
                        placement='bottom'
                        isVisible={this.state.isVisible}
                        fromView={this.touchable}
                        onClose={() => this.closePopover()}>
                        <Settings/>
                    </Popover>
                </View>
                <View>
                    <TouchableHighlight ref={ref => this.touchable3 = ref} style={styles.button}
                                        onPress={() => this.setState({isVisible3: true})}>
                        <Text>Custom Component2</Text>
                    </TouchableHighlight>

                    <Popover
                        placement='bottom'
                        isVisible={this.state.isVisible3}
                        fromView={this.touchable3}
                        onClose={() => this.setState({isVisible3: false})}>
                        <CustomComponent
                            serviceList={this.state.consultationServiceList}
                            onSelectService={this._selectService.bind(this)}
                        />
                    </Popover>
                </View>
                <View>
                    <TouchableHighlight ref={ref => this.touchable2 = ref} style={styles.button}
                                        onPress={() => this.setState({isVisible2: true})}>
                        <Text>Default</Text>
                    </TouchableHighlight>

                    <Popover
                        isVisible={this.state.isVisible2}
                        fromView={this.touchable2}
                        onClose={() => this.setState({isVisible2: false})}>
                        <Text>I'm the content of this popover!</Text>
                    </Popover>
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 15
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(43, 186, 180)',
    },
    button: {
        borderRadius: 4,
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#ccc',
        borderColor: '#333',
        borderWidth: 1,
    }
});
