import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import styles from "../../styles/MainStyle";
import {Dimensions, Image, RefreshControl, ScrollView, TouchableOpacity, View} from "react-native";
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import variables from "../../variables/MyGlobalVariable";

const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');

export default class TestingPricing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pricing_list: [
                {
                    id: 1,
                    title: 'Free',
                    images: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvsOzvglK0iE_qXx05uvGx-ARb67AVm39ynQCbik5Lz_Qfc9al',
                    benefit: ['1 User', 'Basic Support', 'All Core Features'],
                    price: 'Rp. 0',
                    color: '#4f9deb'
                },
                {
                    id: 2,
                    title: 'Regular',
                    images: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRLBymU36V9n0_zJO0Acg_-06FQFvDIpX5_5jEVH1kMhuN9WAw',
                    benefit: ['2 User', 'Basic Support', 'All Core Features'],
                    price: 'Rp. 300',
                    color: '#4f9deb'

                },
                {
                    id: 3,
                    title: 'Executive',
                    images: 'https://secure.img2-fg.wfcdn.com/im/23494134/compr-r85/6840/68402466/default_name.png',
                    benefit: ['3 User', 'Basic Support', 'All Core Features'],
                    price: 'Rp. 300',
                    color: '#4f9deb'
                },
                {
                    id: 4,
                    title: 'VIP',
                    images: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhNd62QLQPTgEwLXIeYAq9fxgUaz2d_CJMu9BEspD8lToUIdkO',
                    benefit: ['4 User', 'Basic Support', 'All Core Features'],
                    price: 'Rp. 400',
                    color: '#eb37e1'
                }
            ]
        }
    }


    render() {
        let imageArraySlides2 = [], marginLeft = 0, marginRight = 0;
        this.state.pricing_list.forEach((data, i) => {
            if(i<=0){
                marginLeft = 0;
                marginRight = 5;
            }else if (i===(this.state.pricing_list.length - 1)) {
                marginLeft = 5;
                marginRight = 0;
            }else{
                marginLeft = 5;
                marginRight = 5;
            }

            let benefit = data.benefit.map((item, key) =>
                <Text
                    key={`benefit${key}`}
                    style={{
                        color: '#525252',
                        fontSize: 17
                    }}>{item}</Text>
            );

            let renderSolidButton = (
                <TouchableOpacity
                    onPress={() => alert(JSON.stringify(data))}
                    style={{
                        marginVertical: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '80%',
                        backgroundColor: data.color,
                        borderRadius: 3
                    }}
                >
                    <Text
                        style={{
                            color: '#fff',
                            padding: 10,
                            fontWeight: 'bold'
                        }}
                    >Pilih Program</Text>
                </TouchableOpacity>
            );

            let renderBorderButton = (
                <TouchableOpacity
                    onPress={() => alert(JSON.stringify(data))}
                    style={{
                        marginVertical: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '80%',
                        backgroundColor: '#fff',
                        borderRadius: 3,
                        borderWidth: 2,
                        borderColor: data.color
                    }}
                >
                    <Text
                        style={{
                            color: data.color,
                            padding: 10,
                            fontWeight: 'bold'
                        }}
                    >Pilih Program</Text>
                </TouchableOpacity>
            );

            const thisImage = (
                <View key={`image${i}`}>
                    <View
                        style={{
                            width: SCREEN_WIDTH * 0.7,
                            height: SCREEN_WIDTH * 0.8,
                            marginLeft: marginLeft,
                            marginRight: marginRight,
                            borderRadius: 5,
                            borderWidth: 2,
                            borderColor: '#dddfe1',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text style={{
                            color: data.color,
                            fontSize: 32,
                            fontWeight: 'bold',
                            marginVertical: 3
                        }}>{data.title}</Text>
                        <View style={{flexDirection: 'row',
                            marginVertical: 3}}>
                            <Text
                                style={{
                                    color: '#000',
                                    fontSize: 32,
                                    fontWeight: 'bold'
                                }}>{data.price}</Text>
                            <Text style={{fontSize: 16}}>
                                .000
                            </Text>
                        </View>
                        {benefit}
                        {data.id !==4 ? renderBorderButton : renderSolidButton}
                    </View>

                </View>

            );
            imageArraySlides2.push(thisImage);
        });
        return (
            <Container>

                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                <Content
                    style={styles.otherStyle.containerMain}
                >
                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                            >

                                {imageArraySlides2}

                            </ScrollView>
                        </View>

                    </View>


                </Content>
            </Container>
        );
    }
}


