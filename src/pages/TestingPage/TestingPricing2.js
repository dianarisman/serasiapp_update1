import React, { Component } from 'react';
import { Container, Content, Text, CardItem, Body, Card, Accordion, Icon} from 'native-base';
import styles from "../../styles/MainStyle";
import {RefreshControl, View, Dimensions, TouchableOpacity, Image} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import moment from 'moment';
import 'moment/min/locales';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import TryAgainButtonFetch from "../../components/TryAgainButtonFetch/TryAgainButtonFetch";
import variables from "../../variables/MyGlobalVariable";
const dataArray = [
    { title: "Benefit", content: "Lorem ipsum dolor sit amet" }
];
const {height: SCREEN_HEIGHT, width: SCREEN_WIDTH} = Dimensions.get('window')
export default class TestingPricing2 extends Component {

    constructor(props) {
        super(props);
        moment.locale('id');
        this.state = {
            isLoading: false,
            isFetchFailed: false,
            user_token: this.props.navigation.getParam('user_token'),
            loadMoreIsVisible: false,
            pricingHistoryList: []
        }
    }

    _onContentRefresh = () => {
        this.getPricingHistory(this.state.user_token)
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    componentDidMount(): void {
        this.getPricingHistory(this.state.user_token)
    }

    getPricingHistory(token) {
        this.setState({isLoading: true, isFetchFailed: false});
        GlobalDataRequest.tes().then((result) => {
            this.setState({
                isLoading: false,
                //pricingHistoryList: result.data
                //my_profile: result

            });
            alert(JSON.stringify(result));
        }).catch(err => {
            alert('error 1 a '+err.message);
            this.setState({isLoading: false, isFetchFailed: true});
        })
    }

    _renderHeader(item, expanded) {
        return (
            <View style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center" ,
                backgroundColor: "#6998ef" }}>
                <Text style={{ fontWeight: "600", color: '#fff' }}>
                    {" "}{item.title}
                </Text>
                {expanded
                    ? <Icon style={{ fontSize: 18 }} type='AntDesign' name="up" />
                    : <Icon style={{ fontSize: 18 }} type='AntDesign' name="down" />}
            </View>
        );
    }
    _renderContent(item) {
        return (
            <Text
                style={{
                    backgroundColor: "#eaf2ff",
                    padding: 10,
                    fontStyle: "italic",
                }}
            >
                {item.content}
            </Text>
        );
    }

    render() {
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                {this.state.isFetchFailed ? (<TryAgainButtonFetch onPress={this.getNews.bind(this)}/>) : null}
                <Content
                    style={styles.otherStyle.containerMain}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={styles.color.serasiColorButton2}
                            title="Loading..."
                            titleColor="#00ff00"
                            colors={[styles.color.serasiColorButton2]}
                            progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                        />}
                >
                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <Card>
                            <CardItem cardBody>
                                <View style={{
                                    backgroundColor: styles.color.serasiColorButton,
                                    height: 150,
                                    width: null,
                                    flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <View>
                                        <Text
                                            style={{
                                                color: '#fff',
                                                fontSize: 20
                                            }}
                                        >Rp. 1.000.000</Text>
                                        <Text
                                            style={{
                                                color: '#fff',
                                                fontSize: 20
                                            }}
                                        >Per Bulan</Text>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem>
                                <Body >
                                <Text
                                    style={{
                                        color: styles.color.serasiColorButton,
                                        fontSize: 20,
                                        fontWeight: 'bold'
                                    }}
                                >Reguler </Text>
                                <Text>- bisa mengajukan perkenalan 3 kali</Text>
                                <Text>- dapat layanan konsultasi gratis 1x</Text>
                                <Text>- masa kontrak 6 bulan</Text>
                                <Text>- dapat memilih member premium dan pasif</Text>
                                {/*<Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    animation={true}
                                    expanded={true}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent}
                                />*/}
                                <TouchableOpacity
                                    style={{
                                        backgroundColor: styles.color.serasiColorButton,
                                        borderRadius: 5,
                                        marginVertical: 10,
                                        paddingVertical: 13,
                                        width: '100%', flexDirection: 'row', justifyContent: 'center'
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: '#fff'
                                        }}>{variables.choose}</Text>
                                </TouchableOpacity>
                                </Body>
                            </CardItem>
                        </Card>
                        <View
                            style={{
                                alignItems: 'center',
                                backgroundColor: '#F3DCF8',
                                borderRadius: 13,
                                padding: 20
                            }}
                        >
                            <Image
                                style={{
                                    height: 150,
                                    width: 150,
                                }}
                                source={{uri: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'}}/>
                                <Text
                                    style={{
                                        fontSize: 25,
                                        fontWeight: 'bold',
                                        textTransform: 'uppercase'
                                    }}>VIP</Text>
                            <View
                                style={{
                                    flexDirection: 'row'
                                }}>
                                <Text
                                    style={{
                                        fontSize: 25,
                                        fontWeight: 'bold',
                                        textTransform: 'uppercase'
                                    }}>Rp. 4.000</Text>
                                <View
                                    style={{
                                        flexDirection: 'row'
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 12,
                                            fontWeight: 'bold',
                                            textTransform: 'uppercase'
                                        }}>.000</Text>
                                    <Text
                                        style={{
                                            fontSize: 12,
                                        }}> / 12 Bulan</Text>
                                </View>
                            </View>
                            <Text>a</Text>
                            <Text>a</Text>
                            <Text>a</Text>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}
