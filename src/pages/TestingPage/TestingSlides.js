import React, { Component } from 'react'
import { Animated, View, StyleSheet, Image, Dimensions, ScrollView, Text, TouchableOpacity, FlatList } from 'react-native'
import {Container, Content} from 'native-base';
const { height, width }= Dimensions.get('window');
const FIXED_BAR_WIDTH = 500;
const BAR_SPACE = 10;
import Carousel from 'react-native-looped-carousel';

const images = [
    'https://s-media-cache-ak0.pinimg.com/originals/ee/51/39/ee5139157407967591081ee04723259a.png',
    'https://s-media-cache-ak0.pinimg.com/originals/40/4f/83/404f83e93175630e77bc29b3fe727cbe.jpg',
    'https://s-media-cache-ak0.pinimg.com/originals/8d/1a/da/8d1adab145a2d606c85e339873b9bb0e.jpg',
    'https://s-media-cache-ak0.pinimg.com/originals/ee/51/39/ee5139157407967591081ee04723259a.png',
    'https://s-media-cache-ak0.pinimg.com/originals/40/4f/83/404f83e93175630e77bc29b3fe727cbe.jpg',
    'https://s-media-cache-ak0.pinimg.com/originals/8d/1a/da/8d1adab145a2d606c85e339873b9bb0e.jpg',
    'https://s-media-cache-ak0.pinimg.com/originals/ee/51/39/ee5139157407967591081ee04723259a.png',
    'https://s-media-cache-ak0.pinimg.com/originals/40/4f/83/404f83e93175630e77bc29b3fe727cbe.jpg',
];

export default class TestingSlides extends Component {

    constructor(props) {
        super(props);
        this.state = {
            size: { width, height },
            datas:[
                {key: 0, text: "Hello"},
                {key: 1, text: "World1"},
                {key: 2, text: "World2"},
                {key: 3, text: "World3"},
                ]
        };
    }

    numItems = images.length;
    //itemWidth = (FIXED_BAR_WIDTH / this.numItems) - ((this.numItems - 1) * BAR_SPACE);
    itemWidth = 20;
    animVal = new Animated.Value(0);

    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ size: { width: layout.width, height: layout.height } });
    };

    render() {
        let imageArray = [];
        let imageArraySlides2 = [];
        let barArray = [];
        images.forEach((image, i) => {
            console.log(image, i);
            const thisImage = (
                <Image
                    key={`image${i}`}
                    source={{uri: image}}
                    style={{ width: width, height: height * 0.3 }}
                />
            );
            imageArray.push(thisImage);

            const scrollBarVal = this.animVal.interpolate({
                inputRange: [width * (i - 1), width * (i + 1)],
                outputRange: [-this.itemWidth, this.itemWidth],
                extrapolate: 'clamp',
            });

            const thisBar = (
                <View
                    key={`bar${i}`}
                    style={[
                        styles.track,
                        {
                            width: this.itemWidth,
                            marginLeft: i === 0 ? 0 : BAR_SPACE,
                        },
                    ]}
                >
                    <Animated.View

                        style={[
                            styles.bar,
                            {
                                width: this.itemWidth,
                                transform: [
                                    { translateX: scrollBarVal },
                                ],
                            },
                        ]}
                    />
                </View>
            );
            barArray.push(thisBar)
        });

        //Slides 2
        images.forEach((image, i) => {
            console.log(image, i);
            const thisImage = (
                <Image
                    key={`image${i}`}
                    source={{uri: image}}
                    style={{ width: width * 0.8, height: height * 0.23, marginHorizontal: 10 }}
                />
            );
            imageArraySlides2.push(thisImage);
        });

        return (
            <Container>
                <Content>
                    <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
                        <Carousel
                            delay={2000}
                            style={{height: 200, width: 500}}
                            autoplay={false}
                            bullets={true}
                            onAnimateNextPage={(p) => console.log(p)}
                        >
                            <View style={{ backgroundColor: '#BADA55', height: 200, width: 500 }}><Text>1</Text></View>
                            <View style={{ backgroundColor: '#f44500', height: 200, width: 500 }}><Text>2</Text></View>
                            <View style={{ backgroundColor: '#eb37e1', height: 200, width: 500 }}><Text>3</Text></View>
                        </Carousel>
                    </View>

                    <View style={{marginTop: 30}}>
                        <Text>Slides pricing</Text>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                                pagingEnabled
                                onScroll={
                                    Animated.event(
                                        [{ nativeEvent: { contentOffset: { x: this.animVal } } }]
                                    )
                                }
                            >

                                {imageArraySlides2}

                            </ScrollView>
                            <View
                                style={styles.barContainer}
                            >
                                {barArray}
                            </View>
                        </View>
                    </View>

                    <View style={{marginTop: 30}}>
                        <Text>Slides pricing2</Text>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <FlatList
                                // Do something when animation ended
                                //onMomentumScrollEnd={(e) => this.onScrollEnd(e)}
                                ref="flatlist"
                                showsHorizontalScrollIndicator={false}
                                data={this.state.datas}
                                keyExtractor={(item) => item.key.toString()}
                                getItemLayout={(data, index) => (
                                    // Max 5 items visibles at once
                                    {length: Dimensions.get('window').width / 5, offset: Dimensions.get('window').width / 5 * index, index}
                                )}
                                horizontal={true}
                                // Here is the magic : snap to the center of an item
                                snapToAlignment='center'
                                // Defines here the interval between to item (basically the width of an item with margins)
                                snapToInterval={Dimensions.get('window').width / 2}
                                renderItem={ ({item}) =>
                                    <TouchableOpacity
                                        //onPress={() => this.scrollToIndex(/* scroll to that item */)}
                                        style={{
                                            width: 300,
                                            height: 200,
                                            backgroundColor: '#6998ef'
                                        }}>
                                        <Text>{item.text}</Text>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                    </View>

                    <View style={{marginTop: 30}}>
                        <Text>Slides 1</Text>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                                pagingEnabled
                                onScroll={
                                    Animated.event(
                                        [{ nativeEvent: { contentOffset: { x: this.animVal } } }]
                                    )
                                }
                            >

                                {imageArray}

                            </ScrollView>
                            <View
                                style={styles.barContainer}
                            >
                                {barArray}
                            </View>
                        </View>
                    </View>

                    <View style={{marginTop: 30}}>
                        <Text>Slides 2</Text>
                        <View
                            style={styles.container}
                            //flex={1}
                        >
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                scrollEventThrottle={10}
                            >

                                {imageArraySlides2}

                            </ScrollView>
                        </View>
                    </View>
                </Content>
            </Container>


        )
    }
}


const styles = StyleSheet.create({
    container: {
        //backgroundColor: '#000000',
        //flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.3
    },
    barContainer: {
        position: 'absolute',
        zIndex: 2,
        bottom: 10,
        flexDirection: 'row',
    },
    track: {
        backgroundColor: '#ccc',
        overflow: 'hidden',
        height: 2,
    },
    bar: {
        backgroundColor: '#5294d6',
        height: 2,
        position: 'absolute',
        left: 0,
        top: 0,
    },
});
