import React, { Component } from 'react';
import { Container, Footer, Tab, Tabs, TabHeading, Icon, Text, FooterTab, Badge, Button} from 'native-base';
import Tab1 from '../HomePageTab/Tab1/Tab1';
import Tab2 from '../HomePageTab/Tab2/Tab2';
import Tab3 from '../HomePageTab/Tab3/Tab3';
import Tab4 from '../HomePageTab/Tab4/Tab4';
import Tab5 from '../HomePageTab/Tab5/Tab5';
import styles from "../../styles/MainStyle";
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import TabBar from "../../components/TabBar/TabBar";
class TestingTab extends Component {
    render() {
        return (
            <Container>
                <Footer>
                    <FooterTab>
                        <Button badge vertical>
                            <Badge><Text>2</Text></Badge>
                            <Icon name="apps" />
                            <Text>Apps</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="camera" />
                            <Text>Camera</Text>
                        </Button>
                        <Button active badge vertical>
                            <Badge ><Text>51</Text></Badge>
                            <Icon active name="navigate" />
                            <Text>Navigate</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="person" />
                            <Text>Contact</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

export default createAppContainer(createBottomTabNavigator({
        Tab1: { screen: Tab1 },
        Tab2: { screen: Tab2 },
        Tab3: { screen: Tab3 },
        Tab4: { screen: Tab4 },
        Tab5: { screen: Tab5 }
    },
    {
        tabBarPosition: 'bottom',
        showIcon: false,
        swipeEnabled: true,
        tabBarComponent: props => {
            return (
                <TabBar {...props}/>
            );
        }
    }
));
