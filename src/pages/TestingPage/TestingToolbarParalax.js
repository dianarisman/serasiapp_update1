import React, { Component } from 'react';
import {
    ActivityIndicator,
    Animated,
    Dimensions,
    Platform,
    ScrollView,
    StyleSheet,
    Text, TouchableOpacity,
    View
} from 'react-native';
import MyToolbar from "../../components/ToolbarTransparent/MyToolbarTransparent";
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import {Icon} from "native-base"
import StatusMaritalDetailProfile from "../../components/StatusMaritaText/StatusMaritalDetailProfile";


const HEADER_EXPANDED_HEIGHT = 300;
const HEADER_COLLAPSED_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT;
const str = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel ultrices ante. Duis vulputate lorem non tortor pharetra, aliquet aliquet leo efficitur. Ut sed rutrum nisi. Pellentesque facilisis erat sit amet mi ornare, et dapibus tortor congue. Integer vulputate magna a vehicula accumsan. Cras nec nunc consequat, volutpat felis vitae, pulvinar nibh. Vestibulum lacinia in tortor vel maximus. Suspendisse semper dolor ligula. Praesent pellentesque suscipit enim, at dictum nisl pellentesque non. Phasellus nec consectetur magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed condimentum porttitor elit ut dignissim. Nunc nec libero a orci porttitor accumsan eget sed diam. Cras dignissim, nulla sed laoreet accumsan, mi quam egestas mauris, id posuere purus lorem sagittis purus. Duis sollicitudin neque ac aliquet sollicitudin.
In eros est, sollicitudin sit amet risus eget, porttitor pulvinar ipsum. Nulla eget quam arcu. Mauris vel odio cursus, hendrerit augue et, ultricies massa. Phasellus pharetra et libero id semper. Sed sollicitudin commodo mi, nec efficitur sem congue vitae. Ut pellentesque augue ut lacus finibus sollicitudin. Donec a auctor augue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam vitae convallis nulla. Maecenas venenatis lorem at mi commodo pharetra. Mauris finibus hendrerit magna, sit amet ultrices turpis aliquet nec. Proin et diam suscipit, sollicitudin risus ac, porta nibh.
Aliquam pretium, elit maximus vehicula lobortis, neque dolor tempor nisl, sit amet interdum erat turpis eu metus. Sed semper libero ac diam finibus, ac interdum orci placerat. Donec nec erat ac erat rhoncus blandit. Nunc felis dui, semper eu porttitor in, porttitor vitae eros. In vel mattis est, vel molestie dui. Nulla semper nisl tempor scelerisque egestas. Duis faucibus, elit id accumsan aliquet, turpis felis scelerisque felis, quis tincidunt felis massa nec eros. Vivamus pellentesque congue velit finibus porttitor. Pellentesque eu mi lacinia sapien fermentum tincidunt sit amet eu nisl. Suspendisse pharetra ex in magna molestie venenatis.
Suspendisse non gravida tortor. Donec tristique ipsum eget arcu aliquet molestie. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam cursus purus eget accumsan maximus. Duis eu iaculis arcu. Donec iaculis, sem vel condimentum maximus, lectus nisl pellentesque dolor, non ullamcorper sapien lectus sed enim. Aenean et leo nisi. Nulla viverra magna id luctus fermentum. Donec et mauris placerat, mollis elit lacinia, cursus lacus. Donec aliquet libero arcu, non consectetur elit maximus sit amet. Quisque lacinia, libero et fermentum rutrum, lorem arcu tincidunt ante, sed iaculis velit tortor non lacus.
Sed accumsan lectus laoreet mollis cursus. Phasellus sagittis vulputate erat, non tempus dui pellentesque vel. Fusce imperdiet nulla vitae mauris facilisis bibendum. Fusce vestibulum fringilla orci, sit amet euismod nunc eleifend id. Curabitur mattis dolor at odio maximus lacinia. Vivamus ornare lorem sed augue faucibus, vel volutpat lacus elementum. Suspendisse potenti.`;


const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get("screen")

export default class TestingToolbarParalax extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(0)
        };

        alert(JSON.stringify(this.props.navigation.getParam('dataDetail')));
        this.dataDetail = this.props.navigation.getParam('dataDetail');
    }

    renderScrollViewContent() {

        let kriteriaDiri_list = this.dataDetail.kriteriaDiri.map( (data, index) => {
            return <Text key={index} style={styles.otherStyle.textBubble}>{data}</Text>
        });

        let kriteriaPasangan_list = this.dataDetail.kriteriaPasangan.map( (data, index) => {
            return <Text key={index} style={styles.otherStyle.textBubble}>{data}</Text>
        });
        return (
            <View>
                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.about}</Text>
                    <Text>{this.dataDetail.about}</Text>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.biodata}</Text>
                    <View style={{
                        flex: 1
                    }}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.height}</Text><Text> : {this.dataDetail.height}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.weight}</Text><Text> : {this.dataDetail.weight}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.status}</Text><Text> : {this.dataDetail.status_marital}</Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{width: (SCREEN_WIDTH/2)-10}}>{variables.nationality}</Text><Text> : {this.dataDetail.nationality}</Text>
                        </View>
                    </View>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.kriteriaDiri}</Text>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {kriteriaDiri_list}
                    </View>
                </View>

                <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                    <Text style={styles.otherStyle.infoDetailProfile}>{variables.kriteriaPasangan}</Text>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {kriteriaPasangan_list}
                    </View>
                </View>

                <TouchableOpacity activeOpacity={ 0.8 } style={{...styles.formLogin.button(styles.color.serasiColorButton3), margin: 5}} onPress={() => {}} >
                    {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.interested}</Text> }
                </TouchableOpacity>
            </View>

        );
    }

    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_EXPANDED_HEIGHT-HEADER_COLLAPSED_HEIGHT],
            outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
            extrapolate: 'clamp'
        });
        const headerTitleOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_EXPANDED_HEIGHT-HEADER_COLLAPSED_HEIGHT],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        });
        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const heroTitleOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_EXPANDED_HEIGHT-HEADER_COLLAPSED_HEIGHT],
            outputRange: [1, 0],
            extrapolate: 'clamp'
        });

        const headerTitle = 'HEADER';

        return (
            <View style={styles.otherStyle.containerMain}>
                <Animated.View style={[styless.header, { height: headerHeight }]}>
                    <Animated.Image
                        style={[
                            styless.backgroundImage,
                            {
                                opacity: imageOpacity,
                                transform: [{ translateY: headerHeight }],
                            },
                        ]}
                        source={{uri: this.dataDetail.images}}
                    />
                    <Animated.View style={{textAlign: 'center', fontSize: 18, color: 'black', opacity: headerTitleOpacity, marginTop: 20}}>
                        <MyToolbar title={this.dataDetail.name} statusbarType='translucent' navigation={this.props.navigation}/>
                    </Animated.View>
                    <Animated.View style={{opacity: heroTitleOpacity, position: 'absolute', top: 35, left: 20}}>
                        <Icon type='Ionicons' name='ios-arrow-back' style={{color: '#fff'}} />
                    </Animated.View>
                    <Animated.View style={{position: 'absolute', bottom: 16, left: 16, opacity: heroTitleOpacity}}>

                        <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold'}}>{this.dataDetail.name}, {this.dataDetail.age}</Text>
                        <StatusMaritalDetailProfile
                            statusMarital={this.dataDetail.status_marital}
                            city={this.dataDetail.city}
                        />
                    </Animated.View>
                </Animated.View>
                <ScrollView
                    contentContainerStyle={styless.scrollContainer}
                    onScroll={Animated.event(
                        [{ nativeEvent: {
                                contentOffset: {
                                    y: this.state.scrollY
                                }
                            }
                        }])
                    }
                    scrollEventThrottle={16}>
                    {this.renderScrollViewContent()}
                    <View style={{height: HEADER_EXPANDED_HEIGHT}}/>
                </ScrollView>
            </View>
        );
    }
}

const styless = StyleSheet.create({
    scrollContainer: {
        //padding: 16,
        paddingTop: HEADER_EXPANDED_HEIGHT
    },
    header: {
        flex: 1,
        backgroundColor: styles.color.statBarColor,
        position: 'absolute',
        width: SCREEN_WIDTH,
        top: 0,
        left: 0,
        zIndex: 9999
    },
    backgroundImage: {
        position: 'absolute',
        top: -HEADER_EXPANDED_HEIGHT,
        left: 0,
        right: 0,
        width: SCREEN_WIDTH,
        height: HEADER_EXPANDED_HEIGHT,
        resizeMode: 'cover',
    },
    title: {
        marginVertical: 16,
        color: "black",
        fontWeight: "bold",
        fontSize: 24
    }
});
