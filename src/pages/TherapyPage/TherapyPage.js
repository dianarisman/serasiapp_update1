import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import styles from "../../styles/MainStyle";
import {
    ActivityIndicator,
    FlatList,
    RefreshControl,
    StyleSheet,
    TouchableOpacity,
    View,
    Picker,
    TouchableHighlight,
    Clipboard
} from "react-native";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import CalendarStrip from 'react-native-calendar-strip';
import moment from "moment";
import variables from "../../variables/MyGlobalVariable";
import PopoverSelectService from "../../components/Popover/PopoverSelectService";
import Popover from 'react-native-popover-view';

let addDatesWhitelist = 0;
let sekarang = moment().format('YYYYMMDD');
let sabtu = moment().add((5-moment().weekday()), 'days').format('YYYYMMDD');
let minggu = moment().add((6-moment().weekday()), 'days').format('YYYYMMDD');

export default class TherapyPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            selectDate: sekarang,
            selectTime: null,
            isVisiblePopoverService: false,
            selectServiceData: variables.consultation_service_list_therapy[0], //default array 0 atau service pertama
            consultationServiceList: variables.consultation_service_list_therapy,
            timeListItem: [
                { id: 1, key: "1", name: '08:00 AM', is_selected: false, is_disabled: false },
                { id: 2, key: "2", name: '09:00 AM', is_selected: false, is_disabled: false },
                { id: 3, key: "3", name: '10:00 AM', is_selected: false, is_disabled: true },
                { id: 4, key: "4", name: '11:00 AM', is_selected: false, is_disabled: false },
                { id: 5, key: "5", name: '12:00 PM', is_selected: false, is_disabled: true },
                { id: 6, key: "6", name: '01:00 PM', is_selected: false, is_disabled: false },
                { id: 7, key: "7", name: '02:00 PM', is_selected: false, is_disabled: true },
                { id: 8, key: "7", name: '03:00 PM', is_selected: false, is_disabled: true },
                { id: 9, key: "8", name: '04:00 PM', is_selected: false, is_disabled: true },
                { id: 10, key: "9", name: '05:00 PM', is_selected: false, is_disabled: false },
                { id: 11, key: "10", name: '06:00 PM', is_selected: false, is_disabled: false }
            ]
        }
    }

    _selectTime(dataItem) {
        const { timeListItem } = this.state;
        timeListItem.forEach((elem) => {
            elem.is_selected = false;
            if (elem.id === dataItem.id) {
                elem.is_selected = true
            }
        });
        this.setState({
            timeListItem,
            selectTime: dataItem.name
        });
    }

    _selectDate(event) {
        this.setState({
            selectDate: moment(event).format('YYYYMMDD')
        });
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        GlobalDataRequest.tes().then((result)=>{
            this.setState({isLoading: false});
            alert(JSON.stringify(result));
        }, () => {
            alert('cant request to server');
        });
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    _selectService(data) {
        const index = this.state.consultationServiceList.findIndex(
            item => data.id === item.id
        );
        this.setState({
            selectServiceData: this.state.consultationServiceList[index],
            selectService: data.name,
            isVisiblePopoverService: false
        });
    }

    numberWithCommas(x) {
        const parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    _onPressPay() {
        if(this.state.selectDate === sabtu || this.state.selectDate === minggu) {
            GlobalDataRequest.toastHandler(variables.dateEmpty,'danger');
            return;
        }else if(this.state.selectTime === null) {
            GlobalDataRequest.toastHandler(variables.timeEmpty,'danger');
            return;
        }

        this.props.navigation.navigate('UploadProofOfPayment', {
            dataDetail: {
                serviceId: this.state.selectServiceData.id,
                serviceKey: this.state.selectServiceData.key,
                serviceName: this.state.selectServiceData.name,
                servicePrice: this.state.selectServiceData.price,
                selectDate: this.state.selectDate,
                selectTime: this.state.selectTime,
            }
        })
    }

    _setContent() {
        GlobalDataRequest.toastHandler(variables.copied, 'success', 'top');
        Clipboard.setString(variables.no_rek);
    }

    render() {
        if(sekarang === sabtu) {
            addDatesWhitelist = 6
        }else if(sekarang === minggu) {
            addDatesWhitelist = 5
        }else{
            addDatesWhitelist = 4
        }
        let datesWhitelist = [{
            start: moment(),
            end: moment().add(addDatesWhitelist, 'days')
        }];
        let datesBlacklist = [
            moment().add((5-moment().weekday()), 'day'), //disabled sabtu & minggu
            moment().add((6-moment().weekday()), 'day'), //disabled sabtu & minggu
        ];
        let locale = {
            name: 'id',
            config: []
        };
        let renderServiceList = this.state.consultationServiceList.map( (data, index) => {
            return <Picker.Item key={index} value={data.name} label={data.name +'\n'+data.price } />
        });
        return (
            <Container>
                <MyToolbarTab
                    backIconVisible={true}
                    navigation={this.props.navigation}
                />
                {/*<Content style={styles.otherStyle.containerMain}
                         refreshControl={
                             <RefreshControl
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                             />}
                >

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10,
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'}}>
                        <View>
                            <Text style={{fontWeight: 'bold', color: '#646464', fontSize: 13, textTransform: 'uppercase' }}>{variables.services} - {this.state.selectServiceData.name}</Text>
                            <Text style={{color: '#b0b8c2'}}>{this.state.selectServiceData.value}</Text>
                        </View>
                        <TouchableOpacity ref={ref => this.touchableSelectService = ref} activeOpacity={0.7} style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}
                                          onPress={() => this.setState({isVisiblePopoverService: true})}>
                            <Text style={{color: '#19a97e', fontSize: 18, fontWeight: 'bold'}}>Rp {this.numberWithCommas(this.state.selectServiceData.price)}</Text>
                            <Icon type='AntDesign' name='down' style={{fontSize: 15, marginHorizontal: 10, color: '#99a3ad'}}/>
                        </TouchableOpacity>
                        <Popover
                            placement='bottom'
                            isVisible={this.state.isVisiblePopoverService}
                            fromView={this.touchableSelectService}
                            onClose={() => this.setState({isVisiblePopoverService: false})}>
                            <PopoverSelectService
                                serviceList={this.state.consultationServiceList}
                                onSelectService={this._selectService.bind(this)}
                            />
                        </Popover>

                    </View>

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <Text
                            style={{
                                fontSize: 13,
                                fontWeight: 'bold',
                                color: '#646464',
                                position: 'absolute',
                                top: 10,
                                left: 10,
                                textTransform: 'uppercase'
                            }}>
                            {variables.consultationSetSchedule}
                        </Text>
                        <CalendarStrip
                            calendarHeaderStyle={{textAlign: 'right',alignSelf: 'flex-end', fontWeight: 'bold', color: '#99a3ad', fontSize: 13}}
                            updateWeek={false}
                            onDateSelected={(ev)=> this._selectDate(ev)}
                            locale={locale}
                            datesWhitelist={datesWhitelist}
                            datesBlacklist={datesBlacklist}
                            highlightDateNameStyle={{color: '#0b00ef'}}
                            daySelectionAnimation={{type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#0b00ef'}}
                            highlightDateNumberStyle={{color: '#0b00ef'}}
                            iconContainer={{flex: 0.1}}
                            style={{height:85}}
                        />
                    </View>

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <FlatList
                            data={ this.state.timeListItem }
                            renderItem={ ({item, index}) =>
                                <TouchableOpacity
                                    disabled={item.is_disabled}
                                    key={index} style={[
                                    styles.consultationPage.GridViewContainer,
                                    item.is_selected ? styles.consultationPage.itemSelected : null,
                                    item.is_disabled ? styles.consultationPage.itemDisabled : null,
                                ]}
                                    onPress={()=> this._selectTime(item)}>
                                    <Text
                                        style={[
                                            styles.consultationPage.GridViewTextLayout,
                                            styles.consultationPage.itemSelectedText(item.is_selected ? '#fff' : '#99a3ad'),
                                            item.is_disabled ? styles.consultationPage.itemDisabled : null
                                        ]}
                                    > {item.name} </Text>
                                </TouchableOpacity> }
                            keyExtractor={item => item.key}
                            extraData={this.state}
                            numColumns={4}
                        />
                    </View>

                    <View style={{marginVertical: 5, backgroundColor: '#ffffff', padding: 10}}>
                        <Text style={{fontWeight: 'bold', color: '#646464', fontSize: 13, textTransform: 'uppercase'}}>{variables.payment} - {variables.transferBankSerasi}</Text>
                        <TouchableOpacity onPress={()=> this._setContent()} style={{flexDirection: 'row'}}>
                            <Text style={{color: '#b0b8c2', fontSize: 15}}>{variables.no_rek}</Text>
                            <Icon style={{color: '#b0b8c2', marginHorizontal: 10, fontSize: 18}} name='content-copy' type='MaterialIcons'/>
                        </TouchableOpacity>

                    </View>

                    <TouchableOpacity activeOpacity={ 0.8 } style={{...styles.formLogin.button(styles.color.serasiColorButton), marginHorizontal: 10}}
                                      onPress={() => this._onPressPay() }>
                        {(this.state.isLoading) ? <ActivityIndicator size="small" color="#ffffff" /> : <Text style={styles.formLogin.buttonText}>{variables.pay}</Text> }
                    </TouchableOpacity>
                </Content>*/}
                <Content
                    style={{...styles.otherStyle.containerMain}}
                    contentContainerStyle={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text>Masih Dalam Tahap Development</Text>
                </Content>
            </Container>
        );
    }
}
