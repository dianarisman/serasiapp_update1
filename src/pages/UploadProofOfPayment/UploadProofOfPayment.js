import React, { Component } from 'react';
import { Container, Content, Icon } from 'native-base';
import {View, Text, Dimensions, TouchableOpacity, Image, Alert, RefreshControl} from 'react-native';
import MyToolbarTab from "../../components/ToolbarTransparent/MyToolbarTab";
import styles from "../../styles/MainStyle";
import variables from "../../variables/MyGlobalVariable";
import GlobalDataRequest from "../../services/GlobalDataRequest";
import Lightbox from "react-native-lightbox";
const { width: SCREEN_WIDTH }= Dimensions.get('window');
import Carousel from "react-native-looped-carousel";

export default class UploadProofOfPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMount: false,
            user_token: null,
            isLoading: false,
            imageHeight: 0,
            imageWidth: 0,
            imageData: null,
            imageBase64: null,
            isFetchFailed: false,
            bankAccount: [],
            paymentData: this.props.navigation.getParam('paymentData')
        };
        this.dataDetail = this.props.navigation.getParam('paymentData');
    }

    componentDidMount() {
        this.setState({isMount: true});
        this.getAuth();
    }

    componentWillUnmount(){
        this.setState({isMount: false});
    }

    getAuth() {
        this.setState({isLoading: true});
        GlobalDataRequest.authToken().then((resolve) => {
            //console.log('tokennya '+resolve);
            this.setState( {user_token: resolve });
            GlobalDataRequest.getBankAccount(resolve).then((result)=>{
                this.setState({isLoading: false, isFetchFailed: false});
                if(this.state.isMount){
                    if(result.success){
                        this.setState({
                            bankAccount: result.data
                        });
                    }else{
                        Alert.alert(variables.warning, variables.somethingWrong);
                    }
                }

            }).catch(error=>{
                console.log('error getBankAccount', error.message);
                this.setState({isLoading: false, isFetchFailed: true});
            });
        });
    }

    selectPhotoTapped() {
        GlobalDataRequest.takePhotoHandler().then((result) => {
            // You can also display the image using data:
            try {
                const source = { uri: 'data:image/jpeg;base64,' + result.data };
                // const source = { uri: result.uri };
                const ratio = SCREEN_WIDTH/result.width;
                const imgBase64 = 'data:image/jpeg;base64,' + result.data;
                this.setState({
                    isLoading: false,
                    imageHeight: result.height * ratio,
                    imageWidth: SCREEN_WIDTH,
                    imageData: source,
                    imageBase64: imgBase64
                });
            }catch (e) {

            }

        });
    }



    _sendProofPayment() {
        this.setState({isLoading: true});
        GlobalDataRequest.confirmBuyPackage(this.state.user_token,this.dataDetail.id,this.state.imageBase64).then((result)=>{
            this.setState({isLoading: false});
            console.log('_sendProofPayment ', result);
            if(result.success){
                this.props.navigation.replace('Home');
                GlobalDataRequest.toastHandler(variables.successSendProofPayementMessage,'success','top');
            }else{
                GlobalDataRequest.toastHandler(variables.somethingWrong,'danger','top');
            }
            /*Alert.alert(variables.success, variables.successSendProofPayementMessage,
                [
                    {text: 'OK', onPress: () => this.props.navigation.replace('Home')},
                ],
                {cancelable: false});*/
        }).catch(()=>{
            Alert.alert(variables.warning, variables.somethingWrong);
            this.setState({isLoading: false});
        });
    }

    _onContentRefresh = () => {
        this.getAuth();
        //setInterval(this.setState({isLoading: false}), 3000);
    };

    render() {
        console.log(this.dataDetail.konfirmasi_pembayaran);
        let renderImage = this.dataDetail.konfirmasi_pembayaran.map( (data, index) => {
            return <Image
                key={data.id.toString()}
                style={{ flex: 1 }}
                resizeMode="contain"
                source={{ uri: GlobalDataRequest.renderImageUri(data.photo) }}
            />
        });
        const renderCarousel = () => (
            <Carousel
                pageInfoTextStyle={{color: '#fff'}}
                pageInfo={true}
                autoplay={false}
                style={{ width: SCREEN_WIDTH, height: SCREEN_WIDTH }}>
                {renderImage}
            </Carousel>
        );

        let renderBankAccount = [];
        const loadingBankAccount = (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                backgroundColor: '#ffffff',
                padding: 10,
            }}>
                <Text
                    style={{
                        color: '#9b9b9b',
                        fontSize: 16,
                    }}
                >{variables.loadingBankData}</Text>
            </View>
        );
        this.state.bankAccount.forEach((data, i) => {
            const bankData = (
                <View key={i.toString()} style={{marginBottom: 10}}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: '#ffffff',
                        padding: 10,
                        justifyContent: 'space-between'
                    }}>
                        <View>
                            <Text
                                style={{
                                    color: '#9b9b9b',
                                    fontSize: 16,
                                }}
                            >{variables.accountNumberLabel}</Text>
                            <Text
                                style={{
                                    color: '#4a4a4a',
                                    fontSize: 16,
                                    fontWeight: 'bold'
                                }}
                            >{data.no_rekening}</Text>
                        </View>
                        <View>
                            <TouchableOpacity
                                onPress={()=>GlobalDataRequest.onCopyText(data.no_rekening)}>
                                <Text
                                    style={{
                                        textTransform: 'uppercase',
                                        color: '#5d64ed',
                                        fontSize: 16
                                    }}
                                >{variables.copy}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: '#ffffff',
                        padding: 10,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{width: SCREEN_WIDTH/1.4}}>
                            <Text
                                style={{
                                    color: '#9b9b9b',
                                    fontSize: 16,
                                }}
                            >{variables.receiverName}</Text>
                            <Text
                                style={{
                                    color: '#4a4a4a',
                                    fontSize: 16,
                                    flex: 0.7,
                                    fontWeight: 'bold',
                                    flexWrap: 'wrap'
                                }}
                            >{data.nama_pemilik}</Text>
                        </View>
                        <View>
                            <Image
                                style={{
                                    width: 65,
                                    height: 19
                                }}
                                source={{uri: GlobalDataRequest.renderImageUri(data.logo_bank)}}
                            />
                        </View>
                    </View>
                </View>
            );
            renderBankAccount.push(bankData);
        });
        return (
            <Container>
                <MyToolbarTab
                    isFetchFailed={this.state.isFetchFailed}
                    tryAgainFetchFunc={this.getAuth.bind(this)}
                    backIconVisible={true}
                    navigation={this.props.navigation}
                    sendProofButtonVisible={!!this.state.imageData && !this.state.isLoading}
                    isLoadingButtonVisible={this.state.imageData && this.state.isLoading}
                    onPressProofButton={this._sendProofPayment.bind(this)}
                />
                <Content style={styles.otherStyle.containerMain}
                         refreshControl={
                             <RefreshControl
                                 refreshing={this.state.isLoading}
                                 onRefresh={this._onContentRefresh}
                                 tintColor={styles.color.serasiColorButton2}
                                 title="Loading..."
                                 titleColor="#00ff00"
                                 colors={[styles.color.serasiColorButton2]}
                                 progressBackgroundColor={styles.color.backgroundColorProgressCircle}
                             />}
                >
                    {/*<View>
                        <View style={{backgroundColor: '#ffffff', padding: 10}}>
                            <Text>
                                Pembayaran dengan code : {this.state.paymentData.orders_id}
                            </Text>
                            <Text>
                                Transfer Bank : {variables.no_rek} a/n {variables.no_rek_name}
                            </Text>
                            <Text>
                                Bank : {variables.no_rek_bank_name}
                            </Text>
                        </View>
                        <View style={{backgroundColor: '#ffffff', padding: 10}}>
                            <Text style={{fontStyle: 'italic'}}>
                                {variables.waitServiceMessage}
                            </Text>
                        </View>
                        <View style={{
                            minHeight: SCREEN_HEIGHT-140,
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center"
                        }}>
                            <TouchableOpacity activeOpacity={0.7} onPress={this.selectPhotoTapped.bind(this)}>
                                {
                                    this.state.imageData ? (<Image source={this.state.imageData}
                                                                    style={{height: this.state.imageHeight,
                                                                        width: this.state.imageWidth}}/>) : (<Icon type='Entypo' name='camera' style={{color: '#ccc', fontSize: (SCREEN_HEIGHT-100)/5}}/>)
                                }
                            </TouchableOpacity>
                        </View>
                    </View> */}

                    <View style={{backgroundColor: '#f0f0f0', padding: 10}}>
                        <Text
                            style={{
                                color: '#333a4d',
                                fontSize: 20,
                                fontWeight: 'bold'
                            }}
                        >{variables.payment}</Text>
                        <Text
                            style={{
                                color: '#9b9b9b',
                                fontSize: 12,
                                fontWeight: 'bold'
                            }}
                        >ID : {this.dataDetail.order_code}</Text>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}>
                            <Text
                                style={{
                                    color: '#9b9b9b',
                                    fontSize: 14,
                                    fontWeight: 'bold'
                                }}
                            >{variables.doTransferBank}</Text>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: '#ffffff',
                        padding: 10}}>
                        <Icon
                            name='access-time'
                            type='MaterialIcons'
                            style={{
                                color: '#367dcf',
                                fontSize: 16,
                                paddingRight: 5
                            }}

                        />
                        <Text
                            style={{
                                color: '#4a4a4a',
                                fontSize: 16,
                            }}
                        >{GlobalDataRequest.dateFormater((this.dataDetail.order_date+' '+this.dataDetail.order_time),'','Do MMMM YYYY, h:mm a')}</Text>
                    </View>

                    <View style={{backgroundColor: '#f0f0f0', padding: 10}}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            paddingVertical: 6,
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}>
                            <Text
                                style={{
                                    color: '#333a4d',
                                    fontSize: 17,
                                    fontWeight: 'bold',
                                    textTransform: 'uppercase'
                                }}
                            >{this.dataDetail.order_detail[0].layanan.categories.name} {this.dataDetail.order_detail[0].layanan.roles.display_name}</Text>
                            <Icon
                                name='info-circle'
                                type='FontAwesome'
                                style={{
                                    color: '#367dcf',
                                    fontSize: 16,
                                    paddingRight: 5
                                }}

                            />
                        </View>
                    </View>

                    <View>
                        {/*<Text>{JSON.stringify(this.dataDetail.konfirmasi_pembayaran)}</Text>*/}
                        {renderBankAccount.length < 1 ? loadingBankAccount : renderBankAccount}
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: '#ffffff',
                            padding: 10,
                            justifyContent: 'space-between'
                        }}>
                            <View>
                                <Text
                                    style={{
                                        color: '#9b9b9b',
                                        fontSize: 16,
                                    }}
                                >{variables.paymentTotal}</Text>
                                <Text
                                    style={{
                                        color: '#05a273',
                                        fontSize: 16,
                                        fontWeight: 'bold'
                                    }}
                                >Rp. {GlobalDataRequest.numberFormat(this.dataDetail.order_detail[0].layanan.price)}</Text>
                            </View>
                            <View>
                                <TouchableOpacity
                                    onPress={()=>GlobalDataRequest.onCopyText(this.dataDetail.order_detail[0].layanan.price)}>
                                    <Text
                                        style={{
                                            textTransform: 'uppercase',
                                            color: '#5d64ed',
                                            fontSize: 16
                                        }}
                                    >{variables.copy}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View
                        style={{
                            marginVertical: 15
                        }}
                    >


                        {this.dataDetail.status !== 'approved' ? (<TouchableOpacity
                            onPress={this.selectPhotoTapped.bind(this)}
                            style={{
                                backgroundColor: styles.color.serasiColorButton,
                                alignItems: 'center'
                            }}
                        >
                            <Text
                                style={{
                                    paddingVertical: 12,
                                    paddingHorizontal: 3,
                                    fontSize: 16,
                                    color: '#fff'
                                }}
                            >{variables.uploadProofPayment}</Text>
                        </TouchableOpacity>) : null}

                        {this.dataDetail.konfirmasi_pembayaran.length > 0 ?
                            (
                                <Lightbox
                                    underlayColor='#fff'
                                    style={{
                                        alignItems: 'center'
                                    }}
                                    springConfig={{tension: 15, friction: 7}} swipeToDismiss={false} renderContent={renderCarousel}>
                                    <Text
                                        style={{
                                            paddingVertical: 12,
                                            paddingHorizontal: 3,
                                            fontSize: 16,
                                            color: '#000'
                                        }}
                                    >{variables.viewProof}</Text>
                                </Lightbox>
                            ) : null
                        }

                    </View>

                </Content>
            </Container>
        );
    }
}
