import * as firebase from "firebase";
import '@firebase/messaging';
import {YellowBox} from "react-native";
import _ from "lodash";
import variables from "../variables/MyGlobalVariable";

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
    if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
    }
};
const config = {
    apiKey: variables.apiKey,
    authDomain: variables.authDomain,
    databaseURL: variables.databaseURL,
    projectId: variables.projectId,
    storageBucket: variables.storageBucket,
    messagingSenderId: variables.messagingSenderId
};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
