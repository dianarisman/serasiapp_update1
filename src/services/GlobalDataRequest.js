import React from 'react';
import variables from "../variables/MyGlobalVariable";
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from "react-native-image-picker";
import {Toast} from "native-base";
import moment from 'moment';
import 'moment/min/locales';
import {Clipboard} from "react-native";
const FETCH_TIMEOUT_INTERVAL = 1000*60;
const SHOW_LOG = true;
class GlobalDataRequest extends React.Component {
    constructor(props) {
        super(props);
        moment.locale('id');
        this.state = {
            token: null,
            fcm_token: null
        };

        this.cacheRegist = 'aa';
        this.token = 'aa';
    }
    static tes(){
        return new Promise((resolve, reject) => {
            fetch(variables.testerUrl)
                .then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        });
    }

    static getExplore(token, criteria_pasangan, city, status_marital){
        let bodyParam = {
            criteria_pasangan: criteria_pasangan,
            city: city,
            status_marital: status_marital
        };
        if(city === '') {
            delete bodyParam.city;
        }

        if(status_marital === '' || status_marital === 'Status perkawinan') {
            delete bodyParam.status_marital;
        }

        if(criteria_pasangan.length <= 0) {
            delete bodyParam.criteria_pasangan;
        }

        //alert(JSON.stringify(bodyParam, null, 3));

        return new Promise((resolve, reject) => {
            fetch(variables.exploreUrl, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                body: JSON.stringify(bodyParam)
                //body: JSON.stringify(dataBody),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        });
    }

    static consoleLog(title, message) {
        if(SHOW_LOG){
            console.log(title,message)
        }
    }
    static getExploreLoadMore(url, token, criteria_pasangan, city, status_marital){

        let bodyParam = [];
        if(city !== '') {
            bodyParam.push({city: city})
        }

        if(status_marital !== '') {
            bodyParam.push({city: status_marital})
        }

        if(criteria_pasangan.length > 0) {
            bodyParam.push({criteria_pasangan: criteria_pasangan});
        }

        let bodyParamFix = bodyParam.map(item => {
            if(item.city === undefined) {
                item.city = city
            }

            if(item.status_marital === undefined) {
                item.status_marital = status_marital
            }

            if(item.criteria_pasangan === undefined) {
                item.criteria_pasangan = criteria_pasangan
            }
            return item;
        });
        //alert(JSON.stringify(bodyParamFix, null, 3));

        return new Promise((resolve, reject) => {
            fetch(url, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                body: JSON.stringify(bodyParamFix[0])
                //body: JSON.stringify(dataBody),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        });
    }

    static login(username, password, fcm_token){
        return new Promise((resolve, reject) => {
            const params = {
                username: username,
                password: password,
                remember: true
            };
            const searchParams = Object.keys(params).map((key) => {
                return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
            }).join('&');

            let tempData = {
                remember: true,
                email: username.trim(),
                phone: username.trim(),
                password: password,
                fcm_token: fcm_token
            };

            let bodyParam = tempData;
            if(!isNaN(username)) {
                delete bodyParam.email;
            }else{
                delete bodyParam.phone;
            }

            fetch(variables.loginUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(bodyParam),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static signUp(data){
        return new Promise((resolve, reject) => {

            fetch(variables.signUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }



    static getQuota(token){
        return new Promise((resolve, reject) => {

            fetch(variables.quotaCount, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static getNotifList(token){
        return new Promise((resolve, reject) => {
            fetch(variables.notifUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static getBankAccount(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.bankAccount, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                }).finally(()=>clearInterval(interval));
        })
    }

    static getNews(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.newsUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                }).finally(()=>clearInterval(interval));
        })
    }

    static getProfile(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.profileUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    
                    reject(error);
                    console.error(error);
                }).finally(()=>clearInterval(interval));
        })
    }

    static getHome(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.homeUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                }).finally( () => clearInterval(interval))
        })
    }

    static updateProfile(token, user_id, dataUpdate){
        return new Promise((resolve, reject) => {
            fetch(variables.profileUrl+'/'+user_id, {
                method: 'PUT',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache",
                body: JSON.stringify(dataUpdate)
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static renderAvatar(src, gender) {
        let source = null;
        if(src === 'users/default.png' || src === '') {
            if(gender === 'pria'){
                source = 'https://i.ibb.co/y5bLB9q/ava-man.png';
            }else if (gender === 'wanita'){
                source = 'https://i.ibb.co/R4yTWK4/ava-woman.png';
            }else {
                source = this.renderImageUri(src);
            }
        } else{
            source = this.renderImageUri(src);
        }
        return source;
    }

    static renderRandomAvatar(src, gender: "men | women") {
        const min = 1;
        const max = 100;
        const rand = parseInt((min + Math.random() * (max - min)),10);
        let source = '';
        if(src === 'users/ava_man.png') {
            source = 'https://randomuser.me/api/portraits/men/'+rand+'.jpg';
        }else if(src === 'users/ava_woman.png') {
            source = 'https://randomuser.me/api/portraits/women/'+rand+'.jpg';
        }else{
            source = this.renderImageUri(src);
        }
        return source;
    }

    static renderImageUri(src) {
        this.consoleLog('renderImageUri', variables.imageUrl+src);
        return variables.imageUrl + src;
    }

    static logout(token){
        return new Promise((resolve, reject) => {
            fetch(variables.logoutUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static changeAvatar(token, user_id, avatarData){
        return new Promise((resolve, reject) => {
            fetch(variables.changeAvatarUrl+'/'+user_id, {
                method: 'PUT',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache",
                body: JSON.stringify({
                    avatar: avatarData
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static interest(token, user_id_other_member){
        return new Promise((resolve, reject) => {
            fetch(variables.interestUrl+'/'+user_id_other_member, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static sendNotif(from, messageData, to_fcm_token, user_id){
        const notification = {
            'registration_ids':[to_fcm_token],
            'data':{
                'id': user_id.toString(),
                "priority": "normal",
                //"collapseKey": "demo",
                //"content_available": true,
                "tag": "directMessage",
                'message':messageData,
                'title':from,
                //"show_in_foreground": true,
                //"vibrate":true, //additionalData
                "sound":'default', //additionalData
            }
        };

        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'key='+variables.fcm_server_key
        };

        return new Promise((resolve, reject) => {
            fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                //withCredentials: true,
                headers: header,
                body: JSON.stringify(notification),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    setTimeout( () => {
                        reject(error);
                        this.sendNotif(from, messageData, to_fcm_token, user_id);
                    },3000);
                    console.error(error);
                });
        })
    }

    static sendNotifInteresting(from, messageData, to_fcm_token, user_id){
        const notification = {
            'registration_ids':[to_fcm_token],
            'data':{
                'id': user_id,
                "priority": "hight",
                'message':messageData,
                'title':from,
                'vibrate':true, //additionalData
                'sound':true, //additionalData
            }
        };

        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'key='+variables.fcm_server_key
        };

        return new Promise((resolve, reject) => {
            fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                //withCredentials: true,
                headers: header,
                body: JSON.stringify(notification),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    setTimeout( () => {
                        reject(error);
                        this.sendNotifInteresting(from, messageData, to_fcm_token, user_id);
                    },3000);
                    console.error(error);
                });
        })
    }

    static selectPackage(token, layanan_id){
        return new Promise((resolve, reject) => {
            fetch(variables.selectPackageUrl, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache",
                body: JSON.stringify({
                    layanan_id: layanan_id
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static interestCanceling(token, interest_id, status, note){
        return new Promise((resolve, reject) => {
            fetch(variables.interestCancelingUrl+'/'+interest_id, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache",
                body: JSON.stringify({
                    status: status,
                    note: note
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static confirmBuyPackage(token, order_id, photo){
        return new Promise((resolve, reject) => {
            fetch(variables.confirmBuyPackage, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache",
                body: JSON.stringify({
                    orders_id: order_id,
                    photo: photo
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static resetPassword(email){
        return new Promise((resolve, reject) => {
            fetch(variables.resetPasswordUrl, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                "cache-control": "no-cache",
                body: JSON.stringify({
                    email: email
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static changePassword(token, currentPassword, newPassword, confirmNewPassword){
        return new Promise((resolve, reject) => {
            fetch(variables.changePasswordUrl, {
                method: 'POST',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache",
                body: JSON.stringify({
                    'current-password':currentPassword,
                    'new-password':newPassword,
                    'new-password_confirmation':confirmNewPassword,
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static getPackageList(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.packageUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                    
                })
                .catch((error) =>{
                    
                    reject(error);
                    console.error(error);
                }).finally(()=> clearInterval(interval));
        })
    }

    static introduction(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.introductionUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                    
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                }).finally(()=> clearInterval(interval));
        })
    }

    static getServiceList(token){
        return new Promise((resolve, reject) => {
            fetch(variables.serviceUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static getHistoryPackage(token){
        return new Promise((resolve, reject) => {
            let interval = setInterval(() => reject( new Error('Request timed out') ), FETCH_TIMEOUT_INTERVAL);
            fetch(variables.packageHistoryUrl, {
                method: 'GET',
                withCredentials: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                "cache-control": "no-cache"
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                    
                }).finally(()=>clearInterval(interval));
        })
    }

    static numberFormat(number){
        /*
        const parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
        */
        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }

    static getAge(birth_date){
        return moment().diff(birth_date, 'years');
    }

    static dateFromNow(date) {
        moment.locale('id');
        return moment(date).fromNow();
    }

    static dateFormater(date, dateFormat, dateOutputFormat){
        /*
        moment().format('MMMM Do YYYY, h:mm:ss a'); // April 24 2019, 10:41:32 malam
        moment().format('dddd');                    // Rabu
        moment().format("MMM Do YY");               // Apr 24 19
        moment().format('YYYY [escaped] YYYY');     // 2019 escaped 2019
        moment().format('LT');   // 22.43
        moment().format('LTS');  // 22.43.31
        moment().format('L');    // 24/04/2019
        moment().format('l');    // 24/4/2019
        moment().format('LL');   // 24 April 2019
        moment().format('ll');   // 24 Apr 2019
        moment().format('LLL');  // 24 April 2019 pukul 22.43
        moment().format('lll');  // 24 Apr 2019 pukul 22.43
        moment().format('LLLL'); // Rabu, 24 April 2019 pukul 22.43
        moment().format('llll');
        */
        let dateMoment = '';
        if(dateFormat === null || dateFormat === ''){
            dateMoment = moment(date).format(dateOutputFormat);
        }else{
            dateMoment = moment(date, dateFormat).format(dateOutputFormat);
        }
        return dateMoment;
    }

    static onCopyText(textToCopy) {
        this.toastHandler(variables.copied, 'success', 'top');
        Clipboard.setString(''+textToCopy);
    }

    static statusPaymentHandler(status){
        let statusReturn = 'undefined';
        if(status==='waiting') {
            statusReturn = variables.waitingPayment
        }else if(status==='approved'){
            statusReturn = variables.approvedPayment
        }else if(status==='waiting_payment' || 'waiting_approval'){
            statusReturn = variables.waitingPaymentApproved
        }
        return statusReturn
    }
    static login_eCluster(username, password){
        return new Promise((resolve, reject) => {
            fetch(variables.loginUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    client_secret: 'secret1',
                    grant_type: 'password',
                    client_id: 'id1',
                    username: username,
                    password: password
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        })
    }

    static testingSaveBase64(data){
        return new Promise((resolve, reject) => {
            fetch("http://192.168.0.4/save_base64/save_data.php", {
                method: 'POST',
                headers: {
                    Accept: 'application/x-www-form-urlencoded',
                    'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                body: 'datanya='+data
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    console.error(error);
                });
        }).catch((err)=> {
            alert("Somthin wrong "+JSON.stringify(err));
        })
    }

    static AuthSave = async (authData, authStatus, authToken) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.authToken, authToken);
                await AsyncStorage.setItem(variables.authKey, authData);
                await AsyncStorage.setItem(variables.authStatus, authStatus);
                await AsyncStorage.getItem(variables.authKey, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                reject(JSON.stringify(error))
            }
        });

    };

    static firsLoginSave = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.getItem(variables.firsLogin_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userIdSave ', error.message);
                reject(error.message)
            }
        });

    };

    static firsLoginGet = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.getItem(variables.firsLogin_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userIdSave ', error.message);
                reject(error.message)
            }
        });

    };

    static userIdSave = async (user_id) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.user_id_KEY, user_id);
                await AsyncStorage.getItem(variables.user_id_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userIdSave ', error.message);
                reject(error.message)
            }
        });

    };

    static fcmUserTokenSave = async (fcm_token) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.fcm_token_KEY, fcm_token);
                await AsyncStorage.getItem(variables.fcm_token_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userIdSave ', error.message);
                reject(error.message)
            }
        });

    };

    static getFcmUserToken = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.getItem(variables.fcm_token_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userIdSave ', error.message);
                reject(error.message)
            }
        });
    };

    static userAvatarSave = async (value) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.user_avatar_KEY, this.renderAvatar(value, ''));
                await AsyncStorage.getItem(variables.user_avatar_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userAvatarSave ', error.message);
                reject(error.message)
            }
        });

    };

    static userRegistrationCache = async (value) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.user_registration_cache_KEY, value);
                await AsyncStorage.getItem(variables.user_registration_cache_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userRegistrationCache ', error.message);
                reject(error.message)
            }
        });
    };

    static getUserRegistrationCache = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.getItem(variables.user_registration_cache_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userIdSave ', error.message);
                reject(null)
            }
        });
    };


    static userNameSave = async (value) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.user_name_KEY, value);
                await AsyncStorage.getItem(variables.user_name_KEY, (error, result) => {
                    if (result) {
                        resolve(result);
                    }
                });
            } catch (error) {
                console.log('error userAvatarSave ', error.message);
                reject(error.message)
            }
        });
    };

    static authClear = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.removeItem(variables.authToken);
                await AsyncStorage.removeItem(variables.authKey);
                await AsyncStorage.removeItem(variables.authStatus);
                await AsyncStorage.removeItem(variables.user_id_KEY);
                await AsyncStorage.removeItem(variables.user_avatar_KEY);
                await AsyncStorage.removeItem(variables.user_name_KEY);
                await AsyncStorage.removeItem(variables.fcm_token_KEY);
                resolve(true);
            } catch (error) {
                reject(JSON.stringify(error))
            }
        });
    };

    static authToken = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                const authToken = await AsyncStorage.getItem(variables.authToken);
                resolve(authToken);
            } catch (error) {
                reject(JSON.stringify(error))
            }
        });
    };

    static appIntroSave = async (data) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.showAppIntroKey, data);
            } catch (error) {
                reject(JSON.stringify(error.message))
            }
        });

    };

    static saveCountBadgePackage = async (count) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(variables.countNotConfirmationBuyPackage, count);
                await AsyncStorage.getItem(variables.countNotConfirmationBuyPackage, (error, result) => {
                    if (result) {
                        resolve(result);
                    }else{
                        resolve(0);
                    }
                });
            } catch (error) {
                resolve(0);
                console.log('countBadgePackage ', error.message);
                reject(error.message);
            }
        });

    };

    static getCountBadgePackage = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.getItem(variables.countNotConfirmationBuyPackage, (error, result) => {
                    if (result) {
                        resolve(result);
                    }else{
                        resolve(0);
                    }
                });
            } catch (error) {
                resolve(0);
                console.log('countBadgePackage ', error.message);
                reject(error.message);
            }
        });

    };

    static takePhotoHandler() {
        return new Promise((resolve, reject) => {
            const options = {
                title: 'Choose option',
                quality: 0.7,
                maxWidth: 500,
                //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
                storageOptions: {
                    skipBackup: true,
                    path: 'serasi-image',
                },
            };

            ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    reject(variables.imagePickerErrorMessageCode1);
                    console.log('ImagePicker Error: ', response.error);
                } /*else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }*/ else {
                    //API Reference
                    //https://github.com/react-native-community/react-native-image-picker/blob/master/docs/Reference.md
                    resolve(response);
                    this.toastHandler(variables.photoSaved);
                }
            });
        }, err => {
            this.toastHandler(variables.imagePickerErrorMessageCode2);
            console.log(variables.imagePickerErrorMessageCode2, err);
        }).catch(err => {
            this.toastHandler(variables.imagePickerErrorMessageCode3);
            console.log(variables.imagePickerErrorMessageCode3, err);
        })
    }

    static toastHandler(toastMessage, toastType, toastPosition) {
        Toast.show({
            text: toastMessage,
            duration: 3000,
            type: toastType,
            position: toastPosition ? toastPosition : 'bottom'
        });
    }

    static _checkIsApproved(item_id, data) {
        let status = '';
        if(data.is_approved !== undefined){
            if(data.is_approved[item_id]!==undefined){
                //console.log('statuscek ', result.isFollowing[item.id].status);
                if(item_id === data.is_approved[item_id].id){
                    status = data.is_approved[item_id].status;
                }else{
                    status = 'undefine1';
                }
            }else{
                status = 'undefine2';
            }
        }else {
            status = 'is_approved undefine';
        }
        return status;
    }

    static _checkIsFollowing(item_id, data) {
        let status = '';
        let interestId = 0;
        if(data.isFollowing !== undefined){
            if(data.isFollowing[item_id]!==undefined){
                //console.log('statuscek ', result.isFollowing[item.id].status);
                if(item_id === data.isFollowing[item_id].id){
                    status = data.isFollowing[item_id].status;
                    interestId = data.isFollowing[item_id].perkenalan_id;
                }else{
                    status = 'undefine1';
                }
            }else{
                status = 'undefine2';
            }
        }else {
            status = 'undefine3';
        }

        return {"status":status, "interestId":interestId};
    }
}
export default GlobalDataRequest;
