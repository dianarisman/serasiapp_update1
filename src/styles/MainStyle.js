import {Dimensions, PixelRatio} from "react-native";

const loginPage = {
    container : {
        backgroundColor:'#455a64',
        flex: 1,
        alignItems:'center',
        justifyContent :'center',
        width: '100%'
    },

    signupTextCont : {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'row'
    },

    signupText: {
        color:'rgba(255,255,255,0.6)',
        fontSize:16
    },

    signupButton: {
        color:'#ffffff',
        fontSize:16,
        fontWeight:'500'
    },
    content_bottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 5,
    },
    leftContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    rightContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    registerText: (color) => ({
        textAlign: 'left',
        color: color
    }),
    forgotPasswordText: (color) => ({
        textAlign: 'right',
        color: color
    }),
    segmentButtonFirst: (isActive) => ({
        borderLeftWidth: 1,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        backgroundColor: ((isActive) ? color.segmentActiveColor : "#ffffff"),
        borderColor: "#ff0400"
    }),
    segmentTextActive: (isActive) => ({
        color: ((isActive) ? "#ffffff" : color.segmentActiveColor)
    }),
    segmentButtonLast: (isActive) => ({
        borderLeftWidth: 0,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: ((isActive) ? color.segmentActiveColor : "#ffffff"),
        borderColor: "#ff0400"
    }),
};

const formLogin = {
    container : {
        flexGrow: 1,
        justifyContent:'center',
        alignItems: 'center'
    },

    inputBox: {
        width:300,
        backgroundColor:'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal:16,
        fontSize:16,
        color:'#ffffff',
        marginVertical: 10
    },
    buttonItem: (color) => ({
        backgroundColor:color,
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 13,
        flex: 1, flexDirection: 'row', justifyContent: 'center'
    }),
    button: (color) => ({
        backgroundColor:color,
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 13
    }),
    buttonBorder: (color) => ({
        borderWidth: 2,
        borderColor:color,
        color: '#000',
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 13
    }),
    buttonRegister: {
        backgroundColor:'#2890a8',
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize:16,
        fontWeight:'500',
        color:'#ffffff',
        textAlign:'center'
    },
    buttonTextColor: (color) => ({
        fontSize:16,
        fontWeight:'500',
        color:color,
        textAlign:'center'
    }),
    buttonTextBorder: (color) => ({
        fontSize:16,
        fontWeight:'500',
        color: color,
        textAlign:'center'
    }),
    buttonText2: {
        fontSize:16,
        fontWeight:'500',
        color:'#323232',
        textAlign:'center'
    },
    buttonTextRegister: {
        fontSize:16,
        fontWeight:'500',
        color:'#3e535d',
        textAlign:'center'
    }
};

const color = {
  backgroundColorProgressCircle: '#f9f9f9',
  jandaColor: '#aa3dfc',
  dudaColor: '#05a273',
  singleColor: '#ff54db',
  menikahColor: '#6998ef',
  placeholderInput: '#9e9e9e',
  errorInput: '#ff0400',
  serasiColorButton: '#ef415a',
  serasiColorButton2: '#6a85ec',
  serasiColorButton3: '#ff54db',
  serasiColorButton4: '#fff',
  segmentActiveColor: '#ef415a',
  statBarColor: '#d2d2d2',
  transparentColor: 'transparent',
  mainContentColor: '#f9f9f9',
  listItemSelect: (color) => ({
      color: color
  })
};

const pageRegister = {
    container : {
        backgroundColor:'#3e535d',
        flex: 1,
        alignItems:'center',
        justifyContent :'center'
    },
    signupTextCont : {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'row'
    },
    signupText: {
        color:'rgba(255,255,255,0.6)',
        fontSize:16
    },
    signupButton: {
        color:'#ffffff',
        fontSize:16,
        fontWeight:'500'
    },
    listItemHeader: {
        borderBottomWidth: 0, marginVertical: 10
    },
    listItemHeaderText: {
        fontSize:20,fontWeight: 'bold'
    }
};

const formRegister = {
    container : {
        flexGrow: 1,
        justifyContent:'center',
        alignItems: 'center'
    },

    inputBox: {
        width:300,
        backgroundColor:'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal:16,
        fontSize:16,
        color:'#ffffff',
        marginVertical: 10
    },
    button: {
        width:300,
        backgroundColor:'#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize:16,
        fontWeight:'500',
        color:'#ffffff',
        textAlign:'center'
    }
};

const testingLogin = {
    container: {
        flex: 1, justifyContent: "center", alignItems: "center"
    },
    inputText: {
        color:'#ff0400',
    },
    separatorWrap: {
        paddingVertical: 15,
        flexDirection: "row",
        alignItems: "center",
    },
    separator: (text) => ({
        borderBottomWidth: 0.8,
        flexGrow: 1,
        borderColor: text,
    }),
    separatorText: (text) => ({
        color: text,
        paddingHorizontal: 10,
    }),
};

const gridDimension = 120;
const otherStyle = {
    textWrap: {
        flex: 1, flexWrap: 'wrap'
    },
    closeButtonImageView: {
        color: 'white',
        borderWidth: 1,
        borderColor: 'white',
        padding: 8,
        borderRadius: 3,
        textAlign: 'center',
        margin: 10,
        alignSelf: 'flex-end',
    },
    packageContainer: (color) => ({
        alignItems: 'center',
        backgroundColor: color,
        borderRadius: 13,
        padding: 20,
        marginVertical: 10
    }),
    gridDimension: gridDimension,
    borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    cardDefaultBg: "#fff",
    cardBorderColor: "#ccc",
    cardBorderRadius: 2,
    cardCustom: {
        backgroundColor: "#fff",
        borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderRadius: 10,
        borderColor: "#ccc",
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3
    },
    cardCustomTransparent: {
        //backgroundColor: "#fff",
        //borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        //borderRadius: 10,
        //borderColor: "#ccc",
        //shadowColor: "#000",
        //shadowOffset: { width: 0, height: 2 },
        //shadowOpacity: 0.1,
        //shadowRadius: 1.5,
        //elevation: 3
    },
    containerData: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 5,
    },
    leftContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    rightContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoSlidesText: {
      fontSize: 18,
        color: '#515151',
        marginVertical: 6
    },
    infoSlidesTextMore: {
        fontSize: 14,
        color: color.serasiColorButton2,
        marginVertical: 4
    },
    infoDetailProfile: {
        fontSize: 23,
        fontWeight: 'bold'
    },
    gridView: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    gridItem: {
        width: (Dimensions.get('window').width-38) * 0.33333333,
        margin: 3,
        borderRadius: 3,
        //height: 100,
        //borderWidth: 1,
        //borderColor: "lightgray",
        alignItems: 'center',
        justifyContent: 'center'
    },
    gridItemImage: {
        borderRadius: 3 ,
        width: (Dimensions.get('window').width-60) * 0.333333,
        height: (Dimensions.get('window').width-60) * 0.333333,
    },
    statusMaritaltext: (color) => ({
        paddingHorizontal: 10,
        borderRadius: 10,
        fontSize: 12,
        color : "#ffffff",
        backgroundColor: color,
    }),
    statusMaritalDetailProfiletext: (color) => ({
        paddingHorizontal: 10,
        borderRadius: 10,
        fontSize: 14,
        color : "#ffffff",
        //position: 'absolute',
        //left: 7,
        //bottom: 3,
        backgroundColor: color,
    }),
    pickerSelect: (color) => ({
       color : color
    }),
    checkBox: (borderColor, backgroundColor) => ({
        borderColor: borderColor,
        backgroundColor: backgroundColor,
        borderRadius: 2
    }),
    containerMain: {
        flex: 1, backgroundColor: color.mainContentColor, zIndex: -1
    },

    containerMainWhite: {
        flex: 1, backgroundColor: '#fff', zIndex: -1
    },

    containerMainBlack: {
        flex: 1, backgroundColor: '#000', zIndex: -1
    },
    textBubble: {
        borderWidth: 1, borderRadius: 5, fontSize: 16, paddingHorizontal: 10,borderColor: '#ccc',
        marginHorizontal: 2, marginVertical: 2
    },
    gridViewMember: {
        marginTop: 0,
        flex: 1,
    },
    itemContainerMember: {
        justifyContent: 'flex-end',
        borderRadius: 5,
        height: gridDimension,
    },
    listItemFlatList: {
        paddingVertical: 5,
        margin: 3,
        flexDirection: "row",
        backgroundColor: "#192338",
        justifyContent: "flex-start",
        alignItems: "center",
        zIndex: -1
    },
    selectedItemFlatlist: {
        backgroundColor: "#FA7B5F"
    },
    floatingLabelEditProfile: {
        marginTop: 5
    }
};

const consultationPage = {
    GridViewContainer: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        margin: 5
    },
    itemSelected: {
        color: '#fff',
        borderRadius: 5,
        backgroundColor: '#ff2abf'
    },
    itemDisabled: {
        //backgroundColor: '#6e6e6e'
        color: '#d8d8d8'
    },
    itemSelectedText: (color)=> ({
        color: color
    }),
    GridViewTextLayout: {
        fontSize: 12,
        fontWeight: 'bold',
        justifyContent: 'center',
    }
};
export default styles = {
    loginPage: loginPage,
    formLogin: formLogin,
    registerPage: pageRegister,
    formRegister: formRegister,
    testingLogin: testingLogin,
    consultationPage: consultationPage,
    color: color,
    otherStyle: otherStyle
}
